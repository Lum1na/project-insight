// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "BTTask_FindMoveToPosition.h"
#include "FPS_Character.h"			
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "AI_BaseEnemyController.h"


EBTNodeResult::Type UBTTask_FindMoveToPosition::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAI_BaseEnemyController* AIPC = Cast<AAI_BaseEnemyController>(OwnerComp.GetAIOwner());
	if (AIPC)
	{
		AFPS_Character* PlayerCharacter = Cast<AFPS_Character>(AIPC->PlayerCharacter);
		if (PlayerCharacter)
		{ 
			uint8 TargetPosID = AIPC->BlackboardComponent->GetKeyID("TargetPos");
			UNavigationSystem* NavSys = UNavigationSystem::GetCurrent(GetWorld());
			if (!NavSys)
			{
				AIPC->BehaviorComponent->GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(TargetPosID, PlayerCharacter->GetActorLocation());
				return EBTNodeResult::Succeeded;
			}

			FNavLocation Result;

			bool bSuccess = NavSys->GetRandomReachablePointInRadius(PlayerCharacter->GetActorLocation(), 500, Result);

			if (!bSuccess)
			{
				AIPC->BehaviorComponent->GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(TargetPosID, PlayerCharacter->GetActorLocation());
				return EBTNodeResult::Succeeded;
			}
	
			AIPC->BehaviorComponent->GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(TargetPosID, Result.Location);
			return EBTNodeResult::Succeeded;
		}
	}
	return EBTNodeResult::Failed;	
}  