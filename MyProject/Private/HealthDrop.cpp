// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "HealthDrop.h"
#include "FPS_Character.h"


AHealthDrop::AHealthDrop()
{
	PickupRadius = CreateDefaultSubobject<USphereComponent>(TEXT("Pickup Sphere"));
	PickupRadius->SetupAttachment(RootComponent);
	PickupRadius->OnComponentBeginOverlap.AddDynamic(this, &AHealthDrop::OnBeginOverlap);
}

void AHealthDrop::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AFPS_Character* Player = Cast<AFPS_Character>(OtherActor);
	if (Player)
	{
		if (Player->GetPlayerStats()->GetCurrentHealth() < Player->GetPlayerStats()->GetMaxHealth())
		{
			/** 10% of Player life*/
			float f_AmountOfHealthReceived = 0;
			f_AmountOfHealthReceived = (Player->GetPlayerStats()->GetMaxHealth() / 100) * 10;

			UAudioComponent* AC = NULL;
			if (PickupSound)
			{
				AC = UGameplayStatics::SpawnSoundAttached(PickupSound, GetRootComponent());
			}
			Player->GetPlayerStats()->ChangeCurrentHealth(+f_AmountOfHealthReceived);
			Destroy();
		}
	}
}
