// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "DieBegegnung_GameMode.h"
#include "UserWidget.h"


ADieBegegnung_GameMode::ADieBegegnung_GameMode()
{

}

void ADieBegegnung_GameMode::BeginPlay()
{
	Super::BeginPlay();	
	for (TActorIterator<AWaypoint_Manager> ActrItr(GetWorld()); ActrItr; ++ActrItr)
	{
		WPManager = *ActrItr;
	}
	if (PlayerHUDClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}

}

void ADieBegegnung_GameMode::StartWaveSpawn()
{
	GetWorld()->GetTimerManager().SetTimer(WaveTimer, this, &ADieBegegnung_GameMode::SpawnEnemyWave, TimeBetweenWaves, true);
}

void ADieBegegnung_GameMode::SpawnEnemyWave()
{
	EnemiesSpawnedThisWave = 0;
	GetWorld()->GetTimerManager().SetTimer(EnemyTimer, this, &ADieBegegnung_GameMode::SpawnEnemy, TimeBetweenEnemys, true,0.0f);
}

void ADieBegegnung_GameMode::SpawnEnemy()
{
	EnemiesSpawnedThisWave++;
	if(EnemiesSpawnedThisWave >= EnemiesPerWave)
		GetWorld()->GetTimerManager().SetTimer(EnemyTimer, this, &ADieBegegnung_GameMode::SpawnEnemy, 0, false);

	if (WPManager)
	{
		ASpawnPoint* SpawnPoint = WPManager->GetRandomSpawnPoint();
		FActorSpawnParameters Spawnparams;
		GetWorld()->SpawnActor<ABase_EnemyCharacter>(EnemyToSpawn, SpawnPoint->GetActorLocation(), SpawnPoint->GetActorRotation(), Spawnparams);
	}
}