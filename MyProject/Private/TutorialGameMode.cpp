// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Blueprint/UserWidget.h"
#include "FPS_Character.h"
#include "TutorialGameMode.h"

void ATutorialGameMode::BeginPlay()
{
	Super::BeginPlay();
	Player = Cast < AFPS_Character>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	Player->bEquipDefaultWeapon = false;
	if (PlayerHUDClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
	Player->SetInformationText("Welcome to the Tutorial, you can move your character by pressing the 'W,A,S,D' keys. You can also jump by pressing 'Space'  and sprint by pressing 'Shift'. Try it out!", false);
}

void ATutorialGameMode::SetbCompletedMovementTutorial(bool _bool)
{
	if (bCompletedMovementTutorial == true) return;
	Player->SetInformationText("Great job, now go through that door and sprint to the end of the Corridor. Be fast! You only have a limited amount of time!", 10.0f);
	bCompletedMovementTutorial = _bool;
}

void ATutorialGameMode::SetbCompletedWeaponTutorial(bool _bool)
{
	bCompletedWeaponTutorial = _bool;
	Player->SetInformationText("Nice work once again! Go the the next room behind you so we can introduce you to the Items in this Game");
}

void ATutorialGameMode::SetbCompletedItemTutorial(bool _bool)
{
	bCompletedItemTutorial = _bool;
}

bool ATutorialGameMode::GetbCompletedMovementTutorial()
{
	return bCompletedMovementTutorial;
}

bool ATutorialGameMode::GetbCompletedWeaponTutorial()
{
	return bCompletedWeaponTutorial;
}

bool ATutorialGameMode::GetbCompletedItemTutorial()
{
	return bCompletedItemTutorial;
}

void ATutorialGameMode::AddTarget()
{
	DummyTargets++;
}

void ATutorialGameMode::AddShotTargetCounter()
{
	ShotTargetCounter++;
	if (ShotTargetCounter == DummyTargets)
	{
		SetbCompletedWeaponTutorial(true);
	}
}

void ATutorialGameMode::CheckMovementTutorialCompleted()
{
	if (bWPressed &&
		bSPressed &&
		bAPressed &&
		bDPressed &&
		bSpacePressed)

		SetbCompletedMovementTutorial(true);
	else
		bCompletedMovementTutorial = false;
}

void ATutorialGameMode::SetbWPressed()
{
	bWPressed = true;
	CheckMovementTutorialCompleted();
}
void ATutorialGameMode::SetbSPressed()
{
	bSPressed = true;
	CheckMovementTutorialCompleted();
}
void ATutorialGameMode::SetbAPressed()
{
	bAPressed = true;
	CheckMovementTutorialCompleted();
}
void ATutorialGameMode::SetbDPressed()
{
	bDPressed = true;
	CheckMovementTutorialCompleted();
}

void ATutorialGameMode::SetbSpacePressed()
{
	bSpacePressed = true;
	CheckMovementTutorialCompleted();
}

void ATutorialGameMode::StartWeaponTutorial()
{
	Player->SetInformationText("Nice! There is a Weapon on the Table. Pick it up by looking at it and pressing the 'F' Key. After that Shoot the Targets to your left.");
}

void ATutorialGameMode::StartItemTutorial()
{
	Player->SetInformationText(" There are two Items on the Table in front of you. A Weapon and a piece of Armor both can be dropped by Enemies when they die "
		" so be on the lookout for them. Dropped items are Indicated by the Colored Beam.", TutorialTextDelay);
	FTimerHandle NextPartTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(NextPartTimerHandle, this, &ATutorialGameMode::StartSecondTutorialPart, TutorialTextDelay, false, TutorialTextDelay);
}

void ATutorialGameMode::StartSecondTutorialPart()
{
	Player->SetInformationText(" After you picked up Weapons and Armor they appear in your Inventory, that you can access by pressing the 'I' or 'B' Key."
		" The Inventory is Seperated into Different Categories for each Weapon Type and Armor Type.", TutorialTextDelay);

	FTimerHandle NextPartTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(NextPartTimerHandle, this, &ATutorialGameMode::StartThirdTutorialPart, TutorialTextDelay, false, TutorialTextDelay);
}

void ATutorialGameMode::StartThirdTutorialPart()
{
	Player->SetInformationText(" By hovering over an Item in your Inventory you can see what stats it has and what stats the item has you are currently wearing."
		" By Pressing on an Item you can choose to either Equip it or Destroy it to free some Space in your Inventory!", TutorialTextDelay);
	FTimerHandle NextPartTimerHandle;
	GetWorld()->GetTimerManager().SetTimer(NextPartTimerHandle, this, &ATutorialGameMode::StartLastTutorialPart, TutorialTextDelay, false, TutorialTextDelay);
}

void ATutorialGameMode::StartLastTutorialPart()
{
	Player->SetInformationText("Now that you have learned it all go through the last door and start your Mission!", TutorialTextDelay / 2);
	SetbCompletedItemTutorial(true);
}
