// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Waypoint_Manager.h"
#include "EngineUtils.h"

AWaypoint_Manager::AWaypoint_Manager()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AWaypoint_Manager::BeginPlay()
{
	Super::BeginPlay();
	for (TActorIterator<AWaypoint> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		AWaypoint* waypoint = *ActorItr;
		WaypointArray.Add(waypoint);
	}

	for (TActorIterator<ASpawnPoint> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		ASpawnPoint* spawnpoint = *ActorItr;
		SpawnpointArray.Add(spawnpoint);
	}
}

AWaypoint* AWaypoint_Manager::GetRandomWaypoint()
{
	if (WaypointArray.Num() > 0)
	{
		int Random = FMath::RandRange(0, (WaypointArray.Num() - 1));
		return WaypointArray[Random];
	}
	else
		return nullptr;
}

ASpawnPoint* AWaypoint_Manager::GetRandomSpawnPoint()
{
	if (SpawnpointArray.Num() > 0)
	{
		int Random = FMath::RandRange(0, (SpawnpointArray.Num() - 1));
		return SpawnpointArray[Random];
	}
	else
		return nullptr;
}