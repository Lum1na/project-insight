// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Item_Pickup.h"
#include "Armor_Base.h"
#include "Armor_Chestpiece.h"
#include "Armor_Feet.h"
#include "Armor_HeadPiece.h"
#include "Armor_Legpiece.h"
#include "Armor_EnergyShield.h"
#include "Rifle.h"
#include "RocketLauncher.h"
#include "SMG.h"
#include "Shotgun.h"
#include "FPS_Character.h"
#include "FPS_PlayerController.h"
#include "Inventory.h"

UInventory::UInventory()
{
	PrimaryComponentTick.bCanEverTick = false;
	i_WeaponInventorySize = 20;
}

void UInventory::BeginPlay()
{
	Super::BeginPlay();
}

void UInventory::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

bool UInventory::GetIsWeaponInventoryFull(TEnumAsByte<InventorySlot> _Inventory)
{
	int FilledInventorySlot = 0;
	bool b_InventoryFull;

	switch (_Inventory)
	{
#pragma region ArmorCases
	case InventorySlot::Chest:
		for (int i = 0; i < Chest_ArmorInventory.Num(); i++)
		{
			if (Chest_ArmorInventory[i] != nullptr)
				FilledInventorySlot++;
			if (FilledInventorySlot == i_WeaponInventorySize)
			{
				b_InventoryFull = true;
				return b_InventoryFull;
			}

		}
		b_InventoryFull = false;
		return b_InventoryFull;
	case InventorySlot::Head:
		for (int i = 0; i < Head_ArmorInventory.Num(); i++)
		{
			if (Head_ArmorInventory[i] != nullptr)
				FilledInventorySlot++;
			if (FilledInventorySlot == i_WeaponInventorySize)
			{
				b_InventoryFull = true;
				return b_InventoryFull;
			}
		}
		b_InventoryFull = false;
		return b_InventoryFull;
	case InventorySlot::Legs:
		for (int i = 0; i < Legs_ArmorInventory.Num(); i++)
		{
			if (Legs_ArmorInventory[i] != nullptr)
				FilledInventorySlot++;
			if (FilledInventorySlot == i_WeaponInventorySize)
			{
				b_InventoryFull = true;
				return b_InventoryFull;
			}
		}
		b_InventoryFull = false;
		return b_InventoryFull;
	case InventorySlot::Feet:
		for (int i = 0; i < Feet_ArmorInventory.Num(); i++)
		{
			if (Feet_ArmorInventory[i] != nullptr)
				FilledInventorySlot++;
			if (FilledInventorySlot == i_WeaponInventorySize)
			{
				b_InventoryFull = true;
				return b_InventoryFull;
			}
		}
		b_InventoryFull = false;
		return b_InventoryFull;
	case InventorySlot::Shield:
		for (int i = 0; i < Energyshield_ArmorInventory.Num(); i++)
		{
			if (Energyshield_ArmorInventory[i] != nullptr)
				FilledInventorySlot++;
			if (FilledInventorySlot == i_WeaponInventorySize)
			{
				b_InventoryFull = true;
				return b_InventoryFull;
			}
		}
		b_InventoryFull = false;
		return b_InventoryFull;
#pragma endregion

#pragma region WeaponCases
	case InventorySlot::Rifle:
		for (int i = 0; i < Rifle_WeaponInventory.Num(); i++)
		{
			if (Rifle_WeaponInventory[i] != nullptr)
				FilledInventorySlot++;
			if (FilledInventorySlot == i_WeaponInventorySize)
			{
				b_InventoryFull = true;
				return	 b_InventoryFull;
			}
		}
		b_InventoryFull = false;
		return b_InventoryFull;
	case InventorySlot::SMG:
		for (int i = 0; i < SMG_WeaponInventory.Num(); i++)
		{
			if (SMG_WeaponInventory[i] != nullptr)
				FilledInventorySlot++;
			if (FilledInventorySlot == i_WeaponInventorySize)
			{
				b_InventoryFull = true;
				return	 b_InventoryFull;
			}
		}
		b_InventoryFull = false;
		return b_InventoryFull;
	case InventorySlot::RocketLauncher:
		for (int i = 0; i < RocketLauncher_WeaponInventory.Num(); i++)
		{
			if (RocketLauncher_WeaponInventory[i] != nullptr)
				FilledInventorySlot++;
			if (FilledInventorySlot == i_WeaponInventorySize)
			{
				b_InventoryFull = true;
				return	 b_InventoryFull;
			}
		}
		b_InventoryFull = false;
		return b_InventoryFull;
	case InventorySlot::Shotgun:
		for (int i = 0; i < Shotgun_WeaponInventory.Num(); i++)
		{
			if (Shotgun_WeaponInventory[i] != nullptr)
				FilledInventorySlot++;
			if (FilledInventorySlot == i_WeaponInventorySize)
			{
				b_InventoryFull = true;
				return	 b_InventoryFull;
			}
		}
		b_InventoryFull = false;
		return b_InventoryFull;
	}
#pragma endregion
	return false;
}


void UInventory::AddWeaponToInventory(AItem_PickUp * _Weapon)
{
	ASMG* Weapon_SMG = Cast<ASMG>(_Weapon);
	ARifle* Weapon_Rifle = Cast<ARifle>(_Weapon);
	ARocketLauncher* Weapon_RocketLauncher = Cast<ARocketLauncher>(_Weapon);
	AShotgun* Weapon_Shotgun = Cast<AShotgun>(_Weapon);

	if (Weapon_SMG)
	{
		if (GetIsWeaponInventoryFull(InventorySlot::SMG) == false)
		{
			if (SMG_WeaponInventory.Num() < i_WeaponInventorySize)
			{
				SMG_WeaponInventory.Add(Weapon_SMG);
				SMG_WeaponInventory[SMG_WeaponInventory.Num() - 1]->WeaponConfig.InventorySlot = SMG_WeaponInventory.Num() - 1;
				SMG_WeaponInventory[SMG_WeaponInventory.Num() - 1]->SetActiveWeapon(false);
				SMG_WeaponInventory[SMG_WeaponInventory.Num() - 1]->HideWeapon();
				SMG_WeaponInventory[SMG_WeaponInventory.Num() - 1]->EquipWeapon();			 			
				return;
			}
			for (int i = 0; i < SMG_WeaponInventory.Num(); i++)
			{
				if (SMG_WeaponInventory[i] == nullptr)
				{
					SMG_WeaponInventory[i] = Weapon_SMG;
					SMG_WeaponInventory[i]->WeaponConfig.InventorySlot = i;
					SMG_WeaponInventory[i]->SetActiveWeapon(false);
					SMG_WeaponInventory[i]->EquipWeapon();
					return;
				}
			}
		}
		else
		{
			if (Owner)
			{
				Owner->SetInformationText(FString("SMG Inventory is Full. Cannot Pick up Weapon!"));
			}
			return;
		}
	}

	if (Weapon_Rifle)
	{
		if (GetIsWeaponInventoryFull(InventorySlot::Rifle) == false)
		{
			if (Rifle_WeaponInventory.Num() < i_WeaponInventorySize)
			{
				Rifle_WeaponInventory.Add(Weapon_Rifle);
				Rifle_WeaponInventory[Rifle_WeaponInventory.Num() - 1]->WeaponConfig.InventorySlot = Rifle_WeaponInventory.Num() - 1;
				Rifle_WeaponInventory[Rifle_WeaponInventory.Num() - 1]->SetActiveWeapon(false);
				Rifle_WeaponInventory[Rifle_WeaponInventory.Num() - 1]->HideWeapon();
				Rifle_WeaponInventory[Rifle_WeaponInventory.Num() - 1]->EquipWeapon();
				return;
			}
			for (int i = 0; i < Rifle_WeaponInventory.Num(); i++)
			{
				if (Rifle_WeaponInventory[i] == nullptr)
				{
					Rifle_WeaponInventory[i] = Weapon_Rifle;
					Rifle_WeaponInventory[i]->WeaponConfig.InventorySlot = i;
					Rifle_WeaponInventory[i]->SetActiveWeapon(false);
					Rifle_WeaponInventory[i]->EquipWeapon();
					return;
				}
			}
		}
		else
		{
			if (Owner)
			{
				Owner->SetInformationText(FString("Rifle Inventory is Full. Cannot Pick up Weapon!"));
			}
			return;
		}
	}

	if (Weapon_RocketLauncher)
	{
		if (GetIsWeaponInventoryFull(InventorySlot::RocketLauncher) == false)
		{
			if (RocketLauncher_WeaponInventory.Num() < i_WeaponInventorySize)
			{
				RocketLauncher_WeaponInventory.Add(Weapon_RocketLauncher);
				RocketLauncher_WeaponInventory[RocketLauncher_WeaponInventory.Num() - 1]->WeaponConfig.InventorySlot = RocketLauncher_WeaponInventory.Num() - 1;
				RocketLauncher_WeaponInventory[RocketLauncher_WeaponInventory.Num() - 1]->SetActiveWeapon(false);
				RocketLauncher_WeaponInventory[RocketLauncher_WeaponInventory.Num() - 1]->HideWeapon();
				RocketLauncher_WeaponInventory[RocketLauncher_WeaponInventory.Num() - 1]->EquipWeapon();
				return;
			}
			for (int i = 0; i < RocketLauncher_WeaponInventory.Num(); i++)
			{
				if (RocketLauncher_WeaponInventory[i] == nullptr)
				{
					RocketLauncher_WeaponInventory[i] = Weapon_RocketLauncher;
					RocketLauncher_WeaponInventory[i]->WeaponConfig.InventorySlot = i;
					RocketLauncher_WeaponInventory[i]->SetActiveWeapon(false);
					RocketLauncher_WeaponInventory[i]->EquipWeapon();
					return;
				}
			}
		}
		else
		{
			if (Owner)
			{
				Owner->SetInformationText(FString("RocketLauncher Inventory is Full. Cannot Pick up Weapon!"));
			}
			return;
		}
	}

	if (Weapon_Shotgun)
	{
		if (GetIsWeaponInventoryFull(InventorySlot::Shotgun) == false)
		{
			if (Shotgun_WeaponInventory.Num() < i_WeaponInventorySize)
			{
				Shotgun_WeaponInventory.Add(Weapon_Shotgun);
				Shotgun_WeaponInventory[Shotgun_WeaponInventory.Num() - 1]->WeaponConfig.InventorySlot = Shotgun_WeaponInventory.Num() - 1;
				Shotgun_WeaponInventory[Shotgun_WeaponInventory.Num() - 1]->SetActiveWeapon(false);
				Shotgun_WeaponInventory[Shotgun_WeaponInventory.Num() - 1]->HideWeapon();
				Shotgun_WeaponInventory[Shotgun_WeaponInventory.Num() - 1]->EquipWeapon();
				return;
			}
			for (int i = 0; i < Shotgun_WeaponInventory.Num(); i++)
			{
				if (Shotgun_WeaponInventory[i] == nullptr)
				{
					Shotgun_WeaponInventory[i] = Weapon_Shotgun;
					Shotgun_WeaponInventory[i]->WeaponConfig.InventorySlot = i;
					Shotgun_WeaponInventory[i]->SetActiveWeapon(false);
					Shotgun_WeaponInventory[i]->EquipWeapon();
					return;
				}
			}
		}
		else
		{
			if (Owner)
			{
				Owner->SetInformationText(FString("Shotgun Inventory is Full. Cannot Pick up Weapon!"));
			}
			return;
		}
	}
}

void UInventory::AddArmorToInventory(AItem_PickUp* _ArmorPiece)
{
	AArmor_Chestpiece* Armor_Chest = Cast<AArmor_Chestpiece>(_ArmorPiece);
	AArmor_HeadPiece* Armor_Head = Cast<AArmor_HeadPiece>(_ArmorPiece);
	AArmor_Feet* Armor_Feet = Cast<AArmor_Feet>(_ArmorPiece);
	AArmor_Legpiece* Armor_Legs = Cast<AArmor_Legpiece>(_ArmorPiece);
	AArmor_EnergyShield* Armor_Shield = Cast<AArmor_EnergyShield>(_ArmorPiece);


	if (Armor_Chest)
	{
		if (GetIsWeaponInventoryFull(InventorySlot::Chest) == false)
		{
			if (Chest_ArmorInventory.Num() < i_WeaponInventorySize)
			{
				Chest_ArmorInventory.Add(Armor_Chest);
				Armor_Chest->Equip();
				Chest_ArmorInventory[Chest_ArmorInventory.Num() - 1]->ArmorConfig.i_InventorySlot = Chest_ArmorInventory.Num() - 1;
				return;
			}
			for (int i = 0; i < Chest_ArmorInventory.Num(); i++)
			{
				if (Chest_ArmorInventory[i] == nullptr)
				{
					Chest_ArmorInventory[i] = Armor_Chest;
					Armor_Chest->Equip();
					Chest_ArmorInventory[i]->ArmorConfig.i_InventorySlot = i;
					return;
				}
			}
		}
		else
		{
			if (Owner)
				Owner->SetInformationText(FString("ChestArmor Inventory is Full. Cannot Pick up Weapon!"));
			return;
		}
	}

	if (Armor_Head)
	{
		if (GetIsWeaponInventoryFull(InventorySlot::Head) == false)
		{
			if (Head_ArmorInventory.Num() < i_WeaponInventorySize)
			{
				Head_ArmorInventory.Add(Armor_Head);
				Armor_Head->Equip();
				Head_ArmorInventory[Head_ArmorInventory.Num() - 1]->ArmorConfig.i_InventorySlot = Head_ArmorInventory.Num() - 1;
				return;
			}
			for (int i = 0; i < Head_ArmorInventory.Num(); i++)
			{
				if (Head_ArmorInventory[i] == nullptr)
				{
					Head_ArmorInventory[i] = Armor_Head;
					Armor_Head->Equip();
					Head_ArmorInventory[i]->ArmorConfig.i_InventorySlot = i;
					return;
				}
			}
		}
		else
		{
			if (Owner)
				Owner->SetInformationText(FString("HeadArmor Inventory is Full. Cannot Pick up Weapon!"));
			return;
		}
	}

	if (Armor_Feet)
	{
		if (GetIsWeaponInventoryFull(InventorySlot::Feet) == false)
		{
			if (Feet_ArmorInventory.Num() < i_WeaponInventorySize)
			{
				Feet_ArmorInventory.Add(Armor_Feet);
				Armor_Feet->Equip();
				Feet_ArmorInventory[Feet_ArmorInventory.Num() - 1]->ArmorConfig.i_InventorySlot = Feet_ArmorInventory.Num() - 1;
				return;
			}
			for (int i = 0; i < Feet_ArmorInventory.Num(); i++)
			{
				if (Feet_ArmorInventory[i] == nullptr)
				{
					Feet_ArmorInventory[i] = Armor_Feet;
					Armor_Feet->Equip();
					Feet_ArmorInventory[i]->ArmorConfig.i_InventorySlot = i;
					return;
				}
			}
		}
		else
		{
			if (Owner)
				Owner->SetInformationText(FString("FeetArmor Inventory is Full. Cannot Pick up Weapon!"));
			return;
		}
	}

	if (Armor_Legs)
	{
		if (GetIsWeaponInventoryFull(InventorySlot::Legs) == false)
		{
			if (Legs_ArmorInventory.Num() < i_WeaponInventorySize)
			{
				Legs_ArmorInventory.Add(Armor_Legs);
				Armor_Legs->Equip();
				Legs_ArmorInventory[Legs_ArmorInventory.Num() - 1]->ArmorConfig.i_InventorySlot = Legs_ArmorInventory.Num() - 1;
				return;
			}
			for (int i = 0; i < Legs_ArmorInventory.Num(); i++)
			{
				if (Legs_ArmorInventory[i] == nullptr)
				{
					Legs_ArmorInventory[i] = Armor_Legs;
					Armor_Legs->Equip();
					Legs_ArmorInventory[i]->ArmorConfig.i_InventorySlot = i;
					return;
				}
			}
		}
		else
		{
			if (Owner)
				Owner->SetInformationText(FString("LegArmor Inventory is Full. Cannot Pick up Weapon!"));
			return;
		}
	}

	if (Armor_Shield)
	{
		if (GetIsWeaponInventoryFull(InventorySlot::Shield) == false)
		{
			if (Energyshield_ArmorInventory.Num() < i_WeaponInventorySize)
			{
				Energyshield_ArmorInventory.Add(Armor_Shield);
				Armor_Shield->Equip();
				Energyshield_ArmorInventory[Energyshield_ArmorInventory.Num() - 1]->ArmorConfig.i_InventorySlot = Energyshield_ArmorInventory.Num() - 1;
				return;
			}
			for (int i = 0; i < Energyshield_ArmorInventory.Num(); i++)
			{
				if (Energyshield_ArmorInventory[i] == nullptr)
				{
					Energyshield_ArmorInventory[i] = Armor_Shield;
					Armor_Shield->Equip();
					Energyshield_ArmorInventory[i]->ArmorConfig.i_InventorySlot = i;
					return;
				}
			}
		}
		else
		{
			if (Owner)
				Owner->SetInformationText(FString("Energyshield Inventory is Full. Cannot Pick up Weapon!"));
			return;
		}
	}
}

void UInventory::RemoveWeaponFromInventory(AWeapon_BaseClass * _Weapon)
{
	ASMG* Weapon_SMG = Cast<ASMG>(_Weapon);
	ARifle* Weapon_Rifle = Cast<ARifle>(_Weapon);
	ARocketLauncher* Weapon_RocketLauncher = Cast<ARocketLauncher>(_Weapon);
	AShotgun* Weapon_Shotgun = Cast<AShotgun>(_Weapon);

	if (Weapon_SMG)
	{
		for (int i = 0; i < SMG_WeaponInventory.Num(); i++)
		{
			if (SMG_WeaponInventory[i] == _Weapon)
			{
				SMG_WeaponInventory[i] = nullptr;
				return;
			}
		}
	}

	if (Weapon_Rifle)
	{
		for (int i = 0; i < Rifle_WeaponInventory.Num(); i++)
		{
			if (Rifle_WeaponInventory[i] == _Weapon)
			{
				Rifle_WeaponInventory[i] = nullptr;
				return;
			}
		}
	}

	if (Weapon_RocketLauncher)
	{
		for (int i = 0; i < RocketLauncher_WeaponInventory.Num(); i++)
		{
			if (RocketLauncher_WeaponInventory[i] == _Weapon)
			{
				RocketLauncher_WeaponInventory[i] = nullptr;
				return;
			}
		}
	}

	if (Weapon_Shotgun)
	{
		for (int i = 0; i < Shotgun_WeaponInventory.Num(); i++)
		{
			if (Shotgun_WeaponInventory[i] == _Weapon)
			{
				Shotgun_WeaponInventory[i] = nullptr;
				return;
			}
		}
	}
}

void UInventory::RemoveArmorFromInventory(AArmor_Base* _Armor)
{
	AArmor_Chestpiece* Armor_Chest = Cast<AArmor_Chestpiece>(_Armor);
	AArmor_HeadPiece* Armor_Head = Cast<AArmor_HeadPiece>(_Armor);
	AArmor_Feet* Armor_Feet = Cast<AArmor_Feet>(_Armor);
	AArmor_Legpiece* Armor_Legs = Cast<AArmor_Legpiece>(_Armor);
	AArmor_EnergyShield* Armor_Shield = Cast<AArmor_EnergyShield>(_Armor);


	if (Armor_Chest)
	{
		for (int i = 0; i < Chest_ArmorInventory.Num(); i++)
		{
			if (Chest_ArmorInventory[i] == _Armor)
			{
				Chest_ArmorInventory[i] = nullptr;
				return;
			}
		}
		return;
	}

	if (Armor_Head)
	{
		for (int i = 0; i < Head_ArmorInventory.Num(); i++)
		{
			if (Head_ArmorInventory[i] == _Armor)
			{
				Head_ArmorInventory[i] = nullptr;
				return;
			}
		}
		return;
	}

	if (Armor_Feet)
	{
		for (int i = 0; i < Feet_ArmorInventory.Num(); i++)
		{
			if (Feet_ArmorInventory[i] == _Armor)
			{
				Feet_ArmorInventory[i] = nullptr;
				return;
			}
		}
		return;
	}

	if (Armor_Legs)
	{
		for (int i = 0; i < Legs_ArmorInventory.Num(); i++)
		{
			if (Legs_ArmorInventory[i] == _Armor)
			{
				Legs_ArmorInventory[i] = nullptr;
				return;
			}
		}
		return;
	}

	if (Armor_Shield)
	{
		for (int i = 0; i < Energyshield_ArmorInventory.Num(); i++)
		{
			if (Energyshield_ArmorInventory[i] == _Armor)
			{
				Energyshield_ArmorInventory[i] = nullptr;
				return;
			}
		}
		return;
	}
}

AWeapon_BaseClass * UInventory::GetWeapon(int32 InventorySlot)
{
	if (SMG_WeaponInventory[InventorySlot] != NULL)
	{
		return SMG_WeaponInventory[InventorySlot];
	}
	else
		return NULL;
}

void UInventory::ToggleInventoryShowing()
{
	b_InventoryShowing = !b_InventoryShowing;
	if (b_InventoryShowing)
	{
		if (Owner)
		{
			Owner->ShowCursor();
			if (Owner->Player_Controller)
				Owner->Player_Controller->AddLastOpenedHudElement(EHudElements::Inventory);
		}
	}
	else
	{
		if (Owner)
		{
			if (Owner->Player_Controller)
				Owner->Player_Controller->RemoveHudElementFromList(EHudElements::Inventory);
			Owner->HideCursor();
		}
	}
}

ESlateVisibility UInventory::GetIsInventoryShowing()
{
	if (b_InventoryShowing)
		return ESlateVisibility::Visible;
	else
		return ESlateVisibility::Hidden;
}

void UInventory::SetCurrentInteractable(AItem_PickUp * Item)
{
	CurrentInteractable = Item;
}

AItem_PickUp* UInventory::GetCurrentInteractable()
{
	if (CurrentInteractable)
		return CurrentInteractable;

	return nullptr;
}

void UInventory::SetOwner(AFPS_Character* _Owner)
{
	Owner = _Owner;
	PlayerStats = Owner->GetPlayerStats();
}

void UInventory::EquipArmor(AArmor_Base* _ArmorToEquip, bool _b_ReplaceCurrentlyEquipped)
{
	AArmor_HeadPiece* HeadArmor = Cast<AArmor_HeadPiece>(_ArmorToEquip);
	if (HeadArmor)
	{
		if (EquippedArmor.EquippedHelmetArmor && _b_ReplaceCurrentlyEquipped == false)
		{
			AddArmorToInventory(HeadArmor);
			return;
		}
		if (EquippedArmor.EquippedHelmetArmor && _b_ReplaceCurrentlyEquipped)
		{
			RemoveArmorFromInventory(HeadArmor);
			UnEquipArmor(1);
		}

		EquippedArmor.EquippedHelmetArmor = HeadArmor;
		EquippedArmor.EquippedHelmetArmor->Equip();
		AddStats(
			EquippedArmor.EquippedHelmetArmor->ArmorConfig.f_Vitality,
			EquippedArmor.EquippedHelmetArmor->ArmorConfig.f_Accuracy,
			EquippedArmor.EquippedHelmetArmor->ArmorConfig.f_CritChance,
			EquippedArmor.EquippedHelmetArmor->ArmorConfig.f_CritDamage,
			EquippedArmor.EquippedHelmetArmor->ArmorConfig.f_Toughness,
			EquippedArmor.EquippedHelmetArmor->ArmorConfig.f_Armor,
			EquippedArmor.EquippedHelmetArmor->ArmorConfig.f_LifeLeach);
	}

	AArmor_Chestpiece* ChestArmor = Cast<AArmor_Chestpiece>(_ArmorToEquip);
	if (ChestArmor)
	{
		if (EquippedArmor.EquippedChestArmor  && _b_ReplaceCurrentlyEquipped == false)
		{
			AddArmorToInventory(ChestArmor);
			return;
		}
		if (EquippedArmor.EquippedChestArmor && _b_ReplaceCurrentlyEquipped)
		{
			RemoveArmorFromInventory(ChestArmor);
			UnEquipArmor(2);
		}
		EquippedArmor.EquippedChestArmor = ChestArmor;
		EquippedArmor.EquippedChestArmor->Equip();
		AddStats(
			EquippedArmor.EquippedChestArmor->ArmorConfig.f_Vitality,
			EquippedArmor.EquippedChestArmor->ArmorConfig.f_Accuracy,
			EquippedArmor.EquippedChestArmor->ArmorConfig.f_CritChance,
			EquippedArmor.EquippedChestArmor->ArmorConfig.f_CritDamage,
			EquippedArmor.EquippedChestArmor->ArmorConfig.f_Toughness,
			EquippedArmor.EquippedChestArmor->ArmorConfig.f_Armor,
			EquippedArmor.EquippedChestArmor->ArmorConfig.f_LifeLeach);
	}

	AArmor_Legpiece* LegArmor = Cast<AArmor_Legpiece>(_ArmorToEquip);
	if (LegArmor)
	{
		if (EquippedArmor.EquippedLegArmor  && _b_ReplaceCurrentlyEquipped == false)
		{
			AddArmorToInventory(LegArmor);
			return;
		}
		if (EquippedArmor.EquippedLegArmor && _b_ReplaceCurrentlyEquipped)
		{
			RemoveArmorFromInventory(LegArmor);
			UnEquipArmor(3);
		}
		EquippedArmor.EquippedLegArmor = LegArmor;
		EquippedArmor.EquippedLegArmor->Equip();
		AddStats(
			EquippedArmor.EquippedLegArmor->ArmorConfig.f_Vitality,
			EquippedArmor.EquippedLegArmor->ArmorConfig.f_Accuracy,
			EquippedArmor.EquippedLegArmor->ArmorConfig.f_CritChance,
			EquippedArmor.EquippedLegArmor->ArmorConfig.f_CritDamage,
			EquippedArmor.EquippedLegArmor->ArmorConfig.f_Toughness,
			EquippedArmor.EquippedLegArmor->ArmorConfig.f_Armor,
			EquippedArmor.EquippedLegArmor->ArmorConfig.f_LifeLeach);

	}

	AArmor_Feet* FeetArmor = Cast<AArmor_Feet>(_ArmorToEquip);
	if (FeetArmor)
	{
		if (EquippedArmor.EquippedFeetArmor  && _b_ReplaceCurrentlyEquipped == false)
		{
			AddArmorToInventory(FeetArmor);
			return;
		}
		if (EquippedArmor.EquippedFeetArmor && _b_ReplaceCurrentlyEquipped)
		{
			RemoveArmorFromInventory(FeetArmor);
			UnEquipArmor(4);
		}
		EquippedArmor.EquippedFeetArmor = FeetArmor;
		EquippedArmor.EquippedFeetArmor->Equip();
		AddStats(
			EquippedArmor.EquippedFeetArmor->ArmorConfig.f_Vitality,
			EquippedArmor.EquippedFeetArmor->ArmorConfig.f_Accuracy,
			EquippedArmor.EquippedFeetArmor->ArmorConfig.f_CritChance,
			EquippedArmor.EquippedFeetArmor->ArmorConfig.f_CritDamage,
			EquippedArmor.EquippedFeetArmor->ArmorConfig.f_Toughness,
			EquippedArmor.EquippedFeetArmor->ArmorConfig.f_Armor,
			EquippedArmor.EquippedFeetArmor->ArmorConfig.f_LifeLeach);
	}

	AArmor_EnergyShield* EnergyShield = Cast<AArmor_EnergyShield>(_ArmorToEquip);
	if (EnergyShield)
	{
		if (EquippedArmor.EquippedEnergyShield && _b_ReplaceCurrentlyEquipped == false)
		{
			AddArmorToInventory(EnergyShield);
			return;
		}
		if (EquippedArmor.EquippedEnergyShield && _b_ReplaceCurrentlyEquipped)
		{
			RemoveArmorFromInventory(EnergyShield);
			UnEquipArmor(5);
		}
		EquippedArmor.EquippedEnergyShield = EnergyShield;
		EquippedArmor.EquippedEnergyShield->Equip();
		AddStats(
			EquippedArmor.EquippedEnergyShield->ArmorConfig.f_Vitality,
			EquippedArmor.EquippedEnergyShield->ArmorConfig.f_Accuracy,
			EquippedArmor.EquippedEnergyShield->ArmorConfig.f_CritChance,
			EquippedArmor.EquippedEnergyShield->ArmorConfig.f_CritDamage,
			EquippedArmor.EquippedEnergyShield->ArmorConfig.f_Toughness,
			EquippedArmor.EquippedEnergyShield->ArmorConfig.f_Armor,
			EquippedArmor.EquippedEnergyShield->ArmorConfig.f_LifeLeach);
	}
}

void UInventory::UnEquipArmor(int _i_Slot)
{
	switch (_i_Slot)
	{
	case 1:
		EquippedArmor.LastEquippedHelmetArmor = EquippedArmor.EquippedHelmetArmor;
		AddArmorToInventory(EquippedArmor.EquippedHelmetArmor);
		EquippedArmor.EquippedHelmetArmor = nullptr;
		AddStats(
			-EquippedArmor.LastEquippedHelmetArmor->ArmorConfig.f_Vitality,
			-EquippedArmor.LastEquippedHelmetArmor->ArmorConfig.f_Accuracy,
			-EquippedArmor.LastEquippedHelmetArmor->ArmorConfig.f_CritChance,
			-EquippedArmor.LastEquippedHelmetArmor->ArmorConfig.f_CritDamage,
			-EquippedArmor.LastEquippedHelmetArmor->ArmorConfig.f_Toughness,
			-EquippedArmor.LastEquippedHelmetArmor->ArmorConfig.f_Armor);
		break;
	case 2:
		EquippedArmor.LastEquippedChestArmor = EquippedArmor.EquippedChestArmor;
		AddArmorToInventory(EquippedArmor.EquippedChestArmor);
		EquippedArmor.EquippedChestArmor = nullptr;
		AddStats(
			-EquippedArmor.LastEquippedChestArmor->ArmorConfig.f_Vitality,
			-EquippedArmor.LastEquippedChestArmor->ArmorConfig.f_Accuracy,
			-EquippedArmor.LastEquippedChestArmor->ArmorConfig.f_CritChance,
			-EquippedArmor.LastEquippedChestArmor->ArmorConfig.f_CritDamage,
			-EquippedArmor.LastEquippedChestArmor->ArmorConfig.f_Toughness,
			-EquippedArmor.LastEquippedChestArmor->ArmorConfig.f_Armor);
		break;
	case 3:
		EquippedArmor.LastEquippedLegArmor = EquippedArmor.EquippedLegArmor;
		AddArmorToInventory(EquippedArmor.EquippedLegArmor);
		EquippedArmor.EquippedLegArmor = nullptr;
		AddStats(
			-EquippedArmor.LastEquippedLegArmor->ArmorConfig.f_Vitality,
			-EquippedArmor.LastEquippedLegArmor->ArmorConfig.f_Accuracy,
			-EquippedArmor.LastEquippedLegArmor->ArmorConfig.f_CritChance,
			-EquippedArmor.LastEquippedLegArmor->ArmorConfig.f_CritDamage,
			-EquippedArmor.LastEquippedLegArmor->ArmorConfig.f_Toughness,
			-EquippedArmor.LastEquippedLegArmor->ArmorConfig.f_Armor);
		break;

	case 4:
		EquippedArmor.LastEquippedFeetArmor = EquippedArmor.EquippedFeetArmor;
		AddArmorToInventory(EquippedArmor.EquippedFeetArmor);
		EquippedArmor.EquippedFeetArmor = nullptr;
		AddStats(
			-EquippedArmor.LastEquippedFeetArmor->ArmorConfig.f_Vitality,
			-EquippedArmor.LastEquippedFeetArmor->ArmorConfig.f_Accuracy,
			-EquippedArmor.LastEquippedFeetArmor->ArmorConfig.f_CritChance,
			-EquippedArmor.LastEquippedFeetArmor->ArmorConfig.f_CritDamage,
			-EquippedArmor.LastEquippedFeetArmor->ArmorConfig.f_Toughness,
			-EquippedArmor.LastEquippedFeetArmor->ArmorConfig.f_Armor);
		break;

	case 5:
		EquippedArmor.LastEquippedEnergyShield = EquippedArmor.EquippedEnergyShield;
		AddArmorToInventory(EquippedArmor.EquippedEnergyShield);
		EquippedArmor.EquippedEnergyShield = nullptr;
		AddStats(
			-EquippedArmor.LastEquippedEnergyShield->ArmorConfig.f_Vitality,
			-EquippedArmor.LastEquippedEnergyShield->ArmorConfig.f_Accuracy,
			-EquippedArmor.LastEquippedEnergyShield->ArmorConfig.f_CritChance,
			-EquippedArmor.LastEquippedEnergyShield->ArmorConfig.f_CritDamage,
			-EquippedArmor.LastEquippedEnergyShield->ArmorConfig.f_Toughness,
			-EquippedArmor.LastEquippedEnergyShield->ArmorConfig.f_Armor);
		break;
	default:
		break;
	}
}

AArmor_Base* UInventory::GetEquippedHeadPiece()
{
	return EquippedArmor.EquippedHelmetArmor;
}

AArmor_Base* UInventory::GetEquippedChestPiece()
{
	return EquippedArmor.EquippedChestArmor;
}

AArmor_Base*  UInventory::GetEquippedLegPiece()
{
	return EquippedArmor.EquippedLegArmor;
}

AArmor_Base* UInventory::GetEquippedFeetPiece()
{
	return EquippedArmor.EquippedFeetArmor;
}

AArmor_Base* UInventory::GetEquippedEnergyShield()
{
	return EquippedArmor.EquippedEnergyShield;
}

void UInventory::AddStats(float _f_Vitality, float _f_Accuracy, float _f_CritChance, float _f_CritDamage, float _f_Toughness, float _f_Armor, float _f_LifeLeach)
{
	if (PlayerStats)
	{
		PlayerStats->SetMaxArmor(PlayerStats->GetMaxArmor() + _f_Armor);
		PlayerStats->SetVitality(PlayerStats->GetVitality() + _f_Vitality);
		PlayerStats->SetAccuracy(PlayerStats->GetAccuracy() + _f_Accuracy);
		PlayerStats->SetCritChance(PlayerStats->GetCritChance() + _f_CritChance);
		PlayerStats->SetCritDamage(PlayerStats->GetCritDamage() + _f_CritDamage);
		PlayerStats->SetToughness(PlayerStats->GetToughness() + _f_Toughness);
		PlayerStats->SetLifeLeach(PlayerStats->GetLifeLeach() + _f_LifeLeach);
	}
	else
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, "No Player stats, cannot add stats! - Inventory_EquipArmor");
}
