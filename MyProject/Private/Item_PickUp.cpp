// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Public/Item_PickUp.h"

AItem_PickUp::AItem_PickUp()
{
	PrimaryActorTick.bCanEverTick = false;
	Name = "Unknown Item";

	CollisionComp = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionComp"));
	RootComponent = CollisionComp;
	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	if (CollisionComp)
	{
		RootComponent = CollisionComp;
		Mesh->SetupAttachment(CollisionComp);
	}
}

void AItem_PickUp::BeginPlay()
{
	Super::BeginPlay();
}

void AItem_PickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AItem_PickUp::ProcessItemPickUp()
{
}

