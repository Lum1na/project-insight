// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "AmmoDrop.h"
#include "FPS_Character.h"


AAmmoDrop::AAmmoDrop()
{																						
	PickupRadius = CreateDefaultSubobject<USphereComponent>(TEXT("Pickup Sphere"));
	PickupRadius->SetupAttachment(RootComponent);
	PickupRadius->OnComponentBeginOverlap.AddDynamic(this, &AAmmoDrop::OnBeginOverlap);
}

void AAmmoDrop::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	/** Check if overlapActor is the player*/
	AFPS_Character* Player = Cast<AFPS_Character>(OtherActor);
	if (Player)
	{
		/** if the overlappingActor is the Player add ammo to the currently used weapon*/
		int* Ammo = Player->GetPlayerStats()->GetCurrentAmmoInUse();
		if (Ammo)
		{
			if (*Ammo < 300)
			{
				UAudioComponent* AC = NULL;
				if (PickupSound)
				{
					AC = UGameplayStatics::SpawnSoundAttached(PickupSound, GetRootComponent());
				}
				*Ammo += i_AmountOfAmmo;
				*Ammo = FMath::Clamp(*Ammo, 0, 300);
				Destroy(); 
			}
		}
	}
}

