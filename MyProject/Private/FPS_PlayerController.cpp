// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "FPS_Character.h"
#include "MyProjectGameModeBase.h"
#include "FPS_PlayerController.h"


AFPS_PlayerController::AFPS_PlayerController()
{

}

void AFPS_PlayerController::AddLastOpenedHudElement(EHudElements _LastOpenedHudElement)
{
	LastOpenedHudElementsArray.Add(_LastOpenedHudElement);
}

void AFPS_PlayerController::CloseLastHudElement()
{
	UE_LOG(LogTemp, Warning, TEXT("Closing last HUD Element"));
	switch (GetLastOpenedHudElement())
	{
	case Inventory:
		ControlledPawn->GetPlayerInventory()->ToggleInventoryShowing();
		UE_LOG(LogTemp, Warning, TEXT("Closing Inventory"));
		break;
	case  Characterstats:
		ControlledPawn->GetPlayerStats()->ToggleCharacterStatsShowing();
		UE_LOG(LogTemp, Warning, TEXT("Closing Characterstats"));
		break;
	case AfterRoundWindow:
		AMyProjectGameModeBase* Gamemode = Cast<AMyProjectGameModeBase>(GetWorld()->GetAuthGameMode());
		Gamemode->ToggleRoundSelection();
		UE_LOG(LogTemp, Warning, TEXT("Closing AfterRoundWindow"));
		break;
	}
}

void AFPS_PlayerController::SetControlledPawn(AFPS_Character* _ControlledPawn)
{
	ControlledPawn = _ControlledPawn;
}

void AFPS_PlayerController::RemoveHudElementFromList(EHudElements _HudElementToRemove)
{
	for (int i = 0; i < LastOpenedHudElementsArray.Num(); i++)
	{
		if (LastOpenedHudElementsArray[i] == _HudElementToRemove)
		{
			LastOpenedHudElementsArray.RemoveAt(i);
		}
	}
}

EHudElements AFPS_PlayerController::GetLastOpenedHudElement()
{
	if (LastOpenedHudElementsArray.Num() > 0)
		return LastOpenedHudElementsArray.Last();

	return EHudElements::Nothing;
}
