// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Armor_EnergyShield.h"
#include "FPS_Character.h"
#include "MyProjectGameModeBase.h"


void AArmor_EnergyShield::GenerateStats()
{
	ArmorConfig.f_Accuracy = 0.0f;
	ArmorConfig.f_Armor = 0.0f;
	ArmorConfig.f_CritChance = 0.0f;
	ArmorConfig.f_CritDamage = 0.0f;
	ArmorConfig.f_Toughness = 0.0f;
	ArmorConfig.f_Vitality = 0.0f;

	AFPS_Character* Player = Cast<AFPS_Character>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (Player)
	{
		int32 Playerlevel = Player->GetPlayerStats()->GetPlayerLevel();

		AMyProjectGameModeBase* GameMode = Cast<AMyProjectGameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			int round = GameMode->GetCurrentRound();

			int i_AmountOfStats = 0;
			if (FMath::RandRange(0, 1) == 1)
			{
				ArmorConfig.f_Armor = FMath::RandRange(f_ArmorBaseStat + Playerlevel + (round * f_ArenaRoundModifier) * f_ArmorMultiplier,
					f_ArmorBaseStat + 1 + Playerlevel + (round * f_ArenaRoundModifier) * (f_MaxStatMultiplier * f_ArmorMultiplier));
				i_AmountOfStats++;
			}
			if (FMath::RandRange(0, 1) == 1 && i_AmountOfStats < i_MaxAmountOfStats)
			{
				ArmorConfig.f_Vitality = FMath::RandRange(f_VitalityBaseStat + Playerlevel + (round * f_ArenaRoundModifier) * f_ArmorMultiplier,
					f_VitalityBaseStat + 1 + Playerlevel + (round * f_ArenaRoundModifier) * (f_MaxStatMultiplier * f_ArmorMultiplier));
				i_AmountOfStats++;
			}

			if (FMath::RandRange(0, 1) == 1 && i_AmountOfStats < i_MaxAmountOfStats)
			{
				ArmorConfig.f_Toughness = FMath::RandRange(f_ToughnessBaseStat + Playerlevel + (round * f_ArenaRoundModifier) * f_ArmorMultiplier,
					f_ToughnessBaseStat + 1 + Playerlevel + (round * f_ArenaRoundModifier) * (f_MaxStatMultiplier * f_ArmorMultiplier));
				i_AmountOfStats++;
			}
		}
	}
}

