// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Rocket.h"
#include "Weapon_BaseClass.h"
#include "RocketLauncher.h"
#include "Base_EnemyCharacter.h"
#include "FPS_Character.h"
#include "GameFramework/ProjectileMovementComponent.h"


ARocket::ARocket(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	CollisionComp = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("CollisionComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &ARocket::OnBeginOverlap);
	RootComponent = CollisionComp;

	ProjectileMovement = ObjectInitializer.CreateDefaultSubobject<UProjectileMovementComponent>(this, TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->ProjectileGravityScale = 0;
	ProjectileMovement->InitialSpeed = 5000.0f;
	ProjectileMovement->MaxSpeed = 6000.0f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;

	InitialLifeSpan = 5.0f;
}
void ARocket::BeginPlay()
{
	Super::BeginPlay();
}

void ARocket::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARocket::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		ABase_EnemyCharacter* Enemy = Cast<ABase_EnemyCharacter>(OtherActor);
		if (Enemy)
		{
			ARocketLauncher* ParentWeapon = Cast<ARocketLauncher>(GetOwner());	

			OwningPlayer = Cast<AFPS_Character>(ParentWeapon->GetOwningPawn());
			if (OwningPlayer)
			{
				int32 TotalDamage = OwningPlayer->CurrentWeapon->WeaponConfig.Damage;
				float CritNumb = FMath::RandRange(0, 100);
				if (CritNumb <= OwningPlayer->GetPlayerStats()->GetCritChance())
				{
					TotalDamage += (f_Damage / 100) * OwningPlayer->GetPlayerStats()->GetCritDamage();
				}
				UE_LOG(LogTemp, Warning, TEXT("Hit Enemy|Rocket : OnBeginOverlap"));
				float f_HealthFromLifeLeach = (Enemy->Damage(TotalDamage, OwningPlayer) / 100) * OwningPlayer->GetPlayerStats()->GetLifeLeach();
				OwningPlayer->GetPlayerStats()->ChangeCurrentHealth(f_HealthFromLifeLeach);
			}
			else
				UE_LOG(LogTemp, Warning, TEXT("No Owner|Rocket : OnBeginOverlap"));
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("No Enemy|Rocket : OnBeginOverlap"));

		if (OtherComp->IsSimulatingPhysics() == true)
		{
			OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
		}
		/** Spawn MuzzleFlash Particle system*/
		ImpactPSC = UGameplayStatics::SpawnEmitterAttached(ImpactFX, OverlappedComp);
		if (ImpactPSC)
		{
			ImpactPSC->SetRelativeLocation(SweepResult.Location);
			ImpactPSC->bAutoDestroy = true;
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("No Muzzle ParticleSystem!| Weapon_BaseClass : InstantFire"));
		Destroy();
	}
}
