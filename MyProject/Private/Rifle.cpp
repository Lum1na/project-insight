// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "FPS_Character.h"
#include "MyProjectGameModeBase.h"
#include "Rifle.h"

ARifle::ARifle()
{
	WeaponConfig.ClipSize = 30;
	WeaponConfig.ShotCost = 1;
	WeaponConfig.ReloadTime = 2.5f;
	WeaponConfig.WeaponSpread = 3.0f;
	WeaponConfig.WeaponRange = 10000.0f;
	WeaponConfig.ShotDelay = .17;
	WeaponConfig.CurrentClipAmmo = WeaponConfig.ClipSize;

	AmmoType = EWeaponAmmoType::AmmoType::ERifleAmmo;
	ProjectileType = EWeaponProjectile::EBullet;
	WeaponConfig.FireType = EWeaponFireType::EAutoFire;
	Name = "Assaultrifle";
}

void ARifle::GenerateStats(float _f_WeaponStatsModifier)
{
	AFPS_Character* Player = Cast<AFPS_Character>(GetWorld()->GetFirstPlayerController()->GetPawn());
	int PlayerLevel = Player->GetPlayerStats()->GetPlayerLevel();

	AMyProjectGameModeBase* GameMode = Cast<AMyProjectGameModeBase>(GetWorld()->GetAuthGameMode());
	int round = GameMode->GetCurrentRound();
	int ArenaroundMultiplier = GameMode->GetArenaRoundMultiplier();

	WeaponConfig.FireType = (FMath::RandBool() == true) ? EWeaponFireType::EAutoFire : EWeaponFireType::EBurstFire;
	AmmoType = EWeaponAmmoType::AmmoType::ERifleAmmo;
	if (WeaponConfig.FireType == EWeaponFireType::EBurstFire)
	{
		WeaponConfig.i_ShotsPerBurst = FMath::RandRange(StatGenerationData.MinShotsPerBurst, StatGenerationData.MaxShotsPerBurst);
		WeaponConfig.f_BurstShotDelay = FMath::RandRange(StatGenerationData.MinBurstShotDelay, StatGenerationData.MaxBurstShotDelay);
		WeaponConfig.ShotDelay = FMath::RandRange(StatGenerationData.MinBurstDelay, StatGenerationData.MaxBurstDelay);
	}
	else
	{
		WeaponConfig.f_BurstShotDelay = 0.0f;
		WeaponConfig.i_ShotsPerBurst = 0;
		WeaponConfig.ShotDelay = FMath::RandRange(StatGenerationData.MinShotDelay, StatGenerationData.MaxShotDelay);
	}

	WeaponConfig.AmountOfPellets = 0;
	WeaponConfig.ClipSize = FMath::RandRange(StatGenerationData.MinClipSize, StatGenerationData.MaxClipSize);

	WeaponConfig.Damage = FMath::RandRange(StatGenerationData.f_WeaponBaseDamage + PlayerLevel + (round * ArenaroundMultiplier) * _f_WeaponStatsModifier,
		StatGenerationData.f_WeaponBaseDamage + 1 + PlayerLevel + (round * ArenaroundMultiplier) *  (f_MaxStatModifier * _f_WeaponStatsModifier));

	WeaponConfig.ReloadTime = FMath::RandRange(StatGenerationData.MinReloadTime, StatGenerationData.MaxReloadTime);
	WeaponConfig.ShotCost = 1;
	WeaponConfig.WeaponRange = 10000.0f;
	WeaponConfig.WeaponSpread = FMath::RandRange(StatGenerationData.MinWeaponSpread, StatGenerationData.MaxWeaponSpread);
	WeaponConfig.CurrentClipAmmo = WeaponConfig.ClipSize;
}