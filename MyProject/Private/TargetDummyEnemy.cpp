// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"

#include "TargetDummyEnemy.h"

ATargetDummyEnemy::ATargetDummyEnemy()
{
	PrimaryActorTick.bCanEverTick = false;
	DummyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	CollisionComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Comp"));
	DummyMesh->AttachToComponent(CollisionComp,FAttachmentTransformRules::KeepRelativeTransform);
	RootComponent = CollisionComp;
}

void ATargetDummyEnemy::BeginPlay()
{
	Super::BeginPlay();	
	GameMode = Cast<ATutorialGameMode>(GetWorld()->GetAuthGameMode());
	GameMode->AddTarget();
}

void ATargetDummyEnemy::HitTarget()
{
	if (bHitTarget == false)
	{
		if (GameMode)
			GameMode->AddShotTargetCounter();

		SetActorRotation(HitMeshRotation);
		SetActorLocation(HitMeshLocation);
		bHitTarget = true;
	}
}

void ATargetDummyEnemy::ResetTarget()
{
	if (bHitTarget == true)
	{
		bHitTarget = false;
		SetActorLocation(DefaultMeshLocation);
		SetActorRotation(DefaultMeshRotation);
	}
}


