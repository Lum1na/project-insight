// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Public/FPS_Character.h"
#include "MyProjectGameModeBase.h"
#include "Shotgun.h"


AShotgun::AShotgun()
{
	WeaponConfig.ClipSize = 12;
	WeaponConfig.ShotCost = 1;
	WeaponConfig.ReloadTime = 2.5f;
	WeaponConfig.WeaponSpread = 20.0f;
	WeaponConfig.WeaponRange = 3000.0f;
	WeaponConfig.ShotDelay = 0.0f;
	WeaponConfig.CurrentClipAmmo = WeaponConfig.ClipSize;
	AmmoType = EWeaponAmmoType::AmmoType::EShotgunAmmo;
	ProjectileType = EWeaponProjectile::ESpread;
	WeaponConfig.FireType = EWeaponFireType::ESingleShot;
	Name = "Shotgun";
}

void AShotgun::GenerateStats(float _f_WeaponStatsModifier)
{
	AFPS_Character* Player = Cast<AFPS_Character>(GetWorld()->GetFirstPlayerController()->GetPawn());
	int PlayerLevel = Player->GetPlayerStats()->GetPlayerLevel();

	AMyProjectGameModeBase* GameMode = Cast<AMyProjectGameModeBase>(GetWorld()->GetAuthGameMode());
	int round = GameMode->GetCurrentRound();
	int ArenaroundMultiplier = GameMode->GetArenaRoundMultiplier();

	WeaponConfig.ClipSize = FMath::RandRange(StatGenerationData.MinClipSize, StatGenerationData.MaxClipSize);
	WeaponConfig.AmountOfPellets = FMath::RandRange(StatGenerationData.MinAmountOfPellets, WeaponConfig.ClipSize);
	WeaponConfig.ReloadTime = FMath::RandRange(StatGenerationData.MinReloadTime, StatGenerationData.MaxReloadTime);
	int shotcost = (FMath::RandRange(0, 1) == 1) ? 1 : WeaponConfig.AmountOfPellets;
	WeaponConfig.ShotCost = shotcost;

	WeaponConfig.Damage = FMath::RandRange(StatGenerationData.f_WeaponBaseDamage + PlayerLevel + (round * ArenaroundMultiplier) * _f_WeaponStatsModifier,
		StatGenerationData.f_WeaponBaseDamage + 1 + PlayerLevel + (round * ArenaroundMultiplier) *  (f_MaxStatModifier * _f_WeaponStatsModifier));

	WeaponConfig.ShotDelay = FMath::RandRange(StatGenerationData.MinShotDelay, StatGenerationData.MaxShotDelay);
	WeaponConfig.WeaponRange = 5000.0f;
	WeaponConfig.WeaponSpread = FMath::RandRange(StatGenerationData.MinWeaponSpread, StatGenerationData.MaxWeaponSpread);
	WeaponConfig.CurrentClipAmmo = WeaponConfig.ClipSize;
}

