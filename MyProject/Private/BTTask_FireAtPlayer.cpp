// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"	
#include "AI_BaseEnemyController.h"
#include "Base_EnemyCharacter.h"
#include "AI_Weapon.h"
#include "FPS_Character.h"
#include "BTTask_FireAtPlayer.h"
#include "Player_Controller.h"
#include "Kismet/KismetMathLibrary.h"


EBTNodeResult::Type UBTTask_FireAtPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (OwnerComp.GetAIOwner())
	{
		ABase_EnemyCharacter* AIC = Cast<ABase_EnemyCharacter>(OwnerComp.GetAIOwner()->GetPawn());

		if (AIC)
		{
			/** Getting the PlayerCharacter*/
			AFPS_Character*	PlayerCharacter = Cast<AFPS_Character>(AIC->PlayerCharacter);

			if (PlayerCharacter && PlayerCharacter->b_IsDead == false)
			{
				ShootAtPlayer(OwnerComp, AIC, PlayerCharacter);
				return EBTNodeResult::Succeeded;
			}
		}
		else
			UE_LOG(LogTemp, Error, TEXT("No Playercharacter. BTTask_FireAtPlayer -> ExecuteTask"));
	}
	else
		UE_LOG(LogTemp, Error, TEXT("No AIOwner. BTTask_FireAtPlayer -> ExecuteTask"));
	return EBTNodeResult::Failed;
}

void UBTTask_FireAtPlayer::ShootAtPlayer(UBehaviorTreeComponent& OwnerComp, ABase_EnemyCharacter* _AIC, AFPS_Character* _PlayerCharacter)
{
	if (b_CanFireWeapon)
	{
		b_CanFireWeapon = false;
		AAI_Weapon* AIWeapon = _AIC->AI_Weapon;
		if (AIWeapon)
		{
			/** Generating Accuracy for the Shot*/
			const int32 RandomSeed = FMath::Rand();
			FRandomStream WeaponRandomStream(RandomSeed);
			float SpreadCone;
			if (_AIC)
			{

				float Distance = FVector::Dist(_AIC->GetActorLocation(), _PlayerCharacter->GetActorLocation());
				if (Distance < 50)
					SpreadCone = FMath::DegreesToRadians(AIWeapon->WeaponConfig.f_Accuarcy);
				else
					SpreadCone = FMath::DegreesToRadians(AIWeapon->WeaponConfig.f_Accuarcy / 2);
			}
			else
				SpreadCone = FMath::DegreesToRadians(AIWeapon->WeaponConfig.f_Accuarcy / 2);



			/** Getting Direction and Origin of the Shot*/
			FRotator AimDir;
			FVector StartTrace;
			OwnerComp.GetAIOwner()->GetPlayerViewPoint(StartTrace, AimDir);

			FRotator PlayerRot = UKismetMathLibrary::FindLookAtRotation(AimDir.Vector(), _PlayerCharacter->GetActorLocation());
			StartTrace = FVector(StartTrace.X, StartTrace.Y, StartTrace.Z);
			const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir.Vector(), SpreadCone, SpreadCone);
			const FVector EndTrace = FVector(StartTrace.X, StartTrace.Y, _PlayerCharacter->GetActorLocation().Z) + ShootDir * 100000.0f;

			/** Setting Paramaters for the Raycast*/
			static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
			FCollisionQueryParams  TraceParams(WeaponFireTag, true, _AIC);
			TraceParams.bTraceAsyncScene = true;
			TraceParams.bReturnPhysicalMaterial = true;
			TraceParams.AddIgnoredActor(_AIC);
			FHitResult Hit(ForceInit);

			/** Firing the Raycast and a debug line*/
			GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_PhysicsBody, TraceParams);
			PlayWeaponSound(AIWeapon->FireSound, _AIC);

			/** Checking if the Hit Actor was the Player. If so then damage him*/
			AFPS_Character*  Player = Cast<AFPS_Character>(Hit.GetActor());
			if (Player)
			{
				Player->Take_Damage(AIWeapon->WeaponConfig.f_Damage, Hit.Location);
			}

			AIWeapon->WeaponConfig.i_CurrentAmmoInClip--;

			if (AIWeapon->WeaponConfig.i_CurrentAmmoInClip <= 0)
			{
				Reload();
				GetWorld()->GetTimerManager().SetTimer(ReloadTimerHandle, this, &UBTTask_FireAtPlayer::FinishReload, AIWeapon->WeaponConfig.f_Reloadtime, false);
			}
			/** Start the Shotdelay cooldown*/
			if (b_IsWeaponCooldownRunning == false)
			{
				b_IsWeaponCooldownRunning = true;
				GetWorld()->GetTimerManager().SetTimer(ShotDelayTimerHandle, this, &UBTTask_FireAtPlayer::ResetShotDelayTimer, AIWeapon->WeaponConfig.f_ShotDelay, false);
			}
		}
		else
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "AI has no Weapon! - Add a Default Weapon in the AI Blueprint");
	}
}

/** Reset the Shotdelay cooldown and let the AI Fire again*/
void UBTTask_FireAtPlayer::ResetShotDelayTimer()
{
	b_CanFireWeapon = true;
	b_IsWeaponCooldownRunning = false;
}

void UBTTask_FireAtPlayer::Reload()
{
	b_CanFireWeapon = false;
}

void UBTTask_FireAtPlayer::FinishReload()
{
	b_CanFireWeapon = true;
}

UAudioComponent* UBTTask_FireAtPlayer::PlayWeaponSound(USoundCue * Sound, APawn* Parent)
{
	UAudioComponent* AC = NULL;
	if (Sound && Parent)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, Parent->GetRootComponent());
	}
	return AC;
}