// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "Base_EnemyCharacter.h"
#include "FPS_Character.h"
#include "AI_BaseEnemyController.h"

AAI_BaseEnemyController::AAI_BaseEnemyController()
{
	BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard Component"));
	BehaviorComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("Behavior Component"));
	bWantsPlayerState = true;
}

void AAI_BaseEnemyController::BeginPlay()
{
	Super::BeginPlay();
}

void AAI_BaseEnemyController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);

	/** Get the AICharacter*/
	if (!AICharacter)
	{
		AICharacter = Cast<ABase_EnemyCharacter>(InPawn);
		AICharacter->SetController(this);
	}

	if (AICharacter && AICharacter->BehaviorTree)
	{
		/** Initialize Blackboard with the Blackboard the Assigned Behavior tree uses*/
		BlackboardComponent->InitializeBlackboard(*AICharacter->BehaviorTree->BlackboardAsset);

		EnemyKeyID = BlackboardComponent->GetKeyID("Target");

		/** Starts the Behaviortree*/
		BehaviorComponent->StartTree(*(AICharacter->BehaviorTree));
	}

	if (!PlayerCharacter)
	{
		/** Getting the PlayerCharacter*/
		if (GetWorld()->GetFirstPlayerController() && GetWorld()->GetFirstPlayerController()->GetPawn() != nullptr)
			PlayerCharacter = Cast<AFPS_Character>(GetWorld()->GetFirstPlayerController()->GetPawn());	
	}

	if (PlayerCharacter)
	{
		/** Sets the Target*/
		BehaviorComponent->GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(EnemyKeyID, PlayerCharacter);
		//GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, "ENEMY IS HERE");
	}  
}

void AAI_BaseEnemyController::StartBehavior(bool b_StartBehavior)
{
	if (b_StartBehavior == false)
	{
		BehaviorComponent->StopTree();
	}
	else if (b_StartBehavior)
		BehaviorComponent->StartTree(*(AICharacter->BehaviorTree));

}
