//Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Weapon_BaseClass.h"
#include "Item_PickUp.h"
#include "FPS_Character.h"
#include "../Public/WeaponSpawner.h"

UWeaponSpawner::UWeaponSpawner()
{
	PrimaryComponentTick.bCanEverTick = false;

	f_WeaponStatsModifier = 1.2;

	Pool_Rifle_Weight = 50;
	Pool_Smg_Weight = Pool_Rifle_Weight + 50;
	Pool_Shotgun_Weight = Pool_Smg_Weight + 50;
	Pool_RocketLauncher_Weight = Pool_Shotgun_Weight + 50;

	Armor_Chestpiece_Weight = 50;
	Armor_Helmet_Weight = Armor_Chestpiece_Weight + 50;
	Armor_Legpiece_Weight = Armor_Legpiece_Weight + 50;
	Armor_Feet_Weight = Armor_Legpiece_Weight + 50;
	Armor_EnergyShield_Weight = Armor_Feet_Weight + 50;

	Weapons_Pool_Weight = 0.5f;
	Armor_Weight = Weapons_Pool_Weight + 0.5f;

#pragma region ArmorTypes

	static ConstructorHelpers::FObjectFinder<UClass> ChestArmorBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Armor/BP_Armor_Chestpiece.BP_Armor_Chestpiece_C'"));
	if (ChestArmorBP.Object)
	{
		ChestArmor = ChestArmorBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> FeetArmorBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Armor/BP_Armor_Feet.BP_Armor_Feet_C'"));
	if (FeetArmorBP.Object)
	{
		FeetArmor = FeetArmorBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> HeadArmorBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Armor/BP_Armor_HeadPiece.BP_Armor_HeadPiece_C'"));
	if (HeadArmorBP.Object)
	{
		HeadArmor = HeadArmorBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> LegArmorBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Armor/BP_Armor_Legpiece.BP_Armor_Legpiece_C'"));
	if (LegArmorBP.Object)
	{
		LegArmor = LegArmorBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> EnergyShieldBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Armor/BP_Armor_EnergyShield.BP_Armor_EnergyShield_C'"));
	if (EnergyShieldBP.Object)
	{
		EnergyShield = EnergyShieldBP.Object;
	}
#pragma endregion

#pragma region WeaponTypes
	static ConstructorHelpers::FObjectFinder<UClass> SmgBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Weapon/BP_SMG.BP_SMG_C'"));
	if (SmgBP.Object)
	{
		SmgSpawn = SmgBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> RifleBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Weapon/BP_Rifle.BP_Rifle_C'"));
	if (RifleBP.Object)
	{
		RifleSpawn = RifleBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> ShotgunBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Weapon/BP_Shotgun.BP_Shotgun_C'"));
	if (ShotgunBP.Object)
	{
		ShotgunSpawn = ShotgunBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> RLBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Weapon/BP_RocketLauncher.BP_RocketLauncher_C'"));
	if (RLBP.Object)
	{
		RocketLauncherSpawn = RLBP.Object;
	}
#pragma endregion

#pragma region Pickups
	static ConstructorHelpers::FObjectFinder<UClass> AmmoDropBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/BP_AmmoDrop.BP_AmmoDrop_C'"));
	if (ChestArmorBP.Object)
	{
		AmmoDrop = AmmoDropBP.Object;
	}
	static ConstructorHelpers::FObjectFinder<UClass> HealthDropBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/BP_HealthDrop.BP_HealthDrop_C'"));
	if (ChestArmorBP.Object)
	{
		HealthDrop = HealthDropBP.Object;
	}
#pragma endregion 
}

void UWeaponSpawner::BeginPlay()
{
	Super::BeginPlay();
}

void UWeaponSpawner::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UWeaponSpawner::ChoosePoolToSpawn()
{
	float f_DropChance = FMath::RandRange(0, 100);
	if (f_DropChance <= f_ChanceOfWeaponOrArmorDrop)
	{
		float PoolValue = FMath::RandRange(0.0f, Armor_Weight);
		if (PoolValue <= Weapons_Pool_Weight)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, "Dropped Weapon");
			DropWeapon();
		}
		else if (PoolValue < Armor_Weight)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, "Dropped Armor");
			DropArmor();
		}
	}

	float ItemDropValue = FMath::RandRange(0, 100);
	if (ItemDropValue <= Pool_PickUp_Weight)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, "Dropped Pickup");
		DropItem();
	}
}

void UWeaponSpawner::DropWeapon()
{
	float PoolValue = FMath::RandRange(0.0f, Pool_RocketLauncher_Weight);
	if (PoolValue < Pool_RocketLauncher_Weight && PoolValue > Pool_Shotgun_Weight)
	{
		SpawnWeapon(RocketLauncherSpawn);
	}
	else if (PoolValue < Pool_Shotgun_Weight && PoolValue > Pool_Smg_Weight)
	{
		SpawnWeapon(ShotgunSpawn);
	}
	else if (PoolValue < Pool_Smg_Weight && PoolValue > Pool_Rifle_Weight)
	{
		SpawnWeapon(SmgSpawn);
	}
	else if (PoolValue < Pool_Rifle_Weight)
	{
		SpawnWeapon(RifleSpawn);
	}
}

void UWeaponSpawner::SpawnWeapon(TSubclassOf<class AWeapon_BaseClass> WeapToSpawn)
{
	WeaponToSpawn = WeapToSpawn;
	if (GetWorld())
	{
		FActorSpawnParameters SpawnParams;
		if (WeapToSpawn)
		{
			AWeapon_BaseClass* Weapon = GetWorld()->SpawnActor<AWeapon_BaseClass>(WeapToSpawn, GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation(), SpawnParams);
			if (Weapon)
			{
				Weapon->CollisionComp->IgnoreActorWhenMoving(GetOwner(), true);
				FVector randomDirection(FMath::RandRange(-50, 50), FMath::RandRange(-50, 50), FMath::RandRange(50, 200));
				Weapon->CollisionComp->AddImpulseAtLocation(randomDirection, Weapon->GetActorLocation());
				Weapon->GenerateStats(f_WeaponStatsModifier);
			}
		}
	}
}

void UWeaponSpawner::DropArmor()
{
	float PoolValue = FMath::RandRange(0.0f, Armor_EnergyShield_Weight);
	if (PoolValue <= Armor_EnergyShield_Weight && PoolValue > Armor_Feet_Weight)
		SpawnArmor(EnergyShield);

	else if (PoolValue < Armor_Feet_Weight && PoolValue > Armor_Legpiece_Weight)
		SpawnArmor(FeetArmor);

	else if (PoolValue < Armor_Legpiece_Weight && PoolValue > Armor_Helmet_Weight)
		SpawnArmor(LegArmor);

	else if (PoolValue < Armor_Helmet_Weight && PoolValue > Armor_Chestpiece_Weight)
		SpawnArmor(HeadArmor);

	else if (PoolValue < Armor_Chestpiece_Weight)
		SpawnArmor(ChestArmor);
}

void UWeaponSpawner::SpawnArmor(TSubclassOf<AArmor_Base> _ArmorToSpawn)
{
	ArmorToSpawn = _ArmorToSpawn;
	if (GetWorld())
	{
		FActorSpawnParameters SpawnParams;
		if (ArmorToSpawn)
		{
			AArmor_Base* Armor = GetWorld()->SpawnActor<AArmor_Base>(ArmorToSpawn, GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation(), SpawnParams);
			if (Armor)
			{
				Armor->CollisionComp->IgnoreActorWhenMoving(GetOwner(), true);
				FVector randomDirection(FMath::RandRange(-50, 50), FMath::RandRange(-50, 50), FMath::RandRange(50, 200));
				Armor->CollisionComp->AddImpulseAtLocation(randomDirection, Armor->GetActorLocation());
				Armor->GenerateStats();
			}
		}
	}
}

void UWeaponSpawner::DropItem()
{
	FMath::RandRange(0, 2) < 1 ? SpawnPickUP(HealthDrop) : SpawnPickUP(AmmoDrop);
}

void UWeaponSpawner::SpawnPickUP(TSubclassOf<AItem_PickUp> Item)
{
	ItemToSpawn = Item;
	if (GetWorld())
	{
		FActorSpawnParameters SpawnParams;
		if (ItemToSpawn)
		{
			AItem_PickUp* Itemm = GetWorld()->SpawnActor<AItem_PickUp>(ItemToSpawn, GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation(), SpawnParams);
			if (Itemm)
			{
				Itemm->CollisionComp->IgnoreActorWhenMoving(GetOwner(), true);
				FVector randomDirection(FMath::RandRange(-50, 50), FMath::RandRange(-50, 50), FMath::RandRange(50, 200));
				Itemm->CollisionComp->AddImpulseAtLocation(randomDirection, Itemm->GetActorLocation());
			}
			else
				UE_LOG(LogTemp, Warning, TEXT("Item Spawn Failed! - WeaponSpawner -> SpawnPickup"));
		}
	}
}
