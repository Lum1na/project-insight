// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "FPS_Character.h"
#include "CombatTextActor.h"


ACombatTextActor::ACombatTextActor()
{
	PrimaryActorTick.bCanEverTick = true;

	ColliderComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Collision Comp"));
	ColliderComp->SetSimulatePhysics(true);
	ColliderComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	RootComponent = ColliderComp;
	CombatText = CreateDefaultSubobject<ATextRenderActor>(TEXT("Combat Text"));
	LifeSpan = 3;
	InitialLifeSpan = LifeSpan;
}

void ACombatTextActor::BeginPlay()
{
	Super::BeginPlay();	
	ColliderComp->AddImpulseAtLocation(FVector::UpVector * 100.0f, GetActorLocation());
	CombatText = GetWorld()->SpawnActor<ATextRenderActor>(ATextRenderActor::StaticClass(), GetActorLocation(), GetActorRotation());
	CombatText->SetLifeSpan(LifeSpan);
	MoveDirection = FVector(FMath::RandRange(-1, 1), FMath::RandRange(-1, 1), FMath::RandRange(0, 1));
}

void ACombatTextActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FVector NewLoc(CombatText->GetActorLocation().X + MoveDirection.X , CombatText->GetActorLocation().Y + MoveDirection.Y, CombatText->GetActorLocation().Z + MoveDirection.Z);
	CombatText->SetActorLocation(NewLoc);
	FRotator LookAt = FRotationMatrix::MakeFromX(Player->GetActorLocation() - GetActorLocation()).Rotator();
	CombatText->SetActorRotation( FRotator(0, LookAt.Yaw,0));
}

void ACombatTextActor::SetText(FString Text, AFPS_Character* _Player)
{
	CombatText->GetTextRender()->SetText( FText::FromString(Text));
	Player = _Player;
}

