// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Public/FPS_Character.h"
#include "MyProjectGameModeBase.h"
#include "RocketLauncher.h"

ARocketLauncher::ARocketLauncher()
{
	WeaponConfig.ClipSize = 12;
	WeaponConfig.ShotCost = 1;
	WeaponConfig.ReloadTime = 2.5f;
	WeaponConfig.WeaponSpread = 3.0f;
	WeaponConfig.WeaponRange = 10000.0f;
	WeaponConfig.ShotDelay = 1.0f;
	WeaponConfig.CurrentClipAmmo = WeaponConfig.ClipSize;


	AmmoType = EWeaponAmmoType::AmmoType::ERocketLauncherAmmo;
	ProjectileType = EWeaponProjectile::EProjectile;
	WeaponConfig.FireType = EWeaponFireType::ESingleShot;
	Name = "Rocket Launcher";
}
void ARocketLauncher::ProjectileFire()
{
	Super::ProjectileFire();

	if (ProjectileClass != NULL)
	{
		/** Spawn MuzzleFlash Particle system*/
		MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, Mesh, "Muzzle");
		if (MuzzlePSC)
		{
			MuzzlePSC->SetRelativeLocation(FVector(0.0f));
			MuzzlePSC->bAutoDestroy = true;
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("No Muzzle ParticleSystem!| Weapon_BaseClass : InstantFire"));


		FVector MFLoc = Mesh->GetSocketLocation("Muzzle");
		FRotator MFRot = Mesh->GetSocketRotation("Muzzle");
		MFRot = FRotator(MFRot.Pitch, MyPawn->GetActorRotation().Yaw, MFRot.Roll);
		FActorSpawnParameters Spawnparams;
		Spawnparams.Owner = this;
		Spawnparams.Instigator = Instigator;
		ARocket* const Rocket = GetWorld()->SpawnActor<ARocket>(ProjectileClass, MFLoc, MFRot, Spawnparams);
	}
}

void ARocketLauncher::GenerateStats(float _f_WeaponStatsModifier)
{
	AFPS_Character* Player = Cast<AFPS_Character>(GetWorld()->GetFirstPlayerController()->GetPawn());
	int PlayerLevel = Player->GetPlayerStats()->GetPlayerLevel();

	AMyProjectGameModeBase* GameMode = Cast<AMyProjectGameModeBase>(GetWorld()->GetAuthGameMode());
	int round = GameMode->GetCurrentRound();
	int ArenaroundMultiplier = GameMode->GetArenaRoundMultiplier();

	WeaponConfig.AmountOfPellets = 0;
	WeaponConfig.ClipSize = FMath::RandRange(StatGenerationData.MinClipSize, StatGenerationData.MaxClipSize);

	WeaponConfig.Damage = FMath::RandRange(StatGenerationData.f_WeaponBaseDamage + PlayerLevel + (round * ArenaroundMultiplier) * _f_WeaponStatsModifier,
		StatGenerationData.f_WeaponBaseDamage + 1 + PlayerLevel + (round * ArenaroundMultiplier) *  (f_MaxStatModifier * _f_WeaponStatsModifier));

	WeaponConfig.ReloadTime = FMath::RandRange(StatGenerationData.MinReloadTime, StatGenerationData.MaxReloadTime);
	WeaponConfig.ShotCost = 1;
	WeaponConfig.ShotDelay = FMath::RandRange(StatGenerationData.MinShotDelay, StatGenerationData.MaxShotDelay);
	WeaponConfig.WeaponRange = 10000.0f;
	WeaponConfig.WeaponSpread = FMath::RandRange(StatGenerationData.MinWeaponSpread, StatGenerationData.MaxWeaponSpread);
	WeaponConfig.CurrentClipAmmo = WeaponConfig.ClipSize;
}
