// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"	
#include "AI_BaseEnemyController.h"
#include "Base_EnemyCharacter.h"
#include "FPS_Character.h"
#include "BTTask_MoveToPlayer.h"


EBTNodeResult::Type UBTTask_MoveToPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (OwnerComp.GetAIOwner())
	{ 
		ABase_EnemyCharacter* AIC = Cast<ABase_EnemyCharacter>(OwnerComp.GetAIOwner()->GetPawn());
		if (AIC)
		{
			AFPS_Character* PlayerCharacter = Cast<AFPS_Character>(AIC->PlayerCharacter);
			if (PlayerCharacter)
			{
				if (AIC && AIC->b_PlayerInSight == false)
				{
					if (OwnerComp.GetAIOwner()->GetMoveStatus() != EPathFollowingStatus::Moving)
					{
						FVector Location;
						//bool Random = GetRandomPointInRadius(PlayerCharacter->GetActorLocation(), 1000, Location);
						//if (Random)
						//	OwnerComp.GetAIOwner()->MoveToLocation(Location, 50.0f, true, true, true, true, 0, false);
						//else
							OwnerComp.GetAIOwner()->MoveToActor(PlayerCharacter, 10.0f, true, true, true, 0, false);
					}
				}
				if (AIC && AIC->b_PlayerInSight == true)
				{
					OwnerComp.GetAIOwner()->StopMovement();
					return EBTNodeResult::Succeeded;
				}
			}
			else
				UE_LOG(LogTemp, Error, TEXT("No PlayerCharacter. BTTask_MoveToPlayer -> ExecuteTask"));
		}
		else
			UE_LOG(LogTemp, Error, TEXT("No AIOwner. BTTask_MoveToPlayer -> ExecuteTask"));
	}
	return EBTNodeResult::Failed;
}


bool UBTTask_MoveToPlayer::GetRandomPointInRadius(const FVector& Origin, float Radius, FVector& OutResult)
{
	UNavigationSystem* NavSys = UNavigationSystem::GetCurrent(GetWorld());
	if (!NavSys)
	{
		//could not get UE4 Nav system
		return false;
	}
	//~~~~~~~~~~~~~

	FNavLocation Result;

	bool bSuccess = NavSys->GetRandomReachablePointInRadius(Origin, Radius, Result);

	if (!bSuccess)
	{
		//report non success case
		return false;
	}
	OutResult = Result.Location;
	return true;
}
