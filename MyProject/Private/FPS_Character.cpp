// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Public/Armor_Base.h"
#include "FPS_PlayerController.h"
#include "TutorialGameMode.h"
#include "MyProjectGameModeBase.h"
#include "FPS_Character.h"


void AFPS_Character::SaveGame()
{
	if (GameMode)
		GameMode->SaveGame();
}

void AFPS_Character::LoadGame()
{
	if (GameMode)
		GameMode->LoadGame();
}

// Sets default values
AFPS_Character::AFPS_Character()
{
	PrimaryActorTick.bCanEverTick = true;

	/*Creating & Setting up CollisionComponent*/
	CollisionComp = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionComp"));
	CollisionComp->SetupAttachment(RootComponent);

	/*Setting movement Variables*/
	GetCharacterMovement()->JumpZVelocity = 700.0f;
	GetCharacterMovement()->AirControl = 0.2f;

	/*Creating & Setting up PlayerCamera*/
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Player Camera"));
	Camera->SetupAttachment(GetCapsuleComponent());
	Camera->SetRelativeLocation(FVector(-10.0f, 0.0f, BaseEyeHeight));
	Camera->bUsePawnControlRotation = true;

	/*Creating & Setting up FirstPersonMesh */
	FirstPersonMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("First Person Mesh"));
	FirstPersonMesh->SetupAttachment(Camera);
	FirstPersonMesh->SetOnlyOwnerSee(true);
	FirstPersonMesh->bCastDynamicShadow = false;
	FirstPersonMesh->CastShadow = false;
	FirstPersonMesh->SetVisibility(false);

	/**Setting up ThirdPersonMesh*/
	//ThirdPersonMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Third Person Mesh"));
	//ThirdPersonMesh->SetupAttachment(CollisionComp);
	GetMesh()->CastShadow = true;
	GetMesh()->bCastDynamicShadow = true;
	GetMesh()->SetOwnerNoSee(true);


	/*Setting Inventory size and Currentweapon*/
	CurrentWeapon = NULL;

	PlayerStats = CreateDefaultSubobject<UPlayerStats>(TEXT("Player Stats"));
	PlayerStats->SetOwner(this);
	Inventory = CreateDefaultSubobject<UInventory>(TEXT("Inventory"));
	Inventory->SetOwner(this);

	ItemPickUpRange = 500;
	f_SprintMultiplier = 1.9f;

	DefaultCameraPosition = Camera->RelativeLocation;
	OffsetCameraPosition = FVector(0.0f, 50.0f, 20.0f);
}

void AFPS_Character::BeginPlay()
{
	Super::BeginPlay();
	HideCursor();
	SpawnPoint = GetActorLocation();
	SpawnRotation = GetActorRotation();

	if (bEquipDefaultWeapon)
	{
		FActorSpawnParameters Spawnparams;
		AWeapon_BaseClass* DefaultWeap = Cast<AWeapon_BaseClass>(GetWorld()->SpawnActor(DefaultWeaponToSpawnWith, &FVector::ZeroVector, &FRotator::ZeroRotator, Spawnparams));
		ProcessWeaponPickUp(DefaultWeap);
	}
	TutorialGameMode = Cast<ATutorialGameMode>(GetWorld()->GetAuthGameMode());
	if (TutorialGameMode)
	{
		Tutorial = true;
	}
	GameMode = Cast<AMyProjectGameModeBase>(GetWorld()->GetAuthGameMode());
	if (GameMode)
		GameMode->SetPlayerCharacter(this);
	Player_Controller = Cast<AFPS_PlayerController>(GetController());
	if (Player_Controller)
		Player_Controller->SetControlledPawn(this);
}

void AFPS_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CheckForItem();
}

void AFPS_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &AFPS_Character::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFPS_Character::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &AFPS_Character::Turn);
	PlayerInputComponent->BindAxis("LookUp", this, &AFPS_Character::LookUp);

	PlayerInputComponent->BindAction("SprintHold", IE_Pressed, this, &AFPS_Character::OnStartSprint);
	PlayerInputComponent->BindAction("SprintHold", IE_Released, this, &AFPS_Character::OnStopSprint);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AFPS_Character::OnStartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AFPS_Character::OnStartJump);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFPS_Character::FireWeapon);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AFPS_Character::StopFire);

	PlayerInputComponent->BindAction("AimDownSight", IE_Pressed, this, &AFPS_Character::OnStartAiming);
	PlayerInputComponent->BindAction("AimDownSight", IE_Released, this, &AFPS_Character::OnStopAiming);

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AFPS_Character::ReloadWeapon);

	PlayerInputComponent->BindAction("Use", IE_Pressed, this, &AFPS_Character::PickUpItem);
	PlayerInputComponent->BindAction("CharacterStats", IE_Pressed, PlayerStats, &UPlayerStats::ToggleCharacterStatsShowing);
	PlayerInputComponent->BindAction("Inventory", IE_Pressed, Inventory, &UInventory::ToggleInventoryShowing);

	PlayerInputComponent->BindAction("RoundSelection", IE_Pressed, this, &AFPS_Character::ToggleRoundSelection);
	PlayerInputComponent->BindAction("HelpWindow", IE_Pressed, this, &AFPS_Character::ToggleHelpWindow);

	PlayerInputComponent->BindAction("Save", IE_Pressed, this, &AFPS_Character::SaveGame);
	PlayerInputComponent->BindAction("Load", IE_Pressed, this, &AFPS_Character::LoadGame);

	PlayerInputComponent->BindAction("Escape", IE_Pressed, this, &AFPS_Character::CloseLastHUDElement);
}

#pragma region Cursor Functions  

void AFPS_Character::ShowCursor()
{
	if (!Player_Controller)
	{
		Player_Controller = Cast<AFPS_PlayerController>(GetController());
	}
	if (Player_Controller)
	{
		Player_Controller->SetMouseLocation(GEngine->GameViewport->Viewport->GetSizeXY().X / 2, GEngine->GameViewport->Viewport->GetSizeXY().Y / 2);
		Player_Controller->bShowMouseCursor = true;
		Player_Controller->bEnableClickEvents = true;
		Player_Controller->bEnableMouseOverEvents = true;
		Player_Controller->SetInputMode(FInputModeGameAndUI());
		b_CursorActive = true;
		return;
	}
	UE_LOG(LogTemp, Warning, TEXT("No PlayerController|Character:ShowCursor"));
}

void AFPS_Character::HideCursor()
{
	if (!Player_Controller)
	{
		Player_Controller = Cast<AFPS_PlayerController>(GetController());
	}
	if (Player_Controller)
	{
		if (Player_Controller)
		{
			if (Player_Controller->GetLastOpenedHudElement() != EHudElements::Nothing)
			{
				return;
			}
		}

		Player_Controller->bShowMouseCursor = false;
		Player_Controller->bEnableClickEvents = false;
		Player_Controller->bEnableMouseOverEvents = false;
		Player_Controller->SetInputMode(FInputModeGameOnly());
		Player_Controller->SetMouseLocation(GEngine->GameViewport->Viewport->GetSizeXY().X / 2, GEngine->GameViewport->Viewport->GetSizeXY().Y / 2);
		b_CursorActive = false;
		return;
	}
	UE_LOG(LogTemp, Warning, TEXT("No PlayerController|Character:HideCursor"));
}
#pragma endregion

#pragma region Movement
void AFPS_Character::MoveForward(float value)
{
	if (TutorialGameMode)
	{
		TutorialGameMode->SetbWPressed();
		TutorialGameMode->SetbSPressed();
	}
	if (b_IsDead == false)
	{
		const bool bLimitRotation = (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling());
		const FRotator Rotation = bLimitRotation ? GetActorRotation() : Controller->GetControlRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
		AddMovementInput(Direction, value);
	}
}

void AFPS_Character::MoveRight(float value)
{
	if (TutorialGameMode)
	{
		TutorialGameMode->SetbAPressed();
		TutorialGameMode->SetbDPressed();
	}
	if (b_IsDead == false)
	{
		FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, value);
	}
}

void AFPS_Character::Turn(float value)
{
	if (b_CursorActive == false && b_IsDead == false)
		ACharacter::AddControllerYawInput(value * f_Sensitivity);
}

void AFPS_Character::LookUp(float value)
{
	if (b_CursorActive == false && b_IsDead == false)
		ACharacter::AddControllerPitchInput(value * f_Sensitivity);
}

void AFPS_Character::OnStartJump()
{
	if (TutorialGameMode)
	{
		TutorialGameMode->SetbSpacePressed();
	}
	if (CurrentWeapon)
		if (CurrentWeapon->GetIsAiming() == true)
			OnStopAiming();
	if (b_IsDead == false)
		bPressedJump = true;
}

void AFPS_Character::OnStopJump()
{
	if (b_IsDead == false)
		bPressedJump = false;
}

void AFPS_Character::OnStartSprint()
{
	if (GetVelocity().Size() > 0.2f)
	{
		b_IsSprinting = true;
		if (CurrentWeapon)
			StopFire();
		if (CurrentWeapon)
			if (CurrentWeapon->GetIsAiming() == true)
				OnStopAiming();

		SetSprinting();
	}
}

void AFPS_Character::OnStopSprint()
{
	if (b_IsSprinting)
	{
		b_IsSprinting = false;
		if (CurrentWeapon)
			CurrentWeapon->b_CanFire = true;

		SetSprinting();
	}
}

void AFPS_Character::SetSprinting()
{
	if (b_IsSprinting)
		GetCharacterMovement()->MaxWalkSpeed *= f_SprintMultiplier;
	else
		GetCharacterMovement()->MaxWalkSpeed /= f_SprintMultiplier;
}
#pragma endregion

#pragma region Damage & Respawn Functions
void AFPS_Character::Take_Damage(float _f_DamageAmount, FVector HitLocation)
{
	if (b_IsDead)
		return;
	b_ShowHitIndicator = true;
	GetWorld()->GetTimerManager().SetTimer(HitIndicatorResetHandle, this, &AFPS_Character::DisableHitIndicator, 1.0f, false);

	float f_FinalDamageTaken = ((_f_DamageAmount * _f_DamageAmount) / (_f_DamageAmount + PlayerStats->PlayerStatsConfig.f_Toughness + PlayerStats->PlayerStatsConfig.i_PlayerLevel) * 2);

	if (PlayerStats->GetCurrentArmor() > 0)
	{
		int restDamage = f_FinalDamageTaken - PlayerStats->GetCurrentArmor();
		PlayerStats->ReduceArmor(f_FinalDamageTaken);

		/** Create the Hit Particle*/
		PlayParticleEffect(HitParticeSystem, HitLocation);

		if (restDamage > 0)
		{
			PlayerStats->ChangeCurrentHealth(-f_FinalDamageTaken);
			/** Create the Hit Particle*/
			//PlayParticleEffect(HitParticeSystem, HitLocation);	
		}
	}
	else
	{
		PlayerStats->ChangeCurrentHealth(-f_FinalDamageTaken);

		/** Create the Hit Particle*/
		PlayParticleEffect(HitParticeSystem, HitLocation);
	}

	if (PlayerStats->PlayerStatsConfig.f_CurrentHealth <= 0)
	{
		PlayerStats->PlayerStatsConfig.f_CurrentHealth = 0;
		Death();
	}
}

void AFPS_Character::Death()
{
	if (GameMode)
		GameMode->EndRoundByPlayerDeath();
	b_IsDead = true;
	GetWorld()->GetTimerManager().SetTimer(RespawnTimerHandle, this, &AFPS_Character::Respawn, f_RespawnTime, false);
	OffsetDeathCamera(true);
	ThirdPersonMesh = GetMesh();
	if (ThirdPersonMesh)
	{
		ThirdPersonMesh->SetOwnerNoSee(false);
		ThirdPersonMesh->SetHiddenInGame(false);
		ThirdPersonMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		ThirdPersonMesh->IgnoreActorWhenMoving(this, true);
		ThirdPersonMesh->SetSimulatePhysics(true);
		ThirdPersonMesh->SetCollisionProfileName("Ragdoll");
	}
	if (CurrentWeapon && CurrentWeapon->GetIsAiming() == true)
		OnStopAiming();
	StopFire();
}

void AFPS_Character::PlayParticleEffect(UParticleSystem* _PS, FVector _HitLocation)
{
	HitPSC = UGameplayStatics::SpawnEmitterAttached(_PS, GetMesh());
	if (HitPSC)
	{
		HitPSC->SetWorldLocation(_HitLocation);
		HitPSC->bAutoDestroy = true;
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("NoParticleSystem!| FPS_Character : Take_Damage"));
}

bool AFPS_Character::GetIsDead()
{
	return b_IsDead;
}

void AFPS_Character::Respawn(FVector _SpawnPosition, FRotator _SpawnRotation)
{
	b_IsDead = false;
	OffsetDeathCamera(true);
	if (ThirdPersonMesh)
	{
		ThirdPersonMesh->SetOwnerNoSee(true);
		ThirdPersonMesh->SetHiddenInGame(true);
		ThirdPersonMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		ThirdPersonMesh->IgnoreActorWhenMoving(this, false);
		ThirdPersonMesh->SetSimulatePhysics(false);
		ThirdPersonMesh->SetCollisionProfileName("CharacterMesh");
		ThirdPersonMesh->AttachToComponent(CollisionComp, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		ThirdPersonMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -90.0f));
		ThirdPersonMesh->SetRelativeRotation(FRotator(0.0f,-90.0f,0.0f));
	}
	if (CurrentWeapon && CurrentWeapon->GetIsAiming() == true)
		OnStopAiming();
	PlayerStats->ChangeCurrentHealth(PlayerStats->GetMaxHealth());
	SetActorLocation(_SpawnPosition);
	SetActorRotation(_SpawnRotation);
}

ESlateVisibility AFPS_Character::GetHitIndicatorVisibility()
{
	if (b_ShowHitIndicator)
		return ESlateVisibility::Visible;

	return ESlateVisibility::Hidden;
}

void AFPS_Character::DisableHitIndicator()
{
	b_ShowHitIndicator = false;
}

void AFPS_Character::OffsetDeathCamera(bool _offset)
{
	if (_offset)
	{
		if (FirstPersonMesh)
			FirstPersonMesh->SetHiddenInGame(true);
		if (CurrentWeapon)
			CurrentWeapon->SetActorHiddenInGame(true);
		if (Camera)
			Camera->SetRelativeLocation(FVector(
				FMath::Lerp(Camera->RelativeLocation.X, OffsetCameraPosition.X, 0.5f),
				FMath::Lerp(Camera->RelativeLocation.Y, OffsetCameraPosition.Y, 0.5f),
				FMath::Lerp(Camera->RelativeLocation.Z, OffsetCameraPosition.Z, 0.5f)));
	}
	else
	{
		if (FirstPersonMesh)
			FirstPersonMesh->SetHiddenInGame(false);
		if (CurrentWeapon)
			CurrentWeapon->SetActorHiddenInGame(false);
		if (Camera)
			Camera->SetRelativeLocation(FVector(
				FMath::Lerp(Camera->RelativeLocation.X, DefaultCameraPosition.X, 1.0f),
				FMath::Lerp(Camera->RelativeLocation.Y, DefaultCameraPosition.Y, 1.0f),
				FMath::Lerp(Camera->RelativeLocation.Z, DefaultCameraPosition.Z, 1.0f)));
	}
}

void AFPS_Character::Respawn()
{
	b_IsDead = false;
	if (PlayerStats)
		PlayerStats->ChangeCurrentHealth(PlayerStats->GetMaxHealth());
	OffsetDeathCamera(false);
	if (ThirdPersonMesh)
	{
		ThirdPersonMesh->SetOwnerNoSee(true);
		ThirdPersonMesh->SetHiddenInGame(true);
		ThirdPersonMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		ThirdPersonMesh->IgnoreActorWhenMoving(this, false);
		ThirdPersonMesh->SetSimulatePhysics(false);
		ThirdPersonMesh->SetCollisionProfileName("CharacterMesh");
		ThirdPersonMesh->AttachToComponent(CollisionComp, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		ThirdPersonMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -90.0f));
		ThirdPersonMesh->SetRelativeRotation(FRotator(0.0f, 0.0f, -90.0f));
	}
	if (CurrentWeapon && CurrentWeapon->GetIsAiming() == true)
		OnStopAiming();
	StopFire();
	SetActorLocation(SpawnPoint);
	SetActorRotation(SpawnRotation);
}


#pragma endregion

#pragma region WeaponFunctions

void AFPS_Character::StopFire()
{
	if (CurrentWeapon)
	{
		GetWorld()->GetTimerManager().ClearTimer(FireTimerHandle);
		CurrentWeapon->StopAnimMontage(CurrentWeapon->FireAnim);
	}
}

void AFPS_Character::FireWeapon()
{
	if (CurrentWeapon != NULL && b_IsDead == false && b_CursorActive == false)
	{
		if (b_IsSprinting)
			OnStopSprint();

		if (CurrentWeapon->WeaponConfig.FireType == EWeaponFireType::ESingleShot)
			CurrentWeapon->Fire();

		else if (CurrentWeapon->WeaponConfig.FireType == EWeaponFireType::EBurstFire)
			GetWorld()->GetTimerManager().SetTimer(FireTimerHandle, CurrentWeapon, &AWeapon_BaseClass::BurstFire, 0.01f, true);

		else if (CurrentWeapon->WeaponConfig.FireType == EWeaponFireType::EAutoFire)
			GetWorld()->GetTimerManager().SetTimer(FireTimerHandle, CurrentWeapon, &AWeapon_BaseClass::Fire, 0.01f, true);

	}
}

void AFPS_Character::ReloadWeapon()
{
	if (CurrentWeapon)
		CurrentWeapon->StartReload();
}

void AFPS_Character::PlayHitNotification()
{
	UAudioComponent* AudioComp = NULL;
	if (EnemyHitSound)
	{
		AudioComp = UGameplayStatics::SpawnSoundAttached(EnemyHitSound, GetMesh());
	}
	SetHitMarkerVisbility(true);
	FTimerDelegate Del;
	Del.BindUFunction(this, "SetHitMarkerVisbility", false);
	GetWorld()->GetTimerManager().SetTimer(HitMarkerTimerHandle, Del, fShowHitmarkerTime, false);
}

ESlateVisibility AFPS_Character::GetHitMarkerVisibility()
{
	if (b_HitmarkerShowing)
		return ESlateVisibility::Visible;
	return ESlateVisibility::Hidden;
}

void AFPS_Character::SetHitMarkerVisbility(bool _b_Visibility)
{
	b_HitmarkerShowing = _b_Visibility;
}

void AFPS_Character::OnStartAiming()
{
	if (CurrentWeapon && b_IsDead == false && b_CursorActive == false)
	{
		if (CurrentWeapon->AmmoType == EWeaponAmmoType::ERifleAmmo)
		{
			if (b_IsSprinting)
				OnStopSprint();
			CurrentWeapon->SetIsAiming(true);
			Camera->FieldOfView = 68;
		}
	}
}

void AFPS_Character::OnStopAiming()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->SetIsAiming(false);
		Camera->FieldOfView = 90;
	}
}

void AFPS_Character::ExecuteCameraShake()
{
	if (CameraShakeToUse)
	{
		Player_Controller->ClientPlayCameraShake(CameraShakeToUse);
		return;
	}
	else
		UE_LOG(LogTemp, Error, TEXT("CameraShake not assigned! - FPS_Character -> ExecuteCameraShake"));

}
#pragma endregion

#pragma region Accessor Functions

ESlateVisibility AFPS_Character::IsRoundSlectionShowing()
{
	if (GameMode)
		return GameMode->GetRoundSelectionShowing();

	return ESlateVisibility::Hidden;
}

void AFPS_Character::ToggleRoundSelection()
{
	if (GameMode)
		GameMode->ToggleRoundSelection();
	else if (!GameMode)
		UE_LOG(LogTemp, Warning, TEXT("No GameMode: Toggle Round Selection | Character"));
}

void AFPS_Character::CloseLastHUDElement()
{
	if (Player_Controller)
		Player_Controller->CloseLastHudElement();
}

ESlateVisibility AFPS_Character::GetIsHelpWindowShowing()
{
	if (bIsHelpWindowShowing)
		return ESlateVisibility::Visible;

	return ESlateVisibility::Hidden;
}

void AFPS_Character::ToggleHelpWindow()
{
	bIsHelpWindowShowing = !bIsHelpWindowShowing;
	if (bIsHelpWindowShowing) ShowCursor();
	else HideCursor();
}

bool AFPS_Character::GetIsSprinting()
{
	return b_IsSprinting;
}

AWeapon_BaseClass* AFPS_Character::GetCurrentWeapon()
{
	return CurrentWeapon;
}

UPlayerStats* AFPS_Character::GetPlayerStats()
{
	return PlayerStats;
}

UInventory* AFPS_Character::GetPlayerInventory()
{
	return Inventory;
}

FString AFPS_Character::GetCurrentClipAmmo()
{
	if (CurrentWeapon != nullptr)
	{
		if (CurrentWeapon->WeaponConfig.bUnlimitedClip)
		{
			FString Ammo = "Unlimited";
			return Ammo;
		}
		else
		{
			return FString::FromInt(CurrentWeapon->WeaponConfig.CurrentClipAmmo);
		}
	}
	else
		return FString::FromInt(0);
}

void AFPS_Character::SetInformationText(FString _InformationText, float _f_DelayToHideText, bool _b_ResetDisplay)
{
	InformationPanelText = _InformationText;
	if (_b_ResetDisplay == false)
		return;
	if (GetWorld()->GetTimerManager().IsTimerActive(InformationResetTimerHandle))
		GetWorld()->GetTimerManager().ClearTimer(InformationResetTimerHandle);

	GetWorld()->GetTimerManager().SetTimer(InformationResetTimerHandle, this, &AFPS_Character::ResetInformationPanel, _f_DelayToHideText, false, _f_DelayToHideText);
}

FString AFPS_Character::GetInformationText() const
{
	return InformationPanelText;
}

void AFPS_Character::ResetInformationPanel()
{
	InformationPanelText = "";
}
#pragma endregion

#pragma region WeaponSelect

void AFPS_Character::ProcessWeaponPickUp(AWeapon_BaseClass* weapon)
{
	if (weapon != nullptr)
	{
		if (FirstPersonMesh)
		{
			if (FirstPersonMesh->IsVisible() == false)
				FirstPersonMesh->SetVisibility(true);
		}
		else
			UE_LOG(LogTemp, Error, TEXT("No First Person Mesh has been assigned!. Cannot change its Visibility - FPS_Character.cpp -> ProcessWeaponPickup"));

		SelectWeapon(weapon);
	}
}

void AFPS_Character::SelectWeapon(AWeapon_BaseClass* weapon)
{
	weapon->SetOwningPawn(this, PlayerStats);

	if (CurrentWeapon == nullptr)
	{
		CurrentWeapon = weapon;
		CurrentWeapon->EquipWeapon();
		CurrentWeapon->SetActiveWeapon(true);
	}
	else
	{
		Inventory->AddWeaponToInventory(Inventory->CurrentInteractable);
		weapon->SetActiveWeapon(false);
	}
	weapon->EquipWeapon();
	PlayerStats->SetCurrentAmmoInUse();
}

void AFPS_Character::EquipItemFromInventory(AItem_PickUp* _ItemToEquip)
{
	AWeapon_BaseClass* WeaponToEquip = Cast<AWeapon_BaseClass>(_ItemToEquip);
	if (WeaponToEquip)
	{
		Inventory->AddWeaponToInventory(CurrentWeapon);
		CurrentWeapon->SetActiveWeapon(false);
		CurrentWeapon->HideWeapon();
		Inventory->RemoveWeaponFromInventory(WeaponToEquip);
		CurrentWeapon = nullptr;
		CurrentWeapon = WeaponToEquip;
		CurrentWeapon->SetActiveWeapon(true);
		CurrentWeapon->SetOwningPawn(this, PlayerStats);
		CurrentWeapon->EquipWeapon();
		PlayerStats->SetCurrentAmmoInUse();
	}

	AArmor_Base* ArmorToEquip = Cast<AArmor_Base>(_ItemToEquip);
	if (ArmorToEquip)
	{
		Inventory->EquipArmor(ArmorToEquip, true);
	}
}

void AFPS_Character::DropInventoryItem(AItem_PickUp* _ItemToDrop)
{

	AWeapon_BaseClass* WeaponToDrop = Cast<AWeapon_BaseClass>(_ItemToDrop);
	if (WeaponToDrop)
	{
		Inventory->RemoveWeaponFromInventory(WeaponToDrop);
		WeaponToDrop->DetachFromPlayer();
	}

	AArmor_Base* ArmorToDrop = Cast<AArmor_Base>(_ItemToDrop);
	if (ArmorToDrop)
	{
		Inventory->RemoveArmorFromInventory(ArmorToDrop);
		ArmorToDrop->DropItem();
	}
}

void AFPS_Character::DeleteItem(AItem_PickUp* _ItemToDestroy)
{
	AWeapon_BaseClass* WeaponToDestroy = Cast<AWeapon_BaseClass>(_ItemToDestroy);
	if (WeaponToDestroy)
		Inventory->RemoveWeaponFromInventory(WeaponToDestroy);

	AArmor_Base* ArmorToDestroy = Cast<AArmor_Base>(_ItemToDestroy);
	if (ArmorToDestroy)
		Inventory->RemoveArmorFromInventory(ArmorToDestroy);

	_ItemToDestroy->Destroy();
}
#pragma endregion

#pragma region ItemPickup

void AFPS_Character::PickUpItem()
{
	//Cast to see what item it is

	AWeapon_BaseClass* WeaponToPickUp = Cast<AWeapon_BaseClass>(Inventory->CurrentInteractable);
	AArmor_Base* ArmorToPickUp = Cast<AArmor_Base>(Inventory->CurrentInteractable);

	//If Weapon then Equip it / put it in the WeaponInventory
	if (WeaponToPickUp)
		ProcessWeaponPickUp(WeaponToPickUp);


	if (ArmorToPickUp)
	{
		Inventory->EquipArmor(ArmorToPickUp, false);
		ArmorToPickUp->SetOwningPlayer(this);
	}
}

void AFPS_Character::CheckForItem()
{
	if (Inventory)
	{
		FRotator AimDir;
		FVector StartTrace;
		Controller->GetPlayerViewPoint(StartTrace, AimDir);
		FVector EndTrace = StartTrace + AimDir.Vector() * ItemPickUpRange;
		FCollisionQueryParams TraceParams("", true, this);
		FHitResult Hit(ForceInit);

		if (GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_Visibility, TraceParams))
		{
			AItem_PickUp* Item = Cast<AItem_PickUp>(Hit.GetActor());
			if (Item)
			{
				/** Check if the Item is a Health or Ammo drop. If yes return and dont set the Interactable*/
				class AHealthDrop* HP = Cast<AHealthDrop>(Item);
				class AAmmoDrop* AM = Cast<AAmmoDrop>(Item);
				if (HP)
				{
					SetInformationText("This is a: Life Pickup it will be picked up by standing near it when you are missing Health", 1);
					return;
				}
				if (AM)
				{
					SetInformationText("This is a: Ammo Pickup it will be picked up by standing near it when you are missing Ammo", 1);
					return;
				}
				/** Else set the CurrentInteractable*/
				Inventory->SetCurrentInteractable(Item);
				return;
			}
		}
		Inventory->SetCurrentInteractable(nullptr);
	}
}


#pragma endregion