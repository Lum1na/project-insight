// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Armor_Base.h"
#include "FPS_Character.h"
#include "MyProjectGameModeBase.h"
#include "PlayerStats.h"


AArmor_Base::AArmor_Base()
{
	PrimaryActorTick.bCanEverTick = true;
	ArmorConfig.f_Armor = 0.0f;
	ArmorConfig.f_Vitality = 0.0f;
	ArmorConfig.f_Accuracy = 0.0f;
	ArmorConfig.f_CritChance = 0.0f;
	ArmorConfig.f_CritDamage = 0.0f;
	ArmorConfig.f_LifeLeach = 0.0f;

	LootPSC = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Loot Beam Comp"));
	LootPSC->SetupAttachment(RootComponent);
	bShowLootPSC = true;
}

void AArmor_Base::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (bShowLootPSC)
	{
		if (LootPSC)
			LootPSC->SetWorldRotation(FVector::UpVector.Rotation());
	}
}

void AArmor_Base::GenerateStats()
{
	/** Get player*/
	AFPS_Character* Player = Cast<AFPS_Character>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (Player)
	{  
		/** get level from player*/
		int32 Playerlevel = Player->GetPlayerStats()->GetPlayerLevel();	

		/** get gamemode*/
		AMyProjectGameModeBase* GameMode = Cast<AMyProjectGameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			/** get current round from gamemode*/
			int round = GameMode->GetCurrentRound();

			/** local variable to track how many stats the item has*/
			int i_AmountOfStats = 0;
			while (i_AmountOfStats < i_MaxAmountOfStats)
			{
				/** Generate stats check if max stats has been reached*/
				if (FMath::RandRange(0, 1) == 1 && i_AmountOfStats < i_MaxAmountOfStats)
				{
					ArmorConfig.f_Armor = FMath::RandRange(f_ArmorBaseStat + Playerlevel + (round * f_ArenaRoundModifier) * f_ArmorMultiplier,
						f_ArmorBaseStat + 1 + Playerlevel + (round * f_ArenaRoundModifier) * (f_MaxStatMultiplier * f_ArmorMultiplier));
					i_AmountOfStats++;
				}


				if (FMath::RandRange(0, 1) == 1 && i_AmountOfStats < i_MaxAmountOfStats)
				{
					ArmorConfig.f_CritDamage = FMath::RandRange(f_CritDamageBaseStat + Playerlevel + (round * f_ArenaRoundModifier) * f_ArmorMultiplier,
						f_CritDamageBaseStat + 1 + Playerlevel + (round * f_ArenaRoundModifier) * (f_MaxStatMultiplier * f_ArmorMultiplier));
					i_AmountOfStats++;
				}

				if (FMath::RandRange(0, 1) == 1 && i_AmountOfStats < i_MaxAmountOfStats)
				{
					ArmorConfig.f_Vitality = FMath::RandRange(f_VitalityBaseStat + Playerlevel + (round * f_ArenaRoundModifier) * f_ArmorMultiplier,
						f_VitalityBaseStat + 1 + Playerlevel + (round * f_ArenaRoundModifier) * (f_MaxStatMultiplier * f_ArmorMultiplier));
					i_AmountOfStats++;
				}

				if (FMath::RandRange(0, 1) == 1 && i_AmountOfStats < i_MaxAmountOfStats)
				{
					ArmorConfig.f_Toughness = FMath::RandRange(f_ToughnessBaseStat + Playerlevel + (round * f_ArenaRoundModifier) * f_ArmorMultiplier,
						f_ToughnessBaseStat + 1 + Playerlevel + (round * f_ArenaRoundModifier) * (f_MaxStatMultiplier * f_ArmorMultiplier));
					i_AmountOfStats++;
				}

				if (FMath::RandRange(0, 1) == 1 && i_AmountOfStats < i_MaxAmountOfStats)
				{
					ArmorConfig.f_Accuracy = FMath::RandRange(2 + (Playerlevel / 10) * f_ArmorMultiplier, 3 + (Playerlevel / 10) *(f_MaxStatMultiplier * f_ArmorMultiplier));
					i_AmountOfStats++;
				}

				if (FMath::RandRange(0, 1) == 1 && i_AmountOfStats < i_MaxAmountOfStats)
				{
					ArmorConfig.f_CritChance = FMath::RandRange(5,15);
					i_AmountOfStats++;
				}

				if (FMath::RandRange(0, 1) == 1 && i_AmountOfStats < i_MaxAmountOfStats)
				{
					ArmorConfig.f_LifeLeach = FMath::RandRange(0.5f, 0.75f);
					i_AmountOfStats++;
				}
			}
		}
	}
}

void AArmor_Base::CopyStats(FArmorData _StatsToCopy)
{
	/** Copy the stats to this item*/
	ArmorConfig.f_Armor = _StatsToCopy.f_Armor;
	ArmorConfig.f_Vitality = _StatsToCopy.f_Vitality;
	ArmorConfig.f_Toughness = _StatsToCopy.f_Toughness;
	ArmorConfig.f_Accuracy = _StatsToCopy.f_Accuracy;
	ArmorConfig.f_CritChance = _StatsToCopy.f_CritChance;
	ArmorConfig.f_CritDamage = _StatsToCopy.f_CritDamage;
	ArmorConfig.f_LifeLeach = _StatsToCopy.f_LifeLeach;
	ArmorConfig.i_InventorySlot = _StatsToCopy.i_InventorySlot;
}

void AArmor_Base::Equip()
{
	/** Disables the loot indicator particle system, collision and disables physic*/
	bShowLootPSC = false;
	if (LootPSC)
		LootPSC->DeactivateSystem();
	Mesh->SetHiddenInGame(true);
	CollisionComp->SetActive(false);
	CollisionComp->SetSimulatePhysics(false);
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AArmor_Base::DropItem()
{
	/** enables the loot indicator particle system*/
	bShowLootPSC = true;
	if (LootPSC)
		LootPSC->ActivateSystem();
	/** show mesh ingame*/
	Mesh->SetHiddenInGame(false);
	/** Enable collision and physics*/
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CollisionComp->IgnoreActorWhenMoving(OwningPlayer, true);
	CollisionComp->SetSimulatePhysics(true);
	/** set location to playerlocation*/
	SetActorLocation(OwningPlayer->GetActorLocation());

	/** Get player lookdirection and add impulse in that direction*/
	FVector CamLoc;
	FRotator CamRot;
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetPlayerViewPoint(CamLoc, CamRot);
	const FVector LookDirection = CamRot.Vector();
	CollisionComp->AddImpulseAtLocation(LookDirection * 5000.0f, GetActorLocation());
}

void AArmor_Base::SetOwningPlayer(AFPS_Character* _OwningPlayer)
{
	/** set player reference*/
	if (OwningPlayer != _OwningPlayer)
	{
		OwningPlayer = _OwningPlayer;
		PlayerStats = OwningPlayer->GetPlayerStats();
	}
}
