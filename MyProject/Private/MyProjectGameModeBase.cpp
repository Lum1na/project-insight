// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Blueprint/UserWidget.h"
#include "Waypoint_Manager.h"
#include "SpawnPoint.h"
#include "FPS_Character.h"
#include "FPS_PlayerController.h"
#include "Base_EnemyCharacter.h"
#include "Weapon_BaseClass.h"
#include "GameSave.h"
#include "MyProjectGameModeBase.h"

AMyProjectGameModeBase::AMyProjectGameModeBase()
{
	PrimaryActorTick.bStartWithTickEnabled = false;
	PrimaryActorTick.bCanEverTick = false;
	b_SurvivedLastRound = true;
}

void AMyProjectGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	i_RoundEnemyAmount = 0;
	b_IsFirstRound = true;

	if (PlayerHUDClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}

	for (TActorIterator<AWaypoint_Manager> ActrItr(GetWorld()); ActrItr; ++ActrItr)
	{
		WaypointManager = *ActrItr;
	}
}

void AMyProjectGameModeBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AMyProjectGameModeBase::StartNextRound()
{
	if (b_StartSelectedRound && b_SurvivedLastRound == true)
	{
		i_CurrentRound = i_RoundToStart;
	}
	else if (b_SurvivedLastRound == false && b_StartSelectedRound == false)
	{
		i_CurrentRound = i_MaxRoundUnlocked;
	}
	else
	{
		i_MaxRoundUnlocked++;
		i_CurrentRound = i_MaxRoundUnlocked;
	}
	ResetEnemies();
}

void AMyProjectGameModeBase::ResetEnemies()
{
	i_RoundEnemyAmount = FMath::RandRange(i_MinEnemiesPerRound, i_MaxEnemiesPerRound);
	if (b_SpawnedEnemies == false)
	{
		for (int i = 0; i < i_MaxEnemiesPerRound; i++)
		{
			SpawnEnemy();
		}
	}

	for (int i = 0; i < SpawnedEnemies.Num(); i++)
	{
		if (i < i_RoundEnemyAmount)
		{
			SpawnedEnemies[i]->Respawn();
			SpawnedEnemies[i]->SetStats(i_CurrentRound);
			SpawnedEnemies[i]->SetActiveState(true);
		}
		else
		{
			SpawnedEnemies[i]->Respawn();
			SpawnedEnemies[i]->SetActorLocation(FVector(0.0f, 0.0f, 0.0f));
			SpawnedEnemies[i]->SetActiveState(false);

		}
	}
	CheckRoundEnd();
}

void AMyProjectGameModeBase::SpawnWeaponFromSaveGame(TSubclassOf<AItem_PickUp> _ItemBase, FWeaponData _WeaponData, bool bIsEquipped)
{
	if (GetWorld())
	{
		UInventory* inv = nullptr;
		if (Player)
		{
			inv = Player->GetPlayerInventory();
		}

		FActorSpawnParameters SpawnParams;
		AWeapon_BaseClass* Weapon = GetWorld()->SpawnActor<AWeapon_BaseClass>(_ItemBase, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		if (Weapon)
		{
			Weapon->CopyStats(_WeaponData, _WeaponData.FireType);
			if (inv != nullptr)
			{
				if (bIsEquipped)
					Player->ProcessWeaponPickUp(Weapon);
				if (bIsEquipped == false)
					inv->AddWeaponToInventory(Weapon);
			}
		}
	}
}

void AMyProjectGameModeBase::SpawnArmorFromSaveGame(TSubclassOf<AItem_PickUp> _ItemBase, FArmorData _ArmorData, bool bIsEquipped)
{
	if (GetWorld())
	{
		UInventory* inv = nullptr;
		if (Player)
		{
			inv = Player->GetPlayerInventory();
		}

		FActorSpawnParameters SpawnParams;
		AArmor_Base* Armor = GetWorld()->SpawnActor<AArmor_Base>(_ItemBase, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		if (Armor)
		{
			Armor->CopyStats(_ArmorData);
			if (inv != nullptr)
			{
				if (bIsEquipped)
					inv->EquipArmor(Armor, false);
				else
					inv->AddArmorToInventory(Armor);
			}
		}
	}
}

void AMyProjectGameModeBase::CheckRoundEnd()
{
	i_EnemiesAlive = 0;
	if (SpawnedEnemies.Num() > 0)
	{
		for (int i = 0; i < SpawnedEnemies.Num(); i++)
		{
			if (i < i_RoundEnemyAmount)
			{
				/** Check each enemie to see if they are alive*/
				i_EnemiesAlive += SpawnedEnemies[i]->b_IsDead == false ? 1 : 0;
			}
		}
		if (i_EnemiesAlive == 0)
		{
			b_FinishedRound = true;
			b_RoundActive = false;
			b_SurvivedLastRound = true;
			SaveGame();
		}
	}
}

void AMyProjectGameModeBase::EndRoundByPlayerDeath()
{
	b_SurvivedLastRound = false;
	for (int i = 0; i < SpawnedEnemies.Num(); i++)
	{
		SpawnedEnemies[i]->Respawn();
		SpawnedEnemies[i]->SetActorLocation(FVector(0.0f, 0.0f, 0.0f));
		SpawnedEnemies[i]->b_IsDead = true;
	}
	i_RoundEnemyAmount = 0;
	i_EnemiesAlive = 0;
	b_RoundActive = false;
	SaveGame();
}

void AMyProjectGameModeBase::SpawnEnemy()
{
	if (WaypointManager)
	{
		ASpawnPoint* SpawnPoint = WaypointManager->GetRandomSpawnPoint();
		if (SpawnPoint != nullptr)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Instigator = nullptr;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.OverrideLevel = nullptr;

			ABase_EnemyCharacter* NewEnemy = GetWorld()->SpawnActor<ABase_EnemyCharacter>(EnemyToSpawn, SpawnPoint->GetActorLocation(), SpawnPoint->GetActorRotation(), SpawnParams);
			if (NewEnemy)
			{
				NewEnemy->Respawn();
				NewEnemy->SetArenaGameMode(this);
				NewEnemy->SetStats(i_CurrentRound);
				SpawnedEnemies.Add(NewEnemy);
			}
			b_SpawnedEnemies = true;
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Unable to Spawn Enemies! | GameMode : SpawnEnemy"));
			b_SpawnedEnemies = false;
		}
	}
}

void AMyProjectGameModeBase::StartNewRoundCountDownTimer()
{
	if (b_ShowRoundSelection == true)
		ToggleRoundSelection();
	f_CountdownTimer = f_NewRoundCountdownTime;
	b_CountdownTimerStarted = true;
	b_RoundActive = true;
	if (b_IsFirstRound) b_IsFirstRound = false;
	GetWorld()->GetTimerManager().SetTimer(NewRoundTimerHandle, this, &AMyProjectGameModeBase::DecreaseNextRoundTimer, 0.01f, true);
}

void AMyProjectGameModeBase::DecreaseNextRoundTimer()
{
	f_CountdownTimer -= 0.01f;
	if (f_CountdownTimer <= 0.0f)
	{
		GetWorld()->GetTimerManager().SetTimer(NewRoundTimerHandle, this, &AMyProjectGameModeBase::DecreaseNextRoundTimer, 0.00f, false);
		b_CountdownTimerStarted = false;
		StartNextRound();
	}
}

void AMyProjectGameModeBase::SaveGame()
{
	UGameSave* SaveGameInstance = Cast<UGameSave>(UGameplayStatics::CreateSaveGameObject(UGameSave::StaticClass()));
	SaveGameInstance->Save(Player,this);
	//UGameplayStatics::SaveGameToSlot(SaveGameInstance, TEXT("MySlot"), 0);
}
void AMyProjectGameModeBase::LoadGame()
{
	UGameSave* LoadGameInstance = Cast<UGameSave>(UGameplayStatics::CreateSaveGameObject(UGameSave::StaticClass()));
	LoadGameInstance = Cast<UGameSave>(UGameplayStatics::LoadGameFromSlot(TEXT("MySlot"), 0));
	LoadGameInstance->LoadGame(Player, this);
}

void AMyProjectGameModeBase::StartSelectedRound()
{
	if (i_RoundToStart >= i_MaxRoundUnlocked)
	{
		b_StartSelectedRound = false;
		StartNewRoundCountDownTimer();
	}
	else if (i_RoundToStart < i_MaxRoundUnlocked)
	{
		b_StartSelectedRound = true;
		StartNewRoundCountDownTimer();
	}
}

#pragma region Acessor Functions;
void AMyProjectGameModeBase::ToggleRoundSelection()
{
	if (b_RoundActive)
	{
		b_ShowRoundSelection = false;
		return;
	}
	b_ShowRoundSelection = !b_ShowRoundSelection;
	if (b_ShowRoundSelection)
	{

		if (Player)
		{
			Player->ShowCursor();
			if (Player->Player_Controller)
				Player->Player_Controller->AddLastOpenedHudElement(EHudElements::AfterRoundWindow);
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("No Player! | GameMode : Toggle Round Selection"));

	}
	else
		if (Player)
		{
			if (Player->Player_Controller)
				Player->Player_Controller->RemoveHudElementFromList(EHudElements::AfterRoundWindow);
			Player->HideCursor();
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("No Player! | GameMode : Toggle Round Selection"));
}

ESlateVisibility AMyProjectGameModeBase::GetRoundSelectionShowing()
{
	if (b_ShowRoundSelection)
		return 	ESlateVisibility::Visible;

	return 	ESlateVisibility::Hidden;
}

int AMyProjectGameModeBase::GetCurrentRound()
{
	return i_CurrentRound;
}

int AMyProjectGameModeBase::GetMaxRoundUnlocked()
{
	return i_MaxRoundUnlocked;
}

float AMyProjectGameModeBase::GetNewRoundCountdownTimer()
{
	return f_CountdownTimer;
}

float AMyProjectGameModeBase::GetArenaRoundMultiplier()
{
	return f_ArenaRoundMultiplier;
}

int AMyProjectGameModeBase::GetAliveEnemies()
{
	return i_EnemiesAlive;
}

int AMyProjectGameModeBase::GetEnemiesThisRound()
{
	return i_RoundEnemyAmount;
}

bool AMyProjectGameModeBase::GetIsNewRoundTimerActive()
{
	return b_CountdownTimerStarted;
}

bool AMyProjectGameModeBase::GetIsRoundActive()
{
	return b_RoundActive;
}

bool AMyProjectGameModeBase::GetSurvivedLastRound()
{
	return b_SurvivedLastRound;
}

bool AMyProjectGameModeBase::GetIsFirstRound()
{
	return b_IsFirstRound;
}

FString AMyProjectGameModeBase::GetAfterRoundText()
{
	if (b_SurvivedLastRound == false)
		return FString("You Died on Round: " + FString::FromInt(GetCurrentRound()) + ". Would you like to restart the previous Round?");
	if (b_IsFirstRound)
		return FString("Would you like to Start a new Round?");
	if (b_SurvivedLastRound)
		return FString("You Survived Round: " + FString::FromInt(GetCurrentRound()) + ". Would you like to Continue?");

	return FString("");
}

ESlateVisibility AMyProjectGameModeBase::GetShowNewRoundCountdownTimer()
{
	if (b_CountdownTimerStarted)
	{
		return ESlateVisibility::Visible;
	}
	else
	{
		return ESlateVisibility::Hidden;
	}
}

void AMyProjectGameModeBase::Setb_StartSelectedRound(bool _b_bool)
{
	b_StartSelectedRound = _b_bool;
}

void AMyProjectGameModeBase::SetSelectedRound(int _i_NextRoundToStart)
{
	i_RoundToStart = _i_NextRoundToStart;
}

void AMyProjectGameModeBase::SetMaxRoundUnlocked(int _MaxRoundsUnlocked)
{
	i_MaxRoundUnlocked = _MaxRoundsUnlocked;
}
#pragma endregion  
