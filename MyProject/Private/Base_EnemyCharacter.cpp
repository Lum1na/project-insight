// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "AI_Weapon.h"
#include "AI_BaseEnemyController.h"
#include "Kismet/KismetMathLibrary.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "FPS_Character.h"	
#include "Public/Waypoint_Manager.h"
#include "MyProjectGameModeBase.h"
#include "Base_EnemyCharacter.h"

ABase_EnemyCharacter::ABase_EnemyCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	AI_Weapon = nullptr;

	f_MaxHealth = 100.0f;
	i_XPonKill = 25.0f;
	i_Toughness = 50;

	//Creating WeaponSpawner Component
	WeaponSpawner = CreateDefaultSubobject<UWeaponSpawner>(TEXT("WeaponSpawner"));
	BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard Component"));
	BehaviorComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("Behavior Component"));
}

void ABase_EnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	AI_Weapon = GetWorld()->SpawnActor<AAI_Weapon>(WeaponToSpawnWith);
	AI_Weapon->EquipWeapon(GetMesh());
	if (AIPC == nullptr)
	{
		AIPC = Cast<AAI_BaseEnemyController>(GetController());
	}

	//if (AIPC)
	//{
		/** Initialize Blackboard with the Blackboard the Assigned Behavior tree uses*/
	//	BlackboardComponent->InitializeBlackboard(*BehaviorTree->BlackboardAsset);

		/** Starts the Behaviortree*/
		//BehaviorComponent->StartTree(*BehaviorTree);

		/** Set blackboard targetkey*/
		//if (PlayerCharacter)
		//	/*AIPC->*/BehaviorComponent->GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>("Target", PlayerCharacter);

	//	/*AIPC->*/BehaviorComponent->GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>("SelfActor", GetController());
	//}
	PlayerCharacter = Cast<AFPS_Character>(GetWorld()->GetFirstPlayerController()->GetCharacter());
	f_CurrentHealth = f_MaxHealth;
}

void ABase_EnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (b_IsDead)
		return;
	CheckLineOfSight();
	if (b_PlayerInSight == true)
	{
		RotateToPlayer();
	}
}

float ABase_EnemyCharacter::GetHealthPercentage()
{
	float Percent = f_CurrentHealth / f_MaxHealth;
	return Percent;
}

void ABase_EnemyCharacter::SetController(AAI_BaseEnemyController* _Controller)
{
	AIPC = _Controller;
}

void ABase_EnemyCharacter::RotateToPlayer()
{
	if (PlayerCharacter)
	{
		FRotator PlayerRot = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), PlayerCharacter->GetActorLocation());
		FRotator NewRot = FMath::RInterpTo(GetActorRotation(), PlayerRot, FApp::GetDeltaTime(), 10);
		SetActorRotation(FRotator(0, NewRot.Yaw, NewRot.Roll));
	}
}

void ABase_EnemyCharacter::CheckLineOfSight()
{
	if (PlayerCharacter && Controller)
	{
		AFPS_Character* Player = Cast<AFPS_Character>(FireRaycast().GetActor());
		if (Player)
		{
			int HitNum = 0;
			Player = nullptr;
			Player = Cast<AFPS_Character>(FireRaycast(FVector(50.0f, 0.0f, 0.0f)).GetActor());
			if (Player)
				HitNum++;

			Player = nullptr;
			Player = Cast<AFPS_Character>(FireRaycast(FVector(-50.0f, 0.0f, 0.0f)).GetActor());
			if (Player)
				HitNum++;

			if (HitNum >= 2)
			{
				if (AIPC->BlackboardComponent)
				{
					if (b_PlayerInSight == false)
					{
						b_PlayerInSight = true;
						AIPC->BlackboardComponent->SetValueAsBool("TargetInLineOfSight", true);
						AIPC->BehaviorComponent->RestartTree();
					}
					return;
				}
			}
		}

		b_PlayerInSight = false;
		if (AIPC && AIPC->BlackboardComponent)
		{
			AIPC->BlackboardComponent->SetValueAsBool("TargetInLineOfSight", false);
		}
	}
}

bool ABase_EnemyCharacter::GetIsDead()
{
	return b_IsDead;
}

float ABase_EnemyCharacter::Damage(float Amount, AFPS_Character* _Player, FVector HitLocation)
{
	f_FinalDamageTaken = (Amount * Amount) / ((Amount + i_Toughness + _Player->GetPlayerStats()->GetPlayerLevel()) * 2);
	f_CurrentHealth -= f_FinalDamageTaken;
	DisplayCombatText(f_FinalDamageTaken, _Player);

	/** Create the Hit Particle*/
	HitPSC = UGameplayStatics::SpawnEmitterAttached(HitParticeSystem, GetMesh());
	if (HitPSC)
	{
		//	HitPSC->SetRelativeLocation(HitLocation);
		HitPSC->SetWorldLocation(HitLocation);
		HitPSC->bAutoDestroy = true;
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("NoParticleSystem!| Base_EnemyCharacter : Take Damage"));

	if (f_CurrentHealth <= 0 && b_IsDead == false)
	{
		f_CurrentHealth = 0;
		Death(_Player);
	}
	return f_FinalDamageTaken;
}

void ABase_EnemyCharacter::Death(AFPS_Character* _Player)
{
	b_IsDead = true;
	if (AIPC && AIPC->BlackboardComponent)
	{
		AIPC->BlackboardComponent->SetValueAsBool("b_IsDead", b_IsDead);
		AIPC->BlackboardComponent->SetValueAsBool("TargetInLineOfSight", false);
	}

	_Player->GetPlayerStats()->GainExperience(i_XPonKill);
	WeaponSpawner->ChoosePoolToSpawn();
	MeshPosition = GetMesh()->GetComponentLocation();
	GetMesh()->SetSimulatePhysics(true);
	GetCapsuleComponent()->SetActive(false);
	GetCapsuleComponent()->SetCollisionProfileName("NoCollision");
	GetCapsuleComponent()->SetEnableGravity(false);
	GetMesh()->SetCollisionProfileName("Ragdoll");
	if (ArenaGameMode) ArenaGameMode->CheckRoundEnd();
}

void ABase_EnemyCharacter::Respawn()
{
	if (GetCapsuleComponent() && GetMesh())
	{
		GetCapsuleComponent()->SetActive(true);
		GetMesh()->SetSimulatePhysics(false);
		GetMesh()->SetCollisionProfileName("CharacterMesh");
		GetCapsuleComponent()->SetCollisionProfileName("Pawn");
		GetCapsuleComponent()->SetEnableGravity(true);
		for (TActorIterator<AWaypoint_Manager> ActorItr(GetWorld()); ActorItr; ++ActorItr)
		{
			AWaypoint_Manager* WPM = *ActorItr;
			ASpawnPoint* SP = WPM->GetRandomSpawnPoint();
			if (SP != nullptr)
			{
				FVector NewPos = FVector(SP->GetActorLocation().X, SP->GetActorLocation().Y, SP->GetActorLocation().Z + 90.0f);
				GetCapsuleComponent()->SetWorldLocation(NewPos);
			}
		}
		GetMesh()->SetWorldLocationAndRotation(FVector(GetCapsuleComponent()->GetComponentLocation().X, GetCapsuleComponent()->GetComponentLocation().Y, GetCapsuleComponent()->GetComponentLocation().Z), FRotator(0.0f, 0.0f, 0.0f));
		FAttachmentTransformRules Attach(EAttachmentRule::SnapToTarget, true);
		GetMesh()->AttachToComponent(GetCapsuleComponent(), Attach);
		GetMesh()->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, -90.0f), FRotator(0.0f, -90.0f, 0.0f));
		f_CurrentHealth = f_MaxHealth;
		b_IsDead = false;
		if (AIPC && AIPC->BlackboardComponent)
			AIPC->BlackboardComponent->SetValueAsBool("b_IsDead", b_IsDead);
	}
}

void ABase_EnemyCharacter::SetArenaGameMode(AMyProjectGameModeBase* _GameMode)
{
	ArenaGameMode = _GameMode;
}

void ABase_EnemyCharacter::SetStats(int _i_RoundToSyncTo)
{
	f_MaxHealth = FMath::RandRange(
		float(f_MaxHealth + PlayerCharacter->GetPlayerStats()->GetPlayerLevel() * _i_RoundToSyncTo),
		f_MaxHealth + 1 + (PlayerCharacter->GetPlayerStats()->GetPlayerLevel() * _i_RoundToSyncTo) * f_MaxStatModifier);
	i_Toughness = FMath::RandRange(
		float(i_Toughness + PlayerCharacter->GetPlayerStats()->GetPlayerLevel() / 1.5f * _i_RoundToSyncTo),
		i_Toughness + 1 + (PlayerCharacter->GetPlayerStats()->GetPlayerLevel() / 1.5f * _i_RoundToSyncTo) * f_MaxStatModifier);
}

void ABase_EnemyCharacter::SetActiveState(bool _activeState)
{
	if (_activeState)
	{
		b_IsDead = false;
		if (AIPC && AIPC->BehaviorComponent)
		AIPC->BehaviorComponent->StartTree(*BehaviorTree);
	}
	else
	{
		b_IsDead = true;
		if (AIPC && AIPC->BehaviorComponent)
		AIPC->BehaviorComponent->StopTree(EBTStopMode::Forced);
	}
}

FHitResult ABase_EnemyCharacter::FireRaycast(FVector _Offset)
{
	FRotator AimDir;
	FVector StartTrace;
	Controller->GetPlayerViewPoint(StartTrace, AimDir);

	StartTrace = FVector(StartTrace.X + _Offset.X, StartTrace.Y + _Offset.Y, StartTrace.Z + _Offset.Z);
	FVector EndTrace = PlayerCharacter->GetActorLocation();
	FCollisionQueryParams TraceParams("", true, this);
	FHitResult Hit(ForceInit);


	//DrawDebugLine(GetWorld(), StartTrace, EndTrace, FColor::Orange);
	GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_PhysicsBody, TraceParams);

	return Hit;
}

void ABase_EnemyCharacter::DisplayCombatText(float _DamageDone, AFPS_Character* _Player)
{
	CombatText = GetWorld()->SpawnActor<ACombatTextActor>(ACombatTextActor::StaticClass(), FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + 50.0f), FRotator(0.0f, 0.0f, 0.0f));

	float Rounded = roundf(_DamageDone);
	if (FMath::Abs(_DamageDone - Rounded) < FMath::Pow(10, -1 * 1))
	{
		_DamageDone = Rounded;
	}
	FNumberFormattingOptions NumberFormat;					//Text.h
	NumberFormat.MinimumIntegralDigits = (false) ? 1 : 0;
	NumberFormat.MaximumIntegralDigits = 10000;
	NumberFormat.MinimumFractionalDigits = 2;
	NumberFormat.MaximumFractionalDigits = 2;
	CombatText->SetText(FText::AsNumber(_DamageDone, &NumberFormat).ToString(), _Player);
}