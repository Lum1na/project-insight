// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Public/FPS_Character.h"
#include "MyProjectGameModeBase.h"
#include "SMG.h"



ASMG::ASMG()
{
	WeaponConfig.ClipSize = 30;
	WeaponConfig.ShotCost = 1;
	WeaponConfig.ReloadTime = 2.5f;
	WeaponConfig.WeaponSpread = 3.0f;
	WeaponConfig.WeaponRange = 10000.0f;
	WeaponConfig.ShotDelay = .17;
	WeaponConfig.CurrentClipAmmo = WeaponConfig.ClipSize;

	AmmoType = EWeaponAmmoType::AmmoType::ESMGAmmo;
	ProjectileType = EWeaponProjectile::EBullet;
	WeaponConfig.FireType = EWeaponFireType::EAutoFire;
	Name = "SMG";
}

void ASMG::GenerateStats(float _f_WeaponStatsModifier)
{
	AFPS_Character* Player = Cast<AFPS_Character>(GetWorld()->GetFirstPlayerController()->GetPawn());
	int PlayerLevel = Player->GetPlayerStats()->GetPlayerLevel();

	AMyProjectGameModeBase* GameMode = Cast<AMyProjectGameModeBase>(GetWorld()->GetAuthGameMode());
	int round = GameMode->GetCurrentRound();
	int ArenaroundMultiplier = GameMode->GetArenaRoundMultiplier();

	WeaponConfig.AmountOfPellets = 0;
	WeaponConfig.ClipSize = FMath::RandRange(StatGenerationData.MinClipSize, StatGenerationData.MaxClipSize);

	WeaponConfig.Damage = FMath::RandRange(StatGenerationData.f_WeaponBaseDamage + PlayerLevel + (round * ArenaroundMultiplier) * _f_WeaponStatsModifier,
		StatGenerationData.f_WeaponBaseDamage + 1 + PlayerLevel + (round * ArenaroundMultiplier) *  (f_MaxStatModifier * _f_WeaponStatsModifier));

	WeaponConfig.ReloadTime = FMath::RandRange(StatGenerationData.MinReloadTime, StatGenerationData.MaxReloadTime);
	WeaponConfig.ShotCost = 1;
	WeaponConfig.ShotDelay = FMath::RandRange(StatGenerationData.MinShotDelay, StatGenerationData.MaxShotDelay);
	WeaponConfig.WeaponRange = 10000.0f;
	WeaponConfig.WeaponSpread = FMath::RandRange(StatGenerationData.MinWeaponSpread, StatGenerationData.MaxWeaponSpread);
	WeaponConfig.CurrentClipAmmo = WeaponConfig.ClipSize;
}

