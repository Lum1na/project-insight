// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Public/Weapon_BaseClass.h"
#include "Public/FPS_Character.h"
#include "Base_EnemyCharacter.h"
#include "TargetDummyEnemy.h"
#include "Engine.h"

// Sets default values
AWeapon_BaseClass::AWeapon_BaseClass()
{
	PrimaryActorTick.bCanEverTick = true;

	/*Getting the Player Controller*/
	Playercontroller = Cast<APlayerController>(GetOwner());

	/** Setting Default WeaponConfig Stats*/
	WeaponConfig.ClipSize = 30;
	WeaponConfig.ShotCost = 1;
	WeaponConfig.ReloadTime = 2.5f;
	WeaponConfig.WeaponSpread = 3.0f;
	WeaponConfig.WeaponRange = 10000.0f;
	WeaponConfig.ShotDelay = 1;
	WeaponConfig.CurrentClipAmmo = WeaponConfig.ClipSize;
	WeaponConfig.f_BurstShotDelay = 0.1f;

	b_CanFire = true;
	f_MaxStatModifier = 1.25f;

	LootPSC = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Loot Beam Comp"));
	LootPSC->SetupAttachment(RootComponent);
	bShowLootPSC = true;
	bCanPlayHitEffectSound = true;
}

void AWeapon_BaseClass::BeginPlay()
{
	Super::BeginPlay();
}

void AWeapon_BaseClass::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bShowLootPSC)
	{
		if (LootPSC)
			LootPSC->SetWorldRotation(FVector::UpVector.Rotation());
	}
}

#pragma region FireWeapon
void AWeapon_BaseClass::Fire()
{
	if (Reloading == false && b_CanFire &&f_FireDelayTimer <= 0 && WeaponConfig.CurrentClipAmmo > 0)
	{
		PlayWeaponSound(FireSound);
		FTimerHandle StopShotAnimation;
		FTimerDelegate ShotAnimTimerDelegate;
		ShotAnimTimerDelegate.BindUFunction(this, FName("StopAnimMontage"), FireAnim);
		float Timer = PlayWeaponAnimation(FireAnim);
		GetWorld()->GetTimerManager().SetTimer(StopShotAnimation, ShotAnimTimerDelegate, Timer, false, Timer);
		SetFireDelay(true);
		switch (ProjectileType)
		{

		case EWeaponProjectile::ESpread:

			if (WeaponConfig.CurrentClipAmmo <= WeaponConfig.AmountOfPellets)
			{
				for (int32 i = 0; i <= WeaponConfig.CurrentClipAmmo; i++)
				{
					InstantFire();
					if (WeaponConfig.ShotCost == WeaponConfig.AmountOfPellets)
						UseAmmo();
				}
				if (WeaponConfig.ShotCost != WeaponConfig.AmountOfPellets)
					UseAmmo();
			}
			else if (WeaponConfig.CurrentClipAmmo > WeaponConfig.AmountOfPellets)
			{
				for (int32 i = 0; i < WeaponConfig.AmountOfPellets; i++)
				{
					InstantFire();
					if (WeaponConfig.ShotCost == WeaponConfig.AmountOfPellets)
						UseAmmo();
				}
				if (WeaponConfig.ShotCost != WeaponConfig.AmountOfPellets)
					UseAmmo();
			}
			break;

		case EWeaponProjectile::EBullet:
			InstantFire();
			UseAmmo();
			break;

		case EWeaponProjectile::EProjectile:
			ProjectileFire();
			UseAmmo();
			break;

		default:
			break;
		}
	}
	else if (WeaponConfig.CurrentClipAmmo <= 0 && f_FireDelayTimer <= 0)
	{
		SetFireDelay(true);
		StopAnimMontage(FireAnim);
		PlayWeaponSound(ClipEmptySound);
	}
	else if (f_FireDelayTimer > 0)
	{
		StopAnimMontage(FireAnim);
	}
}

void AWeapon_BaseClass::BurstFire()
{
	if (f_FireDelayTimer <= 0 && b_CanFire && WeaponConfig.CurrentClipAmmo > 0)
	{
		b_CanFire = false;
		b_isBursting = true;
		i_BurstShotsFired = 0;
		GetWorld()->GetTimerManager().SetTimer(BurstFireTimerHandle, this, &AWeapon_BaseClass::ExecuteBurst, WeaponConfig.f_BurstShotDelay, true);
	}
	else if (WeaponConfig.CurrentClipAmmo <= 0 && f_FireDelayTimer <= 0)
	{
		SetFireDelay(true);
		StopAnimMontage(FireAnim);
		PlayWeaponSound(ClipEmptySound);
	}
	else if (f_FireDelayTimer > 0)
	{
		StopAnimMontage(FireAnim);
	}
}

void AWeapon_BaseClass::ExecuteBurst()
{
	if (WeaponConfig.CurrentClipAmmo > 0)
	{
		i_BurstShotsFired++;
		PlayWeaponSound(FireSound);
		PlayWeaponAnimation(FireAnim);
		InstantFire();
		UseAmmo();
		if (i_BurstShotsFired >= WeaponConfig.i_ShotsPerBurst)
		{
			GetWorld()->GetTimerManager().ClearTimer(BurstFireTimerHandle);
			StopAnimMontage(FireAnim);
			b_CanFire = true;
			b_isBursting = false;
			SetFireDelay(true);
		}
	}
	else
	{
		GetWorld()->GetTimerManager().ClearTimer(BurstFireTimerHandle);
		b_isBursting = false;		  
		StopAnimMontage(FireAnim);
		PlayWeaponSound(ClipEmptySound);
	}
}

void AWeapon_BaseClass::UseAmmo()
{
	if (WeaponConfig.bUnlimitedClip == false)
		WeaponConfig.CurrentClipAmmo--;
}

void AWeapon_BaseClass::InstantFire()
{
	/** Spawn MuzzleFlash Particle system*/
	MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, Mesh, "Muzzle");
	if (MuzzlePSC)
	{
		MuzzlePSC->SetRelativeLocation(FVector(0.0f));
		MuzzlePSC->bAutoDestroy = true;
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("No Muzzle ParticleSystem!| Weapon_BaseClass : InstantFire"));

	/** Player Camerashake*/
	if (MyPawn)
		MyPawn->ExecuteCameraShake();

	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);

	//Get Character Accuracy. Multiply it by 1.5 if the Player is Aiming
	float PlayerAccuracy = MyPawn->GetPlayerStats()->GetAccuracy();
	if (GetIsAiming() == true)
		PlayerAccuracy *= 1.5f;


	const float CurrentSpread = ((PlayerAccuracy + f_AccuracyModifier) * WeaponConfig.WeaponSpread) / (PlayerAccuracy - WeaponConfig.WeaponSpread);
	const float SpreadCone = FMath::DegreesToRadians(CurrentSpread);

	/** Get Player Viewpoint*/
	FRotator AimDir;
	FVector StartTrace;
	GetWorld()->GetFirstLocalPlayerFromController()->GetPlayerController(GetWorld())->GetPlayerViewPoint(StartTrace, AimDir);

	/** Set the Start and endpoints for the Raycast*/
	const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir.Vector(), SpreadCone, SpreadCone);
	const FVector EndTrace = StartTrace + ShootDir * WeaponConfig.WeaponRange;

	/** Get the FHitResult of the Raycast*/
	const FHitResult Impact = WeaponTrace(StartTrace, EndTrace);
	/** Pass it to the Function that handles the actual hit event*/
	ProcessInstantHit(Impact, StartTrace, ShootDir, RandomSeed, CurrentSpread);

	/** Create a Particle system. Trail between the gun barrel and the hit Position*/
	TrailPSC = UGameplayStatics::SpawnEmitterAttached(TrailFX, Mesh, "Muzzle");
	if (TrailPSC)
	{
		TrailPSC->SetRelativeLocation(FVector(0.0f));
		TrailPSC->SetBeamSourcePoint(0, GetActorLocation(), 1);
		TrailPSC->SetBeamEndPoint(1, Impact.Location);
		TrailPSC->bAutoDestroy = true;
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("No Trail ParticleSystem!| Weapon_BaseClass : InstantFire"));
}

void AWeapon_BaseClass::ResetFireTimer()
{
	f_FireDelayTimer = 0.0f;
	b_CanFire = true;
	bCanPlayHitEffectSound = true;
}

FHitResult AWeapon_BaseClass::WeaponTrace(const FVector & TraceFrom, const FVector & TraceTo) const
{
	/** Setting Trace Parameters*/
	static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
	FCollisionQueryParams  TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;
	TraceParams.AddIgnoredActor(this);
	TraceParams.AddIgnoredActor(MyPawn);

	FHitResult Hit(ForceInit);
	/** Shooting the Raycast*/
	GetWorld()->LineTraceSingleByChannel(Hit, TraceFrom, TraceTo, ECC_PhysicsBody, TraceParams);
	return Hit;
}

void AWeapon_BaseClass::ProcessInstantHit(const FHitResult & Impact, const FVector & Origin, const FVector & ShotDir, int32 RandomSeed, float ProjectileSpread)
{
	const FVector EndTrace = Origin + ShotDir * WeaponConfig.WeaponRange;
	const FVector EndPoint = Impact.GetActor() ? Impact.ImpactPoint : EndTrace;

	ABase_EnemyCharacter* Enemy = Cast<ABase_EnemyCharacter>(Impact.GetActor());
	ATargetDummyEnemy* TargetDummyEnemy = Cast<ATargetDummyEnemy>(Impact.GetActor());
	if (Enemy)
	{
		if (Enemy->b_IsDead == true)
			return;
		if (MyPawn && bCanPlayHitEffectSound)
		{
			bCanPlayHitEffectSound = false;
			MyPawn->PlayHitNotification();
		}

		int32 TotalDamage = WeaponConfig.Damage;
		float CritNumb = FMath::RandRange(0, 100);
		if (CritNumb <= PlayerStats->GetCritChance())
		{
			TotalDamage += (WeaponConfig.Damage / 100) * PlayerStats->GetCritDamage();
		}
		float f_HealthFromLifeLeach = (Enemy->Damage(TotalDamage, MyPawn, Impact.Location) / 100) * PlayerStats->GetLifeLeach();
		PlayerStats->ChangeCurrentHealth(f_HealthFromLifeLeach);
	}
	else
	{
		ImpactPSC = UGameplayStatics::SpawnEmitterAttached(ImpactFX, Impact.GetComponent());
		if (ImpactPSC)
		{
			ImpactPSC->SetWorldLocation(Impact.Location);
			ImpactPSC->bAutoDestroy = true;
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("No Impact ParticleSystem!| Weapon_BaseClass : ProcessInstantHit"));
	}
	if (TargetDummyEnemy)
		TargetDummyEnemy->HitTarget();
}

void AWeapon_BaseClass::SetFireDelay(bool _b_bool)
{
	f_FireDelayTimer = WeaponConfig.ShotDelay;
	b_DecreaseFireDelayTimer = _b_bool;
	GetWorld()->GetTimerManager().SetTimer(FireDelayTimerHandle, this, &AWeapon_BaseClass::ResetFireTimer, f_FireDelayTimer);
}

int32 AWeapon_BaseClass::GetPlayerLevel()
{
	return PlayerStats->GetPlayerLevel();
}

void AWeapon_BaseClass::ProjectileFire()
{
	/*Overwritten in derived classes*/
}

UAudioComponent * AWeapon_BaseClass::PlayWeaponSound(USoundCue * Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound && MyPawn)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, MyPawn->GetRootComponent());
	}
	return AC;
}

float AWeapon_BaseClass::PlayWeaponAnimation(UAnimMontage * Animation)
{
	float Duration = 0.0f;
	if (MyPawn)
	{
		USkeletalMeshComponent* UseMesh = MyPawn->FirstPersonMesh;
		if (UseMesh)
		{
			float  Playrate = 0.0f;
			if (Animation == ReloadAnim)
				Playrate = Animation->CalculateSequenceLength() / WeaponConfig.ReloadTime;
			else
				Playrate = 1.0f;

			Duration = UseMesh->AnimScriptInstance->Montage_Play(Animation, Playrate);
		}
	}

	return Duration;
}

void AWeapon_BaseClass::StopAnimMontage(UAnimMontage * Animation)
{
	if (MyPawn)
	{
		USkeletalMeshComponent* UseMesh = MyPawn->FirstPersonMesh;
		if (Animation && UseMesh && UseMesh->AnimScriptInstance->Montage_IsPlaying(Animation))
		{
			UseMesh->AnimScriptInstance->Montage_Stop(Animation->BlendOut.GetBlendTime(), Animation);
		}
	}
}

bool AWeapon_BaseClass::GetIsAiming()
{
	return b_Aiming;
}
void AWeapon_BaseClass::SetIsAiming(bool _b_aiming)
{
	b_Aiming = _b_aiming;
}

APawn* AWeapon_BaseClass::GetOwningPawn()
{
	return MyPawn;
}

#pragma endregion

#pragma region Reload
void AWeapon_BaseClass::StartReload()
{
	if (b_isBursting)
		return;
	/** If Player is dead dont reload*/
	if (MyPawn)
		if (MyPawn->b_IsDead == true)
			return;
	StopAnimMontage(FireAnim);
	if (WeaponConfig.CurrentClipAmmo < WeaponConfig.ClipSize && Reloading == false)
	{
		b_CanFire = false;
		Reloading = true;
		//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, "Reloading!");
		if (ReloadAnim)
			GetWorld()->GetTimerManager().SetTimer(ReloadTimerHandle, this, &AWeapon_BaseClass::Reload, ReloadAnim->SequenceLength, false);
		else
			GetWorld()->GetTimerManager().SetTimer(ReloadTimerHandle, this, &AWeapon_BaseClass::Reload, WeaponConfig.ReloadTime, false);

		PlayWeaponAnimation(ReloadAnim);
	}
}

void AWeapon_BaseClass::Reload()
{
	if (WeaponConfig.bUnlimitedReserve)
	{
		WeaponConfig.CurrentClipAmmo += (WeaponConfig.ClipSize - WeaponConfig.CurrentClipAmmo);
		FinishReload();
		return;
	}
	AmmoInUse = PlayerStats->GetCurrentAmmoInUse();
	if (*AmmoInUse >= (WeaponConfig.ClipSize - WeaponConfig.CurrentClipAmmo))
	{
		*AmmoInUse -= (WeaponConfig.ClipSize - WeaponConfig.CurrentClipAmmo);
		WeaponConfig.CurrentClipAmmo += (WeaponConfig.ClipSize - WeaponConfig.CurrentClipAmmo);
	}
	else
	{
		WeaponConfig.CurrentClipAmmo += *AmmoInUse;
		*AmmoInUse -= (WeaponConfig.ClipSize - WeaponConfig.CurrentClipAmmo);
		if (*AmmoInUse < 0)	*AmmoInUse = 0;
	}
	FinishReload();
}

void AWeapon_BaseClass::StopReload()
{
	b_CanFire = true;
	Reloading = false;
	StopAnimMontage(ReloadAnim);
	GetWorld()->GetTimerManager().ClearTimer(ReloadTimerHandle);
}

void AWeapon_BaseClass::FinishReload()
{
	GetWorld()->GetTimerManager().ClearTimer(ReloadTimerHandle);
	b_CanFire = true;
	Reloading = false;
	StopAnimMontage(ReloadAnim);
}
#pragma endregion

#pragma region Equip & UnEquip
void AWeapon_BaseClass::EquipWeapon()
{
	CollisionComp->SetSimulatePhysics(false);
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	AttachToPlayer();
	bShowLootPSC = false;
	if (LootPSC)
		LootPSC->DeactivateSystem();
}


void AWeapon_BaseClass::AttachToPlayer()
{
	if (MyPawn)
	{
		USkeletalMeshComponent* FirstPersonMesh = MyPawn->FirstPersonMesh;
		if (ActiveWeapon)	Mesh->SetHiddenInGame(false);
		else Mesh->SetHiddenInGame(true);
		if (AmmoType == EWeaponAmmoType::ERifleAmmo)
			Mesh->AttachToComponent(FirstPersonMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, "RifleWeaponSocket");
		else
			Mesh->AttachToComponent(FirstPersonMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, "WeaponSocket");

		if (Reloading) StopReload();
		StopAnimMontage(FireAnim);
	}
}

void AWeapon_BaseClass::DetachFromPlayer()
{
	bShowLootPSC = true;
	if (LootPSC)
		LootPSC->ActivateSystem();
	Mesh->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	Mesh->AttachToComponent(CollisionComp, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CollisionComp->IgnoreActorWhenMoving(MyPawn, true);
	CollisionComp->SetSimulatePhysics(true);
	if (MyPawn)
		SetActorLocation(MyPawn->GetActorLocation());

	FVector CamLoc;
	FRotator CamRot;
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetPlayerViewPoint(CamLoc, CamRot);
	const FVector LookDirection = CamRot.Vector();
	CollisionComp->AddImpulseAtLocation(LookDirection * 5000.0f, GetActorLocation());

	Mesh->SetHiddenInGame(false);

	StopReload();
	StopAnimMontage(FireAnim);
}

void AWeapon_BaseClass::SetActiveWeapon(bool _b_Active)
{
	ActiveWeapon = _b_Active;
}

void AWeapon_BaseClass::HideWeapon()
{
	Mesh->SetHiddenInGame(true);
}

void AWeapon_BaseClass::SetOwningPawn(AFPS_Character * NewOwner, UPlayerStats* NewPlayerStats)
{
	if (MyPawn != NewOwner)
	{
		PlayerStats = NewPlayerStats;
		MyPawn = NewOwner;
	}
}

void AWeapon_BaseClass::DropWeapon()
{
	CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Mesh->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	bShowLootPSC = true;
	LootPSC->ActivateSystem();
}
#pragma endregion

void AWeapon_BaseClass::GenerateStats(float _f_WeaponStatsModifier)
{
	//Overwritten in Child Classes
}

void AWeapon_BaseClass::CopyStats(FWeaponData _WeapConfig, TEnumAsByte<EWeaponFireType::FireType>  _FireType)
{
	WeaponConfig.ClipSize = _WeapConfig.ClipSize;
	WeaponConfig.ShotCost = _WeapConfig.ShotCost;
	WeaponConfig.Damage = _WeapConfig.Damage;
	WeaponConfig.CurrentClipAmmo = _WeapConfig.CurrentClipAmmo;
	WeaponConfig.ReloadTime = _WeapConfig.ReloadTime;
	WeaponConfig.ShotDelay = _WeapConfig.ShotDelay;
	WeaponConfig.i_ShotsPerBurst = _WeapConfig.i_ShotsPerBurst;
	WeaponConfig.WeaponRange = _WeapConfig.WeaponRange;
	WeaponConfig.WeaponSpread = _WeapConfig.WeaponSpread;
	WeaponConfig.AmountOfPellets = _WeapConfig.AmountOfPellets;
	WeaponConfig.InventorySlot = _WeapConfig.InventorySlot;
	WeaponConfig.bUnlimitedClip = _WeapConfig.bUnlimitedClip;
	WeaponConfig.bUnlimitedReserve = _WeapConfig.bUnlimitedReserve;
	WeaponConfig.f_BurstShotDelay = _WeapConfig.f_BurstShotDelay;

	WeaponConfig.FireType = _FireType;
}