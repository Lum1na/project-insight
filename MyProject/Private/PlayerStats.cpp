#include "MyProject.h"
#include "Weapon_BaseClass.h"
#include "FPS_Character.h"
#include "FPS_PlayerController.h"
#include "PlayerStats.h"
#include "Armor_Chestpiece.h"

float UPlayerStats::GetExperiencePercentage()
{
	float Percentage = (float)PlayerStatsConfig.i_CurrentExperience / (float)PlayerStatsConfig.i_ExperienceToNextLevel;
	return Percentage;
}

float UPlayerStats::GetArmorPercentage()
{
	float Percentage = PlayerStatsConfig.f_CurrentArmor / PlayerStatsConfig.f_MaxArmor;
	return Percentage;
}

float UPlayerStats::GetHealthPercentage()
{
	float Percentage = PlayerStatsConfig.f_CurrentHealth / PlayerStatsConfig.f_MaxHealth;
	return Percentage;
}

int32 UPlayerStats::GetCurrentExperience()
{
	return PlayerStatsConfig.i_CurrentExperience;
}
int32 UPlayerStats::GetExperienceToNextLevel()
{
	return PlayerStatsConfig.i_ExperienceToNextLevel;
}
int32 UPlayerStats::GetPlayerLevel()
{
	return PlayerStatsConfig.i_PlayerLevel;
}
float UPlayerStats::GetCurrentArmor()
{
	return PlayerStatsConfig.f_CurrentArmor;
}
float UPlayerStats::GetMaxArmor()
{
	return PlayerStatsConfig.f_MaxArmor;
}
float UPlayerStats::GetVitality()
{
	return PlayerStatsConfig.f_Vitality;
}
float UPlayerStats::GetAccuracy()
{
	return PlayerStatsConfig.f_Accuracy;
}
float UPlayerStats::GetCritChance()
{
	return PlayerStatsConfig.f_CritChance;
}
float UPlayerStats::GetCritDamage()
{
	return PlayerStatsConfig.f_CritDamage;
}
float UPlayerStats::GetLifeLeach()
{
	return PlayerStatsConfig.f_Lifeleach;
}
float UPlayerStats::GetToughness()
{
	return PlayerStatsConfig.f_Toughness;
}
float UPlayerStats::GetCurrentHealth()
{
	return PlayerStatsConfig.f_CurrentHealth;
}
float UPlayerStats::GetMaxHealth()
{
	return PlayerStatsConfig.f_MaxHealth;
}


void UPlayerStats::SetCurrentHealth(float NewCurrentHealthAmount)
{
	PlayerStatsConfig.f_CurrentHealth = NewCurrentHealthAmount;
	PlayerStatsConfig.f_CurrentHealth = FMath::Clamp(PlayerStatsConfig.f_CurrentHealth, 0.0f, PlayerStatsConfig.f_CurrentHealth);
}
void UPlayerStats::SetMaxHealth(float NewMaxHealtAmount)
{
	PlayerStatsConfig.f_MaxHealth = NewMaxHealtAmount;
	PlayerStatsConfig.f_MaxHealth = FMath::Clamp(PlayerStatsConfig.f_MaxHealth, 0.0f, PlayerStatsConfig.f_MaxHealth);
}
void UPlayerStats::SetMaxArmor(float NewArmorAmount)
{
	PlayerStatsConfig.f_MaxArmor = NewArmorAmount;
	PlayerStatsConfig.f_MaxArmor = FMath::Clamp(PlayerStatsConfig.f_MaxArmor, 0.0f, PlayerStatsConfig.f_MaxArmor);
}
void UPlayerStats::SetCurrentArmor(float NewArmorAmount)
{
	PlayerStatsConfig.f_CurrentArmor = NewArmorAmount;
	PlayerStatsConfig.f_CurrentArmor = FMath::Clamp(PlayerStatsConfig.f_CurrentArmor, 0.0f, PlayerStatsConfig.f_MaxArmor);
	if (GetWorld())
		GetWorld()->GetTimerManager().SetTimer(ArmorRechargeTimerHandle, this, &UPlayerStats::ReChargeArmor, f_ArmorRechargeRate, true, f_ArmorRechargeDelayTime);
}
void UPlayerStats::SetVitality(float NewVitalityAmount)
{
	PlayerStatsConfig.f_Vitality = NewVitalityAmount;
	PlayerStatsConfig.f_Vitality = FMath::Clamp(PlayerStatsConfig.f_Vitality, 0.0f, PlayerStatsConfig.f_Vitality);
}
void UPlayerStats::SetToughness(float NewToughnessAmount)
{
	PlayerStatsConfig.f_Toughness = NewToughnessAmount;
	PlayerStatsConfig.f_Toughness = FMath::Clamp(PlayerStatsConfig.f_Toughness, 0.0f, PlayerStatsConfig.f_Toughness);
}
void UPlayerStats::SetAccuracy(float NewAccuracyAmount)
{
	PlayerStatsConfig.f_Accuracy = NewAccuracyAmount;
	PlayerStatsConfig.f_Accuracy = FMath::Clamp(PlayerStatsConfig.f_Accuracy, 0.0f, PlayerStatsConfig.f_Accuracy);
}
void UPlayerStats::SetCritChance(float NewCritChanceAmount)
{
	PlayerStatsConfig.f_CritChance = NewCritChanceAmount;
	PlayerStatsConfig.f_CritChance = FMath::Clamp(PlayerStatsConfig.f_CritChance, 0.0f, PlayerStatsConfig.f_CritChance);
}
void UPlayerStats::SetCritDamage(float NewCritDamageAmount)
{
	PlayerStatsConfig.f_CritDamage = NewCritDamageAmount;
	PlayerStatsConfig.f_CritDamage = FMath::Clamp(PlayerStatsConfig.f_CritDamage, 0.0f, PlayerStatsConfig.f_CritDamage);
}
void UPlayerStats::SetLifeLeach(float NewLifeLeachAmount)
{
	PlayerStatsConfig.f_Lifeleach = NewLifeLeachAmount;
	PlayerStatsConfig.f_Lifeleach = FMath::Clamp(PlayerStatsConfig.f_Lifeleach, 0.0f, PlayerStatsConfig.f_Lifeleach);
}
void UPlayerStats::SetPlayerLevel(float NewPlayerLevel)
{
	PlayerStatsConfig.i_PlayerLevel = NewPlayerLevel;
	PlayerStatsConfig.i_PlayerLevel = FMath::Clamp(PlayerStatsConfig.i_PlayerLevel, 1, PlayerStatsConfig.i_PlayerLevel);
	SetVitality(20.0f + PlayerStatsConfig.i_PlayerLevel);
}
void UPlayerStats::SetCurrentExperience(int NewCurrentExperience)
{
	PlayerStatsConfig.i_CurrentExperience = NewCurrentExperience;
	PlayerStatsConfig.i_CurrentExperience = FMath::Clamp(PlayerStatsConfig.i_CurrentExperience, 0, PlayerStatsConfig.i_CurrentExperience);
}
void UPlayerStats::SetExperienceNeededForNextLevel(int NewExperienceToNextLevel)
{
	PlayerStatsConfig.i_ExperienceToNextLevel = NewExperienceToNextLevel;
	PlayerStatsConfig.i_ExperienceToNextLevel = FMath::Clamp(PlayerStatsConfig.i_ExperienceToNextLevel, 0, PlayerStatsConfig.i_ExperienceToNextLevel);
}


UPlayerStats::UPlayerStats()
{
	PrimaryComponentTick.bCanEverTick = false;
	/*Setting Default Values*/
	WeaponDefaultAmmoCount = 300;

	SMGAmmo = WeaponDefaultAmmoCount;
	RifleAmmo = WeaponDefaultAmmoCount;
	RocketLauncherAmmo = WeaponDefaultAmmoCount;
	ShotgunAmmo = WeaponDefaultAmmoCount;

	SetMaxArmor(0.0f);
	SetCurrentArmor(GetMaxArmor());
	SetVitality(20.0f);
	SetAccuracy(20.0f);
	SetToughness(20.0f);
	SetCritChance(5.0f);
	SetCritDamage(50.0f);
	SetLifeLeach(0.0f);

	SetMaxHealth(100.0f + GetVitality());
	SetCurrentHealth(GetMaxHealth());
	SetPlayerLevel(1);

	bIsCharacterStatsShowing = false;
	SetCurrentExperience(0);
	SetExperienceNeededForNextLevel(150);

	CurrentAmmoInUse = nullptr;
	b_MaxLevelReached = false;
}

void UPlayerStats::BeginPlay()
{
	Super::BeginPlay();
	if (Owner)
		Inventory = Owner->GetPlayerInventory();
	GetWorld()->GetTimerManager().SetTimer(ArmorRechargeTimerHandle, this, &UPlayerStats::ReChargeArmor, f_ArmorRechargeRate, true, f_ArmorRechargeDelayTime);
}

void UPlayerStats::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UPlayerStats::ChangeCurrentHealth(float f_AmountToAdd)
{
	SetCurrentHealth(GetCurrentHealth() + f_AmountToAdd);
}

void UPlayerStats::ReduceArmor(float _f_AmountToChangeBy)
{
	SetCurrentArmor(GetCurrentArmor() - _f_AmountToChangeBy);
	GetWorld()->GetTimerManager().SetTimer(ArmorRechargeTimerHandle, this, &UPlayerStats::ReChargeArmor, f_ArmorRechargeRate, true, f_ArmorRechargeDelayTime);
}

void UPlayerStats::ReChargeArmor()
{
	SetCurrentArmor(GetCurrentArmor() + (GetMaxArmor() / 100) * 10);
	if (GetCurrentArmor() >= GetMaxArmor())
	{
		GetWorld()->GetTimerManager().ClearTimer(ArmorRechargeTimerHandle);
		SetCurrentArmor(GetMaxArmor());
	}
}

void UPlayerStats::GainExperience(int32 AmountOfExperience)
{
	if (GetPlayerLevel() == i_MaxPlayerLevel)
	{
		SetPlayerLevel(i_MaxPlayerLevel);
		return;
	}
	SetCurrentExperience(GetCurrentExperience() + AmountOfExperience);
	if (GetCurrentExperience() > GetExperienceToNextLevel())
	{
		int32 overflowexp = 0;
		overflowexp = (GetCurrentExperience() + AmountOfExperience) - GetExperienceToNextLevel();
		SetCurrentExperience(overflowexp);
		LevelUp();
		while (GetCurrentExperience() >= GetExperienceToNextLevel())
		{
			overflowexp -= GetExperienceToNextLevel();
			SetCurrentExperience(overflowexp);
			LevelUp();
		}
	}
	else if (GetCurrentExperience() == GetExperienceToNextLevel())
	{
		SetCurrentExperience(0);
		LevelUp();
	}
}

void UPlayerStats::LevelUp()
{
	SetPlayerLevel(GetPlayerLevel() + 1);
	if (GetPlayerLevel() == i_MaxPlayerLevel)
	{
		SetExperienceNeededForNextLevel(0);
		SetCurrentExperience(0);
		b_MaxLevelReached = true;
	}
	else
	{
		SetExperienceNeededForNextLevel(100 + 50 * GetPlayerLevel());
		b_MaxLevelReached = false;
	}
	SetCurrentHealth(GetMaxHealth());
}

#pragma region CharacterStats Toggle
ESlateVisibility UPlayerStats::GetCharacterStatsShowing()
{
	if (bIsCharacterStatsShowing == true)
		return ESlateVisibility::Visible;

	return ESlateVisibility::Hidden;
}

void UPlayerStats::ToggleCharacterStatsShowing()
{
	bIsCharacterStatsShowing = !bIsCharacterStatsShowing;

	if (bIsCharacterStatsShowing == true)
	{
		if (Owner)
		{
			Owner->ShowCursor();
			if (Owner->Player_Controller)
				Owner->Player_Controller->AddLastOpenedHudElement(EHudElements::Characterstats);
		}
	}
	else
	{
		if (Owner)
		{
			if (Owner->Player_Controller)
				Owner->Player_Controller->RemoveHudElementFromList(EHudElements::Characterstats);
			Owner->HideCursor();
		}
	}
}
#pragma endregion

#pragma region Getter&Setters
void UPlayerStats::SetCurrentAmmoInUse()
{
	EWeaponAmmoType::AmmoType AmmoType;
	if (Owner && Owner->CurrentWeapon)
	{
		AmmoType = Owner->CurrentWeapon->AmmoType;

		if (AmmoType == EWeaponAmmoType::AmmoType::ESMGAmmo)
			CurrentAmmoInUse = &SMGAmmo;

		else if (AmmoType == EWeaponAmmoType::AmmoType::EShotgunAmmo)
			CurrentAmmoInUse = &ShotgunAmmo;

		else if (AmmoType == EWeaponAmmoType::AmmoType::ERocketLauncherAmmo)
			CurrentAmmoInUse = &RocketLauncherAmmo;

		else if (AmmoType == EWeaponAmmoType::ERifleAmmo)
			CurrentAmmoInUse = &RifleAmmo;
	}
	else
	{
		int i = 0;
		CurrentAmmoInUse = &i;
	}
}

int32* UPlayerStats::GetCurrentAmmoInUse()
{
	return CurrentAmmoInUse;
}

int32 UPlayerStats::GetReserveAmmo()
{
	if (CurrentAmmoInUse)
	{
		ammoInUse = *CurrentAmmoInUse;
		return ammoInUse;
	}
	else
		return 0;
}

void UPlayerStats::SetOwner(AFPS_Character* _Owner)
{
	Owner = _Owner;
	if (Owner)
		Inventory = Owner->GetPlayerInventory();
}
#pragma endregion
