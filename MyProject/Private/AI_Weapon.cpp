// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "AI_Weapon.h" 

AAI_Weapon::AAI_Weapon()
{
	PrimaryActorTick.bCanEverTick = false;
	/** Createing Components*/
	CollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Component"));
	RootComponent = CollisionComponent;
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	WeaponMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);	
	/**Settinge Ammo*/
	WeaponConfig.i_CurrentAmmoInClip = WeaponConfig.i_AmmoInClip;
}

void AAI_Weapon::BeginPlay()
{
	Super::BeginPlay();	  	
}


void AAI_Weapon::EquipWeapon(USkeletalMeshComponent* _MeshToAttachTo)
{
	/** Disable Collision*/
	CollisionComponent->SetSimulatePhysics(false);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	/** Attach to the AI Mesh*/
	RootComponent->AttachToComponent(_MeshToAttachTo, FAttachmentTransformRules::SnapToTargetNotIncludingScale, "WeaponSocket");
}

