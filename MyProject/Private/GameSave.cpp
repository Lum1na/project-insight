// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Inventory.h"
#include "Rifle.h"
#include "Shotgun.h"
#include "RocketLauncher.h"
#include "SMG.h"
#include "Armor_Chestpiece.h"
#include "Armor_EnergyShield.h"
#include "Armor_Feet.h"
#include "Armor_HeadPiece.h"
#include "Armor_Legpiece.h"
#include "GameSave.h"


UGameSave::UGameSave()
{
#pragma region ArmorTypes

	static ConstructorHelpers::FObjectFinder<UClass> ChestArmorBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Armor/BP_Armor_Chestpiece.BP_Armor_Chestpiece_C'"));
	if (ChestArmorBP.Object)
	{
		ChestArmorSpawn = ChestArmorBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> FeetArmorBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Armor/BP_Armor_Feet.BP_Armor_Feet_C'"));
	if (FeetArmorBP.Object)
	{
		FeetArmorSpawn = FeetArmorBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> HeadArmorBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Armor/BP_Armor_HeadPiece.BP_Armor_HeadPiece_C'"));
	if (HeadArmorBP.Object)
	{
		HeadArmorSpawn = HeadArmorBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> LegArmorBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Armor/BP_Armor_Legpiece.BP_Armor_Legpiece_C'"));
	if (LegArmorBP.Object)
	{
		LegArmorSpawn = LegArmorBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> EnergyShieldBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Armor/BP_Armor_EnergyShield.BP_Armor_EnergyShield_C'"));
	if (EnergyShieldBP.Object)
	{
		EnergyShieldSpawn = EnergyShieldBP.Object;
	}
#pragma endregion

#pragma region WeaponTypes
	static ConstructorHelpers::FObjectFinder<UClass> SmgBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Weapon/BP_SMG.BP_SMG_C'"));
	if (SmgBP.Object)
	{
		SmgSpawn = SmgBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> RifleBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Weapon/BP_Rifle.BP_Rifle_C'"));
	if (RifleBP.Object)
	{
		RifleSpawn = RifleBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> ShotgunBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Weapon/BP_Shotgun.BP_Shotgun_C'"));
	if (ShotgunBP.Object)
	{
		ShotgunSpawn = ShotgunBP.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> RLBP(TEXT("Blueprint'/Game/Blueprints/Item_PickUp/Weapon/BP_RocketLauncher.BP_RocketLauncher_C'"));
	if (RLBP.Object)
	{
		RocketLauncherSpawn = RLBP.Object;
	}
#pragma endregion
}

void UGameSave::Save(AFPS_Character* Player, AMyProjectGameModeBase* GameMode)
{
	if(GameMode)
	{
		MaxArenaRound = GameMode->GetMaxRoundUnlocked();
	}
	/** Saving EXP and Level*/
	UPlayerStats* PlayerStats = Player->GetPlayerStats();
	if (PlayerStats)
	{
		PlayerLevel = PlayerStats->GetPlayerLevel();
		CurrentExperience = PlayerStats->GetCurrentExperience();
	}

#pragma region Saving Inventories
	UInventory* inv = Player->GetPlayerInventory();
	if (inv)
	{
		/** Check to see if player has a current Weapon*/
		if (Player->GetCurrentWeapon())
		{
			bHasCurrentWeapon = true;
			CurrentWeapon = Player->GetCurrentWeapon()->WeaponConfig;
			bCurrentWeaponIsSMG = false;
			bCurrentWeaponIsRifle = false;
			bCurrentWeaponIsRocketLauncher = false;
			bCurrentWeaponIsShotgun = false;


			/** Casting to see what type of weapon the player has currently Equipped*/
			ARifle* Weapon_Rifle = Cast<ARifle>(Player->GetCurrentWeapon());
			if (Weapon_Rifle)
				bCurrentWeaponIsRifle = true;
			ASMG* Weapon_SMG = Cast<ASMG>(Player->GetCurrentWeapon());
			if (Weapon_SMG)
				bCurrentWeaponIsSMG = true;
			AShotgun* Weapon_Shotgun = Cast<AShotgun>(Player->GetCurrentWeapon());
			if (Weapon_Shotgun)
				bCurrentWeaponIsShotgun = true;
			ARocketLauncher* Weapon_RocketLauncher = Cast<ARocketLauncher>(Player->GetCurrentWeapon());
			if (Weapon_RocketLauncher)
				bCurrentWeaponIsRocketLauncher = true;
		}

		/** Current Equipped Armor Saving*/
		bHasHeadArmor = false;
		bHasChestArmor = false;
		bHasLegArmor = false;
		bHasFeetArmor = false;
		bHasShield = false;
		if (inv->EquippedArmor.EquippedHelmetArmor)
		{
			EquippedHeadArmor = inv->EquippedArmor.EquippedHelmetArmor->ArmorConfig;
			bHasHeadArmor = true;
		}
		if (inv->EquippedArmor.EquippedChestArmor)
		{
			EquippedChestArmor = inv->EquippedArmor.EquippedChestArmor->ArmorConfig;
			bHasChestArmor = true;
		}
		if (inv->EquippedArmor.EquippedLegArmor)
		{
			EquippedLegArmor = inv->EquippedArmor.EquippedLegArmor->ArmorConfig;
			bHasLegArmor = true;
		}
		if (inv->EquippedArmor.EquippedFeetArmor)
		{
			EquippedFeetArmor = inv->EquippedArmor.EquippedFeetArmor->ArmorConfig;
			bHasFeetArmor = true;
		}
		if (inv->EquippedArmor.EquippedEnergyShield)
		{
			EquippedShieldArmor = inv->EquippedArmor.EquippedEnergyShield->ArmorConfig;
			bHasShield = true;
		}

		/** Saving Weapons*/
		for (int i = 0; i < inv->Rifle_WeaponInventory.Num(); i++)
		{
			if (inv->Rifle_WeaponInventory[i])
				RifleInventory.Add(inv->Rifle_WeaponInventory[i]->WeaponConfig);
		}

		for (int i = 0; i < inv->SMG_WeaponInventory.Num(); i++)
		{
			if (inv->SMG_WeaponInventory[i])
				SMGInventory.Add(inv->SMG_WeaponInventory[i]->WeaponConfig);
		}

		for (int i = 0; i < inv->Shotgun_WeaponInventory.Num(); i++)
		{
			if (inv->Shotgun_WeaponInventory[i])
				ShotgunInventory.Add(inv->Shotgun_WeaponInventory[i]->WeaponConfig);
		}

		for (int i = 0; i < inv->RocketLauncher_WeaponInventory.Num(); i++)
		{
			if (inv->RocketLauncher_WeaponInventory[i])
				RocketLauncherInventory.Add(inv->RocketLauncher_WeaponInventory[i]->WeaponConfig);
		}

		/** Saving Armor*/
		for (int i = 0; i < inv->Head_ArmorInventory.Num(); i++)
		{
			if (inv->Head_ArmorInventory[i])
				HeadInventory.Add(inv->Head_ArmorInventory[i]->ArmorConfig);
		}

		for (int i = 0; i < inv->Chest_ArmorInventory.Num(); i++)
		{
			if (inv->Chest_ArmorInventory[i])
				ChestInventory.Add(inv->Chest_ArmorInventory[i]->ArmorConfig);
		}

		for (int i = 0; i < inv->Legs_ArmorInventory.Num(); i++)
		{
			if (inv->Legs_ArmorInventory[i])
				LegInventory.Add(inv->Legs_ArmorInventory[i]->ArmorConfig);
		}

		for (int i = 0; i < inv->Feet_ArmorInventory.Num(); i++)
		{
			if (inv->Feet_ArmorInventory[i])
				FeetInventory.Add(inv->Feet_ArmorInventory[i]->ArmorConfig);
		}

		for (int i = 0; i < inv->Energyshield_ArmorInventory.Num(); i++)
		{
			if (inv->Energyshield_ArmorInventory[i])
				ShieldInventory.Add(inv->Energyshield_ArmorInventory[i]->ArmorConfig);
		}

	}
#pragma endregion /** Saving Inventories*/

	UGameplayStatics::SaveGameToSlot(this, TEXT("MySlot"), 0);
}

void UGameSave::LoadGame(AFPS_Character* Player, AMyProjectGameModeBase* GameMode)
{  
	/** Loading Highest Arena round*/
	if(GameMode)
	{
		GameMode->SetMaxRoundUnlocked(MaxArenaRound);
	}
	/** Loading EXP and Level*/
	UPlayerStats* PStats = Player->GetPlayerStats();
	if(PStats)
	{
		PStats->SetPlayerLevel(PlayerLevel);
		PStats->SetCurrentExperience(CurrentExperience);
	}

#pragma region Loading Inventories
	/** Loading Weapons*/
	if (bHasCurrentWeapon)
	{
		if (bCurrentWeaponIsRifle)
			GameMode->SpawnWeaponFromSaveGame(RifleSpawn, CurrentWeapon, true);
		if (bCurrentWeaponIsSMG)
			GameMode->SpawnWeaponFromSaveGame(SmgSpawn, CurrentWeapon, true);
		if (bCurrentWeaponIsShotgun)
			GameMode->SpawnWeaponFromSaveGame(ShotgunSpawn, CurrentWeapon, true);
		if (bCurrentWeaponIsRocketLauncher)
			GameMode->SpawnWeaponFromSaveGame(RocketLauncherSpawn, CurrentWeapon, true);
	}

	if (bHasHeadArmor)
		GameMode->SpawnArmorFromSaveGame(HeadArmorSpawn, EquippedHeadArmor, true);
	if (bHasChestArmor)
		GameMode->SpawnArmorFromSaveGame(ChestArmorSpawn, EquippedChestArmor, true);
	if (bHasLegArmor)
		GameMode->SpawnArmorFromSaveGame(LegArmorSpawn, EquippedLegArmor, true);
	if (bHasFeetArmor)
		GameMode->SpawnArmorFromSaveGame(FeetArmorSpawn, EquippedFeetArmor, true);
	if (bHasShield)
		GameMode->SpawnArmorFromSaveGame(EnergyShieldSpawn, EquippedShieldArmor, true);


	for (int i = 0; i < RifleInventory.Num(); i++)
	{
		GameMode->SpawnWeaponFromSaveGame(RifleSpawn, RifleInventory[i]);
	}

	for (int i = 0; i < SMGInventory.Num(); i++)
	{
		GameMode->SpawnWeaponFromSaveGame(SmgSpawn, SMGInventory[i]);
	}

	for (int i = 0; i < ShotgunInventory.Num(); i++)
	{
		GameMode->SpawnWeaponFromSaveGame(ShotgunSpawn, ShotgunInventory[i]);
	}

	for (int i = 0; i < RocketLauncherInventory.Num(); i++)
	{
		GameMode->SpawnWeaponFromSaveGame(RocketLauncherSpawn, RocketLauncherInventory[i]);
	}

	/** Saving Armor*/
	for (int i = 0; i < HeadInventory.Num(); i++)
	{
		GameMode->SpawnArmorFromSaveGame(HeadArmorSpawn, HeadInventory[i]);
	}

	for (int i = 0; i < ChestInventory.Num(); i++)
	{
		GameMode->SpawnArmorFromSaveGame(ChestArmorSpawn, ChestInventory[i]);
	}

	for (int i = 0; i < LegInventory.Num(); i++)
	{
		GameMode->SpawnArmorFromSaveGame(LegArmorSpawn, LegInventory[i]);
	}

	for (int i = 0; i < FeetInventory.Num(); i++)
	{
		GameMode->SpawnArmorFromSaveGame(FeetArmorSpawn, FeetInventory[i]);
	}

	for (int i = 0; i < ShieldInventory.Num(); i++)
	{
		GameMode->SpawnArmorFromSaveGame(EnergyShieldSpawn, ShieldInventory[i]);
	}
#pragma endregion
}