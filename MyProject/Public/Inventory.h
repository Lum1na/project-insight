// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Weapon_BaseClass.h"
#include "Widget.h"
#include "Engine/DataTable.h"
#include "Inventory.generated.h"


USTRUCT(BlueprintType)
struct FEquippedArmor
{
	GENERATED_USTRUCT_BODY()
public:
	AArmor_Base* EquippedChestArmor;
	AArmor_Base* EquippedHelmetArmor;
	AArmor_Base* EquippedLegArmor;
	AArmor_Base* EquippedFeetArmor;
	AArmor_Base* EquippedEnergyShield;

	AArmor_Base* LastEquippedChestArmor;
	AArmor_Base* LastEquippedHelmetArmor;
	AArmor_Base* LastEquippedLegArmor;
	AArmor_Base* LastEquippedFeetArmor;
	AArmor_Base* LastEquippedEnergyShield;
};

UENUM(BlueprintType)
enum InventorySlot
{
	Rifle,
	SMG,
	Shotgun,
	RocketLauncher,
	Head,
	Chest,
	Legs,
	Feet,
	Shield
};

class AItem_Pickup;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MYPROJECT_API UInventory : public UActorComponent
{
	GENERATED_BODY()

#pragma region Defaults
public:
	UInventory();
protected:
	virtual void BeginPlay() override;
public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
#pragma endregion

public:
	bool GetIsWeaponInventoryFull(TEnumAsByte<InventorySlot> _Inventory);
	void AddWeaponToInventory(AItem_PickUp* _Weapon);
	void AddArmorToInventory(AItem_PickUp* _ArmorPiece);
	/**Removes a Weapon from the Inventory it belongs in*/
	void RemoveWeaponFromInventory(AWeapon_BaseClass* _Weapon);
	/** Removes an Armor from the Inventory it belongs in*/
	void RemoveArmorFromInventory(AArmor_Base* _Armor);
	AWeapon_BaseClass* GetWeapon(int32 InventorySlot);

	UPROPERTY(EditDefaultsOnly)
		int32 i_WeaponInventorySize;

#pragma region InventoryUI
	void ToggleInventoryShowing();
	UFUNCTION(BlueprintCallable, Category = "Inventory|Visibility")
		ESlateVisibility GetIsInventoryShowing();
#pragma endregion

	UFUNCTION(BlueprintCallable)
		void SetCurrentInteractable(AItem_PickUp* Item);
	UFUNCTION(BlueprintCallable)
		AItem_PickUp* GetCurrentInteractable();
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		AItem_PickUp* CurrentInteractable;

	void SetOwner(AFPS_Character* _Owner);
	void EquipArmor(AArmor_Base* _ArmorToEquip, bool _b_ReplaceCurrentlyEquipped);
	/** UnEquip Armor based on Slot
	 @params: 1= Head, 2 = Chest, 3 = Legs, 4 = Feet, 5 = EnergyShield*/
	void UnEquipArmor(int _i_Slot);

	/** Struct of the currently and last equipped armorpieces*/
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		FEquippedArmor EquippedArmor;

	UFUNCTION(BlueprintCallable)
		AArmor_Base* GetEquippedHeadPiece();
	UFUNCTION(BlueprintCallable)
		AArmor_Base* GetEquippedChestPiece();
	UFUNCTION(BlueprintCallable)
		AArmor_Base* GetEquippedLegPiece();
	UFUNCTION(BlueprintCallable)
		AArmor_Base* GetEquippedFeetPiece();
	UFUNCTION(BlueprintCallable)
		AArmor_Base* GetEquippedEnergyShield();
	bool b_InventoryShowing;


	void AddStats(float _f_Vitality = 0.0f, float _f_Accuracy = 0.0f, float _f_CritChance = 0.0f,
		float _f_CritDamage = 0.0f, float _f_Toughness = 0.0f, float _f_Armor = 0.0f, float _f_LifeLeach = 0.0f);

	/** Different Arrays for the seperate Weapon types*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class ASMG*> SMG_WeaponInventory;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class ARifle*> Rifle_WeaponInventory;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AShotgun*> Shotgun_WeaponInventory;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class ARocketLauncher*> RocketLauncher_WeaponInventory;

	/** Different Arrays for the seperate Armor types*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AArmor_Chestpiece*> Chest_ArmorInventory;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AArmor_HeadPiece*> Head_ArmorInventory;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AArmor_Legpiece*> Legs_ArmorInventory;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AArmor_Feet*> Feet_ArmorInventory;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AArmor_EnergyShield*> Energyshield_ArmorInventory;
private:
	class AFPS_Character* Owner;
	class UPlayerStats* PlayerStats;
};
