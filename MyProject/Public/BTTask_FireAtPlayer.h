// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_FireAtPlayer.generated.h"

/**
 *
 */
UCLASS()
class MYPROJECT_API UBTTask_FireAtPlayer : public UBTTaskNode
{
	GENERATED_BODY()

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:

	void ShootAtPlayer(UBehaviorTreeComponent& OwnerComp, ABase_EnemyCharacter* _AIC, AFPS_Character* _PlayerCharacter);
	void ResetShotDelayTimer();
	void Reload();
	void FinishReload();
	UAudioComponent* PlayWeaponSound(USoundCue * Sound, APawn* Parent);
	bool b_IsWeaponCooldownRunning;
	bool b_CanFireWeapon = true;

	FTimerHandle ShotDelayTimerHandle;
	FTimerHandle ReloadTimerHandle;
};
