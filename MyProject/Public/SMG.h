// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Weapon_BaseClass.h"
#include "SMG.generated.h"

UCLASS()
class MYPROJECT_API ASMG : public AWeapon_BaseClass
{
	GENERATED_BODY()

		ASMG();

	virtual void GenerateStats(float _f_WeaponStatsModifier) override;	

protected:
	UPlayerStats* PlayerStats;
};
