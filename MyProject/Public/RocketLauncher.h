// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Weapon_BaseClass.h"
#include "RocketLauncher.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API ARocketLauncher : public AWeapon_BaseClass
{
	GENERATED_BODY()
		ARocketLauncher();

	UFUNCTION()
	virtual void ProjectileFire() override;
	virtual void GenerateStats(float _f_WeaponStatsModifier) override;

protected:
	UPlayerStats* PlayerStats;
};
