// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Weapon_BaseClass.h"
#include "Rifle.generated.h"

/**
 *
 */
UCLASS()
class MYPROJECT_API ARifle : public AWeapon_BaseClass
{
	GENERATED_BODY()
		ARifle();
public:

	virtual void GenerateStats(float _f_WeaponStatsModifier) override;

protected:
	UPlayerStats* PlayerStats;

};
