// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "FPS_PlayerController.generated.h"

/**
 *
 */

class AFPS_Character;

enum EHudElements
{
	Characterstats,
	Inventory,
	AfterRoundWindow,
	Nothing
};

UCLASS()
class MYPROJECT_API AFPS_PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AFPS_PlayerController();


	/** Sets the last opened HUD Element so they can be Closed in Order*/
	void AddLastOpenedHudElement(EHudElements _LastOpenedHudElement);
	/** Closes the last Opened Hud Element*/
	void CloseLastHudElement();

	/** Sets the Controlled Pawn*/
	void SetControlledPawn(AFPS_Character* _ControlledPawn);

	/** Removes an Element if it has been toggled off without hitting the ESCAPE key*/
	void RemoveHudElementFromList(EHudElements _HudElementToRemove);

	/** Returns the Last Opened Element from the Array*/
	EHudElements GetLastOpenedHudElement();
private:


	/** Array that holds all opened HUD Elements to be closed in Order*/
	TArray<TEnumAsByte<EHudElements>> LastOpenedHudElementsArray;

	/** Pawn that is being Controlled by this Controller*/
	AFPS_Character* ControlledPawn;
};
