// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Item_PickUp.generated.h"

UCLASS()
class MYPROJECT_API AItem_PickUp : public AActor
{
	GENERATED_BODY()

public:
#pragma region Defaults
	AItem_PickUp();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
#pragma endregion
	virtual void ProcessItemPickUp();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		FString Name;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item|Collision")
		UBoxComponent* CollisionComp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		USkeletalMeshComponent* Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		UTexture2D* Icon;


	UFUNCTION(BlueprintCallable)
		FString GetUseText() const { return FString::Printf(TEXT("%s : Press 'F' to Pickup!"), *Name); }
};
