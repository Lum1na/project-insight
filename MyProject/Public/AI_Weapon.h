// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "AI_Weapon.generated.h"


USTRUCT(BlueprintType)
struct FWeaponConfig
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly)
		float f_Accuarcy;
	UPROPERTY(EditDefaultsOnly)
		float f_Damage;
	UPROPERTY(EditDefaultsOnly)
		float f_ShotDelay;
	UPROPERTY(EditDefaultsOnly)
		int32 i_AmmoInClip;
	int32 i_CurrentAmmoInClip;
	UPROPERTY(EditDefaultsOnly)
		float f_Reloadtime;	 
};

UCLASS()
class MYPROJECT_API AAI_Weapon : public AActor
{
	GENERATED_BODY()
#pragma region Defaults	
public:
	AAI_Weapon();
protected:
	virtual void BeginPlay() override;
#pragma endregion

public:
	/** Equips the Weapon and attaches it to the AI Mesh*/
	void EquipWeapon(USkeletalMeshComponent* _MeshToAttachTo);

	/** Firing sound that plays when ai shoots*/
	UPROPERTY(EditDefaultsOnly)
		USoundCue* FireSound;

	UPROPERTY(EditDefaultsOnly)
		UBoxComponent* CollisionComponent;
	UPROPERTY(EditDefaultsOnly)
		USkeletalMeshComponent* WeaponMesh;
	/** Struct for the weapon data*/
	UPROPERTY(EditDefaultsOnly)
		FWeaponConfig WeaponConfig;
};
