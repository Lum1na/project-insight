// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Item_PickUp.h"
#include "Armor_Base.generated.h"

/**
 *
 */
class AFPS_Character;
class UPlayerStats;

USTRUCT(BlueprintType)
struct FArmorData
{
	GENERATED_USTRUCT_BODY()

public:
	/** Gives the Player an extra Healthbar that depletes first and regenerates over time*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor Stats")
		float f_Armor;
	/** Gives the PLayer extra life 1 to 1*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor Stats")
		float f_Vitality;
	/** Reduces the Damage the Player takes*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor Stats")
		float f_Toughness;
	/** Increases the Weaponaccuracy| 100% Accuracy is MAX*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor Stats")
		float f_Accuracy;
	/**Gives the Player a Chance to crit and do CritDamage as extra damage on top of normal weapondamage*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor Stats")
		float f_CritChance;
	/** % Extra Damage the Player does when getting a Critical Hit  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor Stats")
		float f_CritDamage;
	/** % of the Damage dealt is returned as Health*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor Stats")
		float f_LifeLeach;
	/** Item's Slot in the Inventory*/
	int32 i_InventorySlot;
};

UCLASS()

class MYPROJECT_API AArmor_Base : public AItem_PickUp
{
	GENERATED_BODY()
public:
	AArmor_Base();
	virtual void Tick(float DeltaSeconds) override;

	/** Amount of Stats the Armor piece can have. Example: 3 Stats randomly generates Armor, Crit Chance and Vitality */
	UPROPERTY(EditDefaultsOnly, Category = Config)
		int32 i_MaxAmountOfStats;
	/** struct that holds all Available Stats*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
		FArmorData ArmorConfig;
	/** Base value the Armor Stats gets Generated with when Armorpiece is dropped*/
	UPROPERTY(EditDefaultsOnly, Category = Config)
		float f_ArmorBaseStat;
	UPROPERTY(EditDefaultsOnly, Category = Config)
		float f_VitalityBaseStat;
	UPROPERTY(EditDefaultsOnly, Category = Config)
		float f_ToughnessBaseStat;
	UPROPERTY(EditDefaultsOnly, Category = Config)
		float f_CritDamageBaseStat;
	UPROPERTY(EditDefaultsOnly, Category = Config)
		float f_ArenaRoundModifier;
	UPROPERTY(EditDefaultsOnly, Category = Config)
		float f_ArmorMultiplier;
	UPROPERTY(EditDefaultsOnly, Category = Config)
		float f_MaxStatMultiplier;

#pragma region LootNotification
	/** Particle system that indicates a dropped item*/
	UPROPERTY(EditAnywhere, Category = "Weapon|Loot Notification")
		UParticleSystemComponent* LootPSC;
	/** bool that determines if the Particlesystem is enabled/disabled*/
	bool bShowLootPSC; 
#pragma endregion

	/** whether or not the armor piece is currently equipped or not*/
	bool b_EquipedArmor;
	/** whether or not the stats of the armor piece have been added to the playerstats*/
	bool b_StatsAdded;
	/** */
	int i_ArmorSlot;

	/** Function that generates the stats of the armor piece when an Enemy drops it*/
	virtual void GenerateStats();
	/** Copy the stats of an armor to another armor (used when items are spawned from the save game)*/
	void CopyStats(FArmorData _StatsToCopy);
	/** Function to equip the item*/
	void Equip();
	/** Function to drop the item*/
	void DropItem();
	/** Sets the Player reference*/
	void SetOwningPlayer(AFPS_Character* _OwningPlayer);

private:

	AFPS_Character* OwningPlayer;
	UPlayerStats* PlayerStats;
};
