// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Public/Inventory.h"
#include "Widget.h"
#include "Weapon_BaseClass.h"
#include "PlayerStats.h"
#include "FPS_Character.generated.h"

class AFPS_PlayerController;


UCLASS()
class MYPROJECT_API AFPS_Character : public ACharacter
{
	GENERATED_BODY()

public:
	void SaveGame();
	void LoadGame();
#pragma region Default Stuff
	AFPS_Character();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
#pragma endregion

#pragma region Window Toggle
public:
	UFUNCTION(BlueprintCallable, Category = "Character|HUD Toggle")
		ESlateVisibility GetIsHelpWindowShowing();
	UPROPERTY(EditAnywhere)
		bool bIsHelpWindowShowing;
	UFUNCTION(BlueprintCallable)
		ESlateVisibility IsRoundSlectionShowing();
	void ToggleRoundSelection();
	void CloseLastHUDElement();

#pragma endregion

#pragma region Cursor Options
public:
	/** Shows the Cursor on screen.*/
	UFUNCTION(BlueprintCallable)
		void ShowCursor();
	/** Hides the Cursor.*/
	UFUNCTION(BlueprintCallable)
		void HideCursor();
#pragma endregion

#pragma region Damage & Respawn Functions
public:
	void Take_Damage(float _f_DamageAmount, FVector HitLocation = FVector::ZeroVector);
	void Death();
	void PlayParticleEffect(UParticleSystem* _PS, FVector _HitLocation);
	UFUNCTION(BlueprintCallable, Category = "Character|TakeDamage")
		bool GetIsDead();
	UFUNCTION(BlueprintCallable, Category = "Character|TakeDamage")
		void Respawn(FVector _SpawnPosition, FRotator _SpawnRotation);
	UFUNCTION(BlueprintCallable, Category = "Character|TakeDamage")
		ESlateVisibility GetHitIndicatorVisibility();

	/** Particle System used when the Player has no Armor*/
	UPROPERTY(EditDefaultsOnly, Category = "Character|TakeDamage")
		UParticleSystem* HitParticeSystem;
	/** Particle System used when the Player has Armor*/
	UPROPERTY(EditDefaultsOnly, Category = "Character|TakeDamage")
		UParticleSystem* ArmorHitParticeSystem;

	bool b_IsDead;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character|TakeDamage")
		float f_RespawnTime;	
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character|TakeDamage")
		FVector OffsetCameraPosition;
private:
	void Respawn();
	void DisableHitIndicator();
	void OffsetDeathCamera(bool _offset);

	FVector DefaultCameraPosition;


	UParticleSystemComponent* HitPSC;
	FTimerHandle RespawnTimerHandle;
	FTimerHandle HitIndicatorResetHandle;
	bool b_ShowHitIndicator;
	FVector SpawnPoint;
	FRotator SpawnRotation;
#pragma endregion

#pragma region Weapon Functions & Variables
public:
	/*Functions*/
	void FireWeapon();
	void StopFire();
	void ReloadWeapon();

	/** Shows the Hitmarker on the UI and Plays a Selected Hit Sound*/
	void PlayHitNotification();
	UFUNCTION(BlueprintCallable, Category = "Character|HitMarker")
	ESlateVisibility GetHitMarkerVisibility();
	UFUNCTION()
	void SetHitMarkerVisbility(bool _b_Visibility);
	bool b_HitmarkerShowing;
	/** How long the Hitmarker will stay visible*/
	UPROPERTY(EditDefaultsOnly, Category = "Character|HitMarker")
		float fShowHitmarkerTime;
	UPROPERTY(EditDefaultsOnly, Category = "Character|HitMarker")
		USoundCue* EnemyHitSound;
	FTimerHandle HitMarkerTimerHandle;

	void ProcessWeaponPickUp(AWeapon_BaseClass* weapon);
	void SelectWeapon(AWeapon_BaseClass* weapon);
	/** Equips an item from the Inventory*/
	UFUNCTION(BlueprintCallable, Category = "Character|Item")
		void EquipItemFromInventory(AItem_PickUp* _ItemToEquip);
	/** Deletes an Item from the Inventory*/
	UFUNCTION(BlueprintCallable, Category = "Character|Item")
		void DeleteItem(AItem_PickUp* _ItemToDestroy); 
	 /** Drops an Item from the Inventory*/
	UFUNCTION(BlueprintCallable, Category = "Character|Item")
		void DropInventoryItem(AItem_PickUp* _ItemToDrop);	 

	/** Returns the Currently Equipped Weapon*/
	UFUNCTION(BlueprintCallable, Category = "Character|Weapon")
		AWeapon_BaseClass* GetCurrentWeapon();

	/** A Default Weapon the Player Spawns with when the bool below is true*/
	UPROPERTY(EditAnywhere, Category = "Character|Weapon")
		TSubclassOf<AWeapon_BaseClass> DefaultWeaponToSpawnWith;

	/** True = the Player Spawnes with a above selected Default Weapon*/
	UPROPERTY(EditAnywhere, Category = "Character|Weapon")
		bool bEquipDefaultWeapon;

	/** The Camera shake that should be used when firing the Weapon*/
 	UPROPERTY(EditAnywhere, Category = "Character|Camera Shake")
		TSubclassOf<UCameraShake> CameraShakeToUse;

	void ExecuteCameraShake();


	/** the currently equipped weapon*/
	AWeapon_BaseClass* CurrentWeapon;
	FTimerHandle FireTimerHandle;
#pragma endregion

#pragma region Tutorial stuff
private:
	/** True if Gamemode is Tutorial Gamemode*/
	bool Tutorial;
	/** A reference to the Tutorial Gamemode*/
	class ATutorialGameMode* TutorialGameMode;
#pragma endregion

#pragma region Movement Functions & Variables
public:
	UPROPERTY(EditAnywhere, Category = "Character|Movement")
		float f_SprintMultiplier;
	UPROPERTY(EditAnywhere, Category = "Character|Movement")
		float f_Sensitivity;

#pragma endregion

#pragma region Accessor Functions
public:
	UFUNCTION(BlueprintCallable, Category = "Character|Accesor Function")
		bool GetIsSprinting();
	UFUNCTION(BlueprintCallable, Category = "Character|Accesor Function")
		UPlayerStats* GetPlayerStats();
	UFUNCTION(BlueprintCallable, Category = "Character|Accesor Function")
		UInventory* GetPlayerInventory();
	UFUNCTION(BlueprintCallable, Category = "Character|Accesor Function")
		FString GetCurrentClipAmmo();
#pragma endregion


#pragma region InformationPanel
public:
	UFUNCTION(BlueprintCallable)
		void SetInformationText(FString _InformationText, float _f_DelayToHideText = 5.0f, bool _b_ResetDisplay = true);
	UFUNCTION(BlueprintCallable)
		FString GetInformationText() const;

	void ResetInformationPanel();
	FTimerHandle InformationResetTimerHandle;
private:
	FString InformationPanelText;
public:
#pragma endregion

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character")
		int32 ItemPickUpRange;

#pragma region Components
public:
	UPROPERTY(EditAnywhere, Category = "Character|Components")
		UBoxComponent* CollisionComp;

	UPROPERTY(EditDefaultsOnly, Category = "Character|Components")
		USkeletalMeshComponent* FirstPersonMesh;

	UPROPERTY(EditAnywhere, Category = "Character|Components")
		USkeletalMeshComponent* ThirdPersonMesh;

	UPROPERTY(EditAnywhere, Category = "Character|Components")
		UCameraComponent* Camera;

	UPROPERTY(EditAnywhere, Category = "Character|Components")
		UInventory* Inventory;

	UPROPERTY(EditAnywhere, Category = "Character|Components")
		UPlayerStats* PlayerStats;

	AFPS_PlayerController* Player_Controller;  
#pragma endregion

protected:
	void ToggleHelpWindow();

	void OnStartAiming();
	void OnStopAiming();

	void PickUpItem();
	void CheckForItem();  

	/** Movementfunctions*/
	virtual void MoveForward(float value);
	virtual void MoveRight(float value);
	virtual void Turn(float value);
	virtual void LookUp(float value);
	void OnStartSprint();
	void OnStopSprint();
	void OnStartJump();
	void OnStopJump();
	void SetSprinting();

	bool b_IsSprinting;
	bool b_IsFiring;
	bool b_CursorActive;

	class AMyProjectGameModeBase* GameMode;
};