// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Item_PickUp.h"
#include "HealthDrop.generated.h"

/**
 *
 */
UCLASS()
class MYPROJECT_API AHealthDrop : public AItem_PickUp
{
	GENERATED_BODY()

public:
	AHealthDrop();

	UPROPERTY(EditAnywhere)
		USphereComponent* PickupRadius;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* PickupSound;
	UFUNCTION()
		void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
