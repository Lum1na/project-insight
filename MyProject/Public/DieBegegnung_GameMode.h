// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "Base_EnemyCharacter.h"
#include "Waypoint_Manager.h"
#include "DieBegegnung_GameMode.generated.h"

/**
 *
 */
UCLASS()
class MYPROJECT_API ADieBegegnung_GameMode : public AGameModeBase
{
	GENERATED_BODY()
public:
		///////////Desired Player HUD to be spawned///////////
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HUD", Meta = (BlueprintsProtected = "true"))
		TSubclassOf<class UUserWidget> PlayerHUDClass;
	UPROPERTY(BlueprintReadOnly)
		UUserWidget* CurrentWidget;
public:
	ADieBegegnung_GameMode();
	virtual void BeginPlay() override;

	/** The Enemy that will be Spawned*/
	UPROPERTY(EditAnyWhere, Category = "EnemyWave")
		TSubclassOf<ABase_EnemyCharacter> EnemyToSpawn;

	/** The amount of Enemies spawned per wave*/
	UPROPERTY(EditAnyWhere, Category = "EnemyWave")
		float EnemiesPerWave;

	/** The Time between each enemy during a WaveSpawn*/
	UPROPERTY(EditAnyWhere, Category = "EnemyWave")
		float TimeBetweenEnemys;

	/** The Time between waves*/
	UPROPERTY(EditAnyWhere, Category = "EnemyWave")
		float TimeBetweenWaves;
	UFUNCTION(BlueprintCallable)
		void StartWaveSpawn();
	void SpawnEnemyWave();
	void SpawnEnemy();

private:
	AWaypoint_Manager* WPManager;
	int EnemiesSpawnedThisWave;
	FTimerHandle WaveTimer;
	FTimerHandle EnemyTimer;
};
