// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Item_PickUp.h"
#include "AmmoDrop.generated.h"

/**
 *
 */
UCLASS()
class MYPROJECT_API AAmmoDrop : public AItem_PickUp
{
	GENERATED_BODY()

public:
	AAmmoDrop();
	/** Amount of ammo the player receives when he picks up the item*/
	UPROPERTY(EditAnywhere)
		int i_AmountOfAmmo;

	/** Collision Sphere that indicates the pickup range*/
	UPROPERTY(EditAnywhere)
		USphereComponent* PickupRadius;

	/** Sound that plays when item is picked up*/
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* PickupSound;

	/** Checking overlap*/
	UFUNCTION()
		void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
