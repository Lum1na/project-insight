// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Waypoint.generated.h"

UCLASS()
class MYPROJECT_API AWaypoint : public AActor
{
	GENERATED_BODY()
	
public:	
#pragma region Defaults
	AWaypoint();
	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;
#pragma endregion	
};
