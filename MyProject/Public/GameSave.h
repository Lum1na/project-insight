// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SaveGame.h"
#include "FPS_Character.h"
#include "MyProjectGameModeBase.h"
#include "GameSave.generated.h"

/**
 *
 */
UCLASS()
class MYPROJECT_API UGameSave : public USaveGame
{
	GENERATED_BODY()
public:
	UGameSave();

	void Save(AFPS_Character* Player, AMyProjectGameModeBase* GameMode);
	void LoadGame(AFPS_Character* Player, AMyProjectGameModeBase* GameMode);

private:
	UPROPERTY()
		int MaxArenaRound;
	UPROPERTY()
		int PlayerLevel;
	UPROPERTY()
		int CurrentExperience;

	UPROPERTY()
		FWeaponData CurrentWeapon;
	UPROPERTY()
		bool bHasCurrentWeapon;
	/** To Determine what Weapontype the currentWeapon is*/
	UPROPERTY()
		bool bCurrentWeaponIsRifle;
	UPROPERTY()
		bool bCurrentWeaponIsSMG;
	UPROPERTY()
		bool bCurrentWeaponIsShotgun;
	UPROPERTY()
		bool bCurrentWeaponIsRocketLauncher;
	/**	//////////////////////////////////////////////// */


	UPROPERTY()
		FArmorData EquippedHeadArmor;
	UPROPERTY()
		FArmorData EquippedChestArmor;
	UPROPERTY()
		FArmorData EquippedLegArmor;
	UPROPERTY()
		FArmorData EquippedFeetArmor;
	UPROPERTY()
		FArmorData EquippedShieldArmor;

	UPROPERTY()
		bool bHasHeadArmor;
	UPROPERTY()
		bool bHasChestArmor;
	UPROPERTY()
		bool bHasLegArmor;
	UPROPERTY()
		bool bHasFeetArmor;
	UPROPERTY()
		bool bHasShield;



	UPROPERTY()
		TArray<FWeaponData> RifleInventory;
	UPROPERTY()
		TArray<FWeaponData> SMGInventory;
	UPROPERTY()
		TArray<FWeaponData> ShotgunInventory;
	UPROPERTY()
		TArray<FWeaponData> RocketLauncherInventory;

	UPROPERTY()
		TArray<FArmorData> HeadInventory;
	UPROPERTY()
		TArray<FArmorData> ChestInventory;
	UPROPERTY()
		TArray<FArmorData> LegInventory;
	UPROPERTY()
		TArray<FArmorData> FeetInventory;
	UPROPERTY()
		TArray<FArmorData> ShieldInventory;


	///// Holding References to Blueprints of the items /////
	TSubclassOf<AWeapon_BaseClass> SmgSpawn;
	TSubclassOf<AWeapon_BaseClass> RifleSpawn;
	TSubclassOf<AWeapon_BaseClass> ShotgunSpawn;
	TSubclassOf<AWeapon_BaseClass> RocketLauncherSpawn;

	TSubclassOf<AArmor_Base> HeadArmorSpawn;
	TSubclassOf<AArmor_Base> ChestArmorSpawn;
	TSubclassOf<AArmor_Base> LegArmorSpawn;
	TSubclassOf<AArmor_Base> FeetArmorSpawn;
	TSubclassOf<AArmor_Base> EnergyShieldSpawn;
};
