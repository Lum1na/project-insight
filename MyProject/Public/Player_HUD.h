// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "Player_HUD.generated.h"

/**
 *
 */

struct  Icon
{
	FString name;
	UTexture2D* tex;
	Icon() { name = "UNKNOWN ICON"; tex = 0; }
	Icon(FString& iName, UTexture2D* iTex)
	{
		name = iName;
		tex = iTex;
	}
};

struct Widget
{
	Icon icon;
	FVector2D pos, size;
	Widget(Icon iicon)
	{
		icon = iicon;
	}
	float left() { return pos.X; }
	float right() { return pos.X + size.X; }
	float top() { return pos.Y; }
	float bottom() { return pos.Y + size.Y; }
};



UCLASS()
class MYPROJECT_API APlayer_HUD : public AHUD
{
	GENERATED_BODY()

	void DrawWidget();
	void DrawHud();
	void ToggleInventory();
};
