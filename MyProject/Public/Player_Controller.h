// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "Player_Controller.generated.h"

class AItem_Pickup;
/**
 * 
 */
UCLASS()
class MYPROJECT_API APlayer_Controller : public APlayerController
{
	GENERATED_BODY()	

public:
	/**Interactable the player is Currently looking at */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	AItem_Pickup* CurrentInteractable;
};
