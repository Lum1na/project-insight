// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h" 
#include "Public/WeaponSpawner.h"
#include "CombatTextActor.h"
#include "Base_EnemyCharacter.generated.h"

UCLASS()
class MYPROJECT_API ABase_EnemyCharacter : public ACharacter
{
	GENERATED_BODY()

#pragma region Defaults
public:
	ABase_EnemyCharacter();
protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
#pragma endregion

public:
	UFUNCTION(BlueprintCallable, Category = "EnemyCharacter|Health")
		float GetHealthPercentage();
	UPROPERTY(EditAnywhere, Category = "AI|Behavior")
		class UBehaviorTree* BehaviorTree; 
	UPROPERTY(transient)
		class UBlackboardComponent* BlackboardComponent;
	UPROPERTY(transient)
		class UBehaviorTreeComponent* BehaviorComponent;

	/** The Weapon the AI Spawns with when the bool below is true*/
	UPROPERTY(EditAnywhere, Category = "AI")
		TSubclassOf<class AAI_Weapon> WeaponToSpawnWith;
	class AAI_Weapon*  AI_Weapon;

	void SetController(class AAI_BaseEnemyController* _Controller);

	float Damage(float Amount, AFPS_Character* _Player, FVector HitLocation = FVector::ZeroVector);
	void Respawn();
	void SetArenaGameMode(class AMyProjectGameModeBase* _GameMode);
	void SetStats(int _i_RoundToSyncTo);
	/** Sets the State the AI should be in during a Round.
	 * Active = Its an active ai during the round and not placed outside the Map Behavior Tree will be activated
	 * Inactive = Its not active during the round and is placed outside the Map. Behavior Tree will be deactivated
	 */
	void SetActiveState(bool _activeState);
	FHitResult FireRaycast(FVector _Offset = FVector::ZeroVector);

	bool b_IsDead;
	UFUNCTION(BlueprintCallable)
	bool GetIsDead();

	bool b_PlayerInSight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float f_MaxStatModifier;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float f_MaxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		int32 i_XPonKill;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		int32 i_Toughness;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		int32 i_RespawnTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		UWeaponSpawner* WeaponSpawner;

	/** Particle System used when the Player has no Armor*/
	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* HitParticeSystem;

	class AFPS_Character* PlayerCharacter;
	AAI_BaseEnemyController* AIPC;
private:
	void RotateToPlayer();
	void CheckLineOfSight();   

	void Death(AFPS_Character* _Player);
	void DisplayCombatText(float _DamageDone, AFPS_Character* _Player);

	UParticleSystemComponent* HitPSC;
	AMyProjectGameModeBase* ArenaGameMode;
	ACombatTextActor* CombatText;
	FVector MeshPosition;
	FTimerHandle RespawnHandle;
	float f_CurrentHealth;
	float f_FinalDamageTaken;
};
