// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "SpawnPoint.generated.h"

UCLASS()
class MYPROJECT_API ASpawnPoint : public AActor
{
	GENERATED_BODY()
#pragma region Defaults	
public:	
	ASpawnPoint();
protected:
	virtual void BeginPlay() override;
#pragma endregion
};
