// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Widget.h"
#include "Public/Armor_Base.h"
#include "PlayerStats.generated.h"

class AFPS_Character;


USTRUCT(BlueprintType)
struct FPlayerStatsConfig
{
	GENERATED_USTRUCT_BODY()

public:
	/** Amount of Health the Player currently has.*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats|PlayerConfig")
		float f_CurrentHealth;
	/** Amount of MaxHealth the Player can have.*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats|PlayerConfig")
		float f_MaxHealth;
	/** Amount of maximum armor the Player can have.
		Adds another healthbar that depletes before the Players life*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats|PlayerConfig")
		float f_MaxArmor;
	/** Amount of armor the Player currently has*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats|PlayerConfig")
		float f_CurrentArmor;
	/** Amount of vitality the Player currently has.
		Adds extra health to the overall Player health ( 1 to 1)*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats|PlayerConfig")
		float f_Vitality;
	/** Amount of toughness the Player currently has.
		Reduced the Damage taken from enemies*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats|PlayerConfig")
		float f_Toughness;
	/** Amount of Accuracy the Player currently has.
		Reduces amount of Spread on the Weapon the higher the value is*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats|PlayerConfig")
		float f_Accuracy;
	/** Amount of Critical hitchance the Player currently has.
		Allowes the Player to Criticaly hit and do aditional Damage*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats|PlayerConfig")
		float f_CritChance;
	/** Amount of Criticalhitdamage the Player currently has.
		%Amount of Damage the Player does aditionally when criticaly hitting an Enemy*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats|PlayerConfig")
		float f_CritDamage;
	/** Amount of Lifeleach the Player Currently has.
		%of the Damage done by the Player is returned as Health*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats|PlayerConfig")
		float f_Lifeleach;

	/** currentexperience of the Player in that level*/
	int32 i_CurrentExperience;
	/** Experience needed to levelup*/
	int32 i_ExperienceToNextLevel;
	/** The Players Level*/
	int32 i_PlayerLevel;
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MYPROJECT_API UPlayerStats : public UActorComponent
{
	GENERATED_BODY()

public:
	UPlayerStats();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditDefaultsOnly, Category = "PlayerStats")
		int32	i_MaxPlayerLevel;
	bool b_MaxLevelReached;
#pragma region Accessor Functions

	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetExperiencePercentage();
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetArmorPercentage();
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetHealthPercentage();


	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		int32 GetCurrentExperience();

	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		int32 GetExperienceToNextLevel();
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		int32 GetPlayerLevel();

	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetCurrentArmor();
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetMaxArmor();
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetVitality();
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetAccuracy();
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetCritChance();
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetCritDamage();
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetLifeLeach();
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetToughness();
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetCurrentHealth();
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		float GetMaxHealth();


	void SetCurrentHealth(float NewCurrentHealthAmount);
	void SetMaxHealth(float NewMaxHealtAmount);
	void SetMaxArmor(float NewArmorAmount);
	void SetCurrentArmor(float NewArmorAmount);
	void SetVitality(float NewVitalityAmount);
	void SetToughness(float NewToughnessAmount);
	void SetAccuracy(float NewAccuracyAmount);
	void SetCritChance(float NewCritChanceAmount);
	void SetCritDamage(float NewCritDamageAmount);
	void SetLifeLeach(float NewLifeLeachAmount);

	void SetPlayerLevel(float NewPlayerLevel);
	void SetCurrentExperience(int NewCurrentExperience);
	void SetExperienceNeededForNextLevel(int NewExperienceToNextLevel);



#pragma endregion
	void SetOwner(AFPS_Character* _Owner);

	void ChangeCurrentHealth(float f_AmountToAdd);
	void ReduceArmor(float _f_AmountToReduceBy);
	/** adds Experience to the Currentexperience and calls levelup function when ExpToNextLevel is Reached.
	Also saves overflow Experience and adds it to the new level Experienceamount*/
	void GainExperience(int32 AmountOfExperience);
	/** Adds a Level to the overall Playerlevel and gives the Player more base Vitality*/
	void LevelUp();
	/** a reference to the struct holding all the Playerstats variables*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats")
		FPlayerStatsConfig PlayerStatsConfig;

	/**Toggles a bool and an ESlateVisibility Variable to Toggle the Characterstats on the UI*/
	void ToggleCharacterStatsShowing();
	/** Returns if the Characterstats should be Shown or not*/
	UFUNCTION(BlueprintCallable, Category = "PlayerStats|Panel")
		ESlateVisibility GetCharacterStatsShowing();

	/** Time it takes before Armor starts regenerating after getting hit*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats|Armor Recharge")
		float f_ArmorRechargeDelayTime;
	/** Amount of time it takes for 1 Armor to regenerate*/
	UPROPERTY(EditAnywhere, Category = "PlayerStats|Armor Recharge")
		float f_ArmorRechargeRate;
	void ReChargeArmor();
private:
	/** the bool and ESlatvisibility that determin if the Characterstats get schown or not*/
	bool bIsCharacterStatsShowing;

	FTimerHandle ArmorRechargeTimerHandle;
	/** A reference to the PlayerCharacter*/
	AFPS_Character* Owner;
	/** A reference to the Player Inventory*/
	class UInventory* Inventory;

public:
#pragma region Ammo Types
	/** Sets the current ammo used by the Players currently equipped Weapon*/
	void SetCurrentAmmoInUse();
	/** Returns the Current Reserve Ammo used by the Players currently equipped Weapon to Show on the UI*/
	UFUNCTION(BlueprintCallable, Category = "PlayerStats")
		int32 GetReserveAmmo();

	/** returns a Pointer to the Ammo currently used by the Players currently equipped Weapon*/
	int* GetCurrentAmmoInUse();

private:
	/** the Ammo currently used by the Players currently equipped Weapon to Return to the UI*/
	int ammoInUse;
	/** Default Reserverammo count for all Weapons*/
	int WeaponDefaultAmmoCount;

	/** a Pointer to the Ammo currently used by the Players currently equipped Weapon*/
	int* CurrentAmmoInUse;

	/** the different Ammotypes*/
	int SMGAmmo;
	int RifleAmmo;
	int RocketLauncherAmmo;
	int ShotgunAmmo;
#pragma endregion
};
