// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "AI_BaseEnemyController.generated.h"

/**
 *
 */
UCLASS()
class MYPROJECT_API AAI_BaseEnemyController : public AAIController
{
	GENERATED_BODY()
public:
	virtual void BeginPlay() override;

	UPROPERTY(transient)
		class UBlackboardComponent* BlackboardComponent;
	UPROPERTY(transient)
		class UBehaviorTreeComponent* BehaviorComponent;

	AAI_BaseEnemyController();
	virtual void Possess(APawn* InPawn) override;
	void StartBehavior(bool b_StartBehavior);

	uint8 EnemyKeyID;

	class ABase_EnemyCharacter* AICharacter;
	class AFPS_Character* PlayerCharacter;
};
