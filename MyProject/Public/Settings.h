// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SaveGame.h"
#include "Settings.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API USettings : public USaveGame
{
	GENERATED_BODY()
	
	
public:
	UPROPERTY(BlueprintReadWrite, Category = "Settings")
		float f_MouseSensetivity;
	UPROPERTY(BlueprintReadWrite, Category = "Settings")
		bool b_InvertMouse;
	UPROPERTY(BlueprintReadWrite, Category = "Settings")
		float f_FieldOfView;
	UPROPERTY(BlueprintReadWrite, Category = "Settings")
		bool b_LoadGame;
	
};
