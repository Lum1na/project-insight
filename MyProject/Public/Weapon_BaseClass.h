// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Public/Item_PickUp.h"
#include "Rocket.h"
#include "Weapon_BaseClass.generated.h"

class AFPS_Character;
class UPlayerStats;

#define TRACE_WEAPON ECC_GameTraceChannel1

UENUM(BlueprintType)
namespace EWeaponProjectile
{
	/**Type of Projectile the Weapon uses*/
	enum ProjectileType
	{
		EBullet UMETA(Displayname = "Bullet"),
		ESpread UMETA(Displayname = "Spread"),
		EProjectile UMETA(Displayname = "Projectile")
	};
}

UENUM(BlueprintType)
namespace EWeaponFireType
{
	/** Firemode of the Weapon*/
	enum FireType
	{
		ESingleShot UMETA(Displayname = "SingleShot"),
		EBurstFire UMETA(Displayname = "BurstFire"),
		EAutoFire UMETA(Displayname = "AutoFire")
	};
}

UENUM(BlueprintType)
namespace EWeaponAmmoType
{
	/**Ammotype the Weapon Uses*/
	enum AmmoType
	{
		ESMGAmmo UMETA(Displayname = "SMG Ammo"),
		ERifleAmmo UMETA(Displayname = "Rifle Ammo"),
		ERocketLauncherAmmo UMETA(Displayname = "Rockets"),
		EShotgunAmmo UMETA(Displayname = "Shotgun Ammo")
	};
}

USTRUCT(BlueprintType)
struct FWeaponData
{
	GENERATED_USTRUCT_BODY()

		/** Size of the Clip*/
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
		int ClipSize;
	/** Amount of Ammo consumed per Shot*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
		int ShotCost;
	/** Damage done per Shot*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float Damage;
	/** Time it takes for the Weapon to Reload*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float ReloadTime;
	/** Delay between shots*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float ShotDelay;
	/** Maximal Range of the Weapon (there is no Damage Dropoff)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float WeaponRange;
	/** Current Ammo in Clip*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float CurrentClipAmmo;
	/** Amount of WeaponSpread(Shotgun only)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float WeaponSpread;
	/** Amount of Pellets the Weapon Shoots (Shotgun only)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		int AmountOfPellets;
	/** Amount of Shots per Burst the Weapon Shoots (Burstfire only)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		int i_ShotsPerBurst;
	/** Delay between shots during a burst*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float f_BurstShotDelay;
	/** The Inventory Slotnumber of the Weapon*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		int InventorySlot;
	/** If the Player has Unlimited Ammo (Debugging & Tutorial) True = No Ammo is Consumed on Shot*/
	UPROPERTY(EditAnywhere, Category = "Ammo")
		bool bUnlimitedClip;
	/** true = the Weapon does not Consume Reserveammo on Reload*/
	UPROPERTY(EditAnywhere, Category = "Ammo")
		bool bUnlimitedReserve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		TEnumAsByte<EWeaponFireType::FireType> FireType;
};


USTRUCT(BlueprintType)
struct FStatGenerationData
{
	GENERATED_USTRUCT_BODY()

public:
	/** Base Damage used when Calculating WeaponStats*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float f_WeaponBaseDamage;

	/** Size of the Clip*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
		int MinClipSize;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo)
		int MaxClipSize;

	/** Time it takes for the Weapon to Reload*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float MinReloadTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float MaxReloadTime;

	/** Delay between shots*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float MinShotDelay;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float MaxShotDelay;

	/** Delay between shots when the Rifle is in Burst Fire*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float MinBurstDelay;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float MaxBurstDelay;

	/** Amount of WeaponSpread(Shotgun only)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float MinWeaponSpread;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float MaxWeaponSpread;

	/** Amount of Pellets the Weapon Shoots (Shotgun only)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		int MinAmountOfPellets;

	/** Amount of Shots per Burst the Weapon Shoots (Burstfire only)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		int MinShotsPerBurst;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		int MaxShotsPerBurst;

	/** Delay between shots during a burst*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float MinBurstShotDelay;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		float MaxBurstShotDelay;
};


UCLASS()
class MYPROJECT_API AWeapon_BaseClass : public AItem_PickUp
{
	GENERATED_BODY()

public:
#pragma region Defaults
	AWeapon_BaseClass();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
#pragma endregion

	//////////Fire Functions//////////
	/** Fires the Weapon and based on the Projectiletype calls different Functions*/
	void Fire();
	/** Called by the Player class and calls the ExecuteBurst funtion with a delay for each shot*/
	void BurstFire();
	/** Projectile fire (Overridden in Derived Classes)*/
	virtual void ProjectileFire();
	bool b_CanFire;
private:
	/** Fires the burst*/
	void ExecuteBurst();
	/** Function that Uses Ammo*/
	void UseAmmo();
	/** Weaponfire with Raytrace/Raycast*/
	void InstantFire();
	void ResetFireTimer();
	FTimerHandle FireDelayTimerHandle;
	/** Shots fires in a single burst*/
	int i_BurstShotsFired;
	/** Timer handle for the burst fire Function */
	FTimerHandle BurstFireTimerHandle;


	//////////Reload//////////
public:
	/** Starts the Reload Routine*/
	void StartReload();
	/** Stopes the Reloading*/
	void StopReload();
	bool Reloading;
private:
	/** Finishes the Reload resets Timermanager and lets the Player use the Weapon again*/
	void FinishReload();
	/** Gets called after Weaponconfig.Reloadtime or Animation Length (if animation is available)
	Calculates ammo used and Subtracts that from the Reserveammo*/
	void Reload();

	//////////Equipping & Unequipping//////////
public:
	/** Functions to Equip the Weapon to the Player and Disable collision and set Mesh etc*/
	void EquipWeapon();
	/** Detaches the Weapon from the Player(Unequipping it)*/
	void DetachFromPlayer();
	void SetActiveWeapon(bool _b_Active);
	void HideWeapon();
private:
	void AttachToPlayer();
	void DropWeapon();
	bool ActiveWeapon;
public:
	/** Sets the Owning Player and its Playerstats*/
	void SetOwningPawn(AFPS_Character* NewOwner, UPlayerStats* NewPlayerStats);

	/** Generates the Stats for the Weapon. Called when dropped by an Enemy.
	Stats are Balanced with the Playerlevel/Arenaround in mind*/
	virtual void GenerateStats(float _f_WeaponStatsModifier);
	/** Copies the Stats from the Weapon on the Ground to the Weapon the Player uses*/
	void CopyStats(FWeaponData _WeapConfig, TEnumAsByte<EWeaponFireType::FireType> _FireType);

	/** References to Above Declaired Enums and Structs*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		FWeaponData WeaponConfig;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		FStatGenerationData StatGenerationData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		TEnumAsByte<EWeaponProjectile::ProjectileType> ProjectileType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
		TEnumAsByte<EWeaponAmmoType::AmmoType> AmmoType;
	/** Projectile class to Instantiate for Projectile Weapons*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Projectile)
		TSubclassOf<class ARocket> ProjectileClass;
	bool bCanPlayHitEffectSound;
	/** Sounds Cues and Audio Components to use*/
#pragma region Sound

	UAudioComponent* PlayWeaponSound(USoundCue* Sound);

	UPROPERTY(Transient)
		UAudioComponent* FireAC;

	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* FireSound;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* FireLoopSound;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* FireFinishSound;
	UPROPERTY(EditDefaultsOnly, Category = Sound)
		USoundCue* ClipEmptySound;
#pragma endregion

	/** AnimationMontages and Audio Components to use*/
#pragma region Animation

	UFUNCTION()
		float PlayWeaponAnimation(UAnimMontage* Animation);
	UFUNCTION()
		void StopAnimMontage(UAnimMontage* Animation);

	UPROPERTY(EditDefaultsOnly, Category = Animation)
		UAnimMontage* ReloadAnim;
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		UAnimMontage* FireAnim;
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		UAnimMontage* AimAnim;
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		UAnimMontage* AimFireAnim;
#pragma endregion

#pragma region MuzzleFlash
	/** FX for muzzle flash */
	UPROPERTY(EditDefaultsOnly, Category = "Particle System")
		UParticleSystem* MuzzleFX;
	/** spawned component for muzzle FX */
	UParticleSystemComponent* MuzzlePSC;

	/** FX for the Bullet Trail */
	UPROPERTY(EditDefaultsOnly, Category = "Particle System")
		UParticleSystem* TrailFX;
	UParticleSystemComponent* TrailPSC;

	/** FX for the Bullet Trail */
	UPROPERTY(EditDefaultsOnly, Category = "Particle System")
		UParticleSystem* ImpactFX;
	UParticleSystemComponent* ImpactPSC;
#pragma endregion  

#pragma region LootNotification
	UPROPERTY(EditAnywhere, Category = "Weapon|Loot Notification")
		UParticleSystemComponent* LootPSC;
	bool bShowLootPSC;

#pragma endregion

	UFUNCTION(BlueprintPure, Category = "Accesor Function")
		bool GetIsAiming();
	void SetIsAiming(bool _b_aiming);

	APawn* GetOwningPawn();

	/** Modifier used for Weapon accuracy Calculations*/
	UPROPERTY(EditAnywhere, Category = Config)
		float f_AccuracyModifier;
	/** Used for when dropping a Weapon to calculate the maximum stat value*/
	UPROPERTY(EditAnywhere, Category = Config)
		float f_MaxStatModifier;

protected:
	FHitResult WeaponTrace(const FVector &TraceFrom, const FVector &TraceTo) const;
	void ProcessInstantHit(const FHitResult & Impact, const FVector &Origin, const FVector &ShotDir, int32 RandomSeed, float ProjectileSpread);
	/** Starts FireDelay decrease to Check if the Weapon is ready to fire again*/
	void SetFireDelay(bool _b_bool);

	int32* AmmoInUse;
	int32 GetPlayerLevel();

	/** True == weapon has been shot and fire Delay should tick down*/
	bool b_DecreaseFireDelayTimer;
	/** true if the Player is holding right mouse button*/
	bool b_Aiming;
	/** Amount of Delay left before the Player can Fire again Gets set when the Player shot*/
	float f_FireDelayTimer;
	/** whether or not the weapon is executing a burst*/
	bool b_isBursting;

	UPlayerStats* PlayerStats;
	APlayerController* Playercontroller;
	AFPS_Character* MyPawn;

	/** timer handle that handles Reloading*/
	FTimerHandle ReloadTimerHandle;
};
