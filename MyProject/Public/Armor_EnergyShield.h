// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Armor_Base.h"
#include "Armor_EnergyShield.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API AArmor_EnergyShield : public AArmor_Base
{
	GENERATED_BODY()
	
public:
	/** Generate stats for shield (only has 1 of 3 defensive stats) */
	void GenerateStats() override;
	
	
};
