// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/TextRenderActor.h"
#include "CombatTextActor.generated.h"

class AFPS_Character;
UCLASS()
class MYPROJECT_API ACombatTextActor : public AActor
{
	GENERATED_BODY()
#pragma region Defaults	
public:
	ACombatTextActor();
protected:
	virtual void BeginPlay() override;
public:
	virtual void Tick(float DeltaTime) override;
#pragma endregion
	void SetText(FString Text, AFPS_Character* _Player);
	UPROPERTY(EditAnywhere, Category = "Combat Text")
		float LifeSpan;

private:
	ATextRenderActor* CombatText;
	UCapsuleComponent* ColliderComp;
	AFPS_Character* Player;
	FVector MoveDirection;
};
