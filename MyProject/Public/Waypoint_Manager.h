// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Public/Waypoint.h"
#include "Public/SpawnPoint.h"
#include "Waypoint_Manager.generated.h"

UCLASS()
class MYPROJECT_API AWaypoint_Manager : public AActor
{
	GENERATED_BODY()
	
public:	
#pragma region Defaults
	AWaypoint_Manager();
	virtual void BeginPlay() override;
#pragma endregion

	TArray<AWaypoint*> WaypointArray;
	TArray<ASpawnPoint*> SpawnpointArray;

	AWaypoint* GetRandomWaypoint();
	ASpawnPoint* GetRandomSpawnPoint();
};
