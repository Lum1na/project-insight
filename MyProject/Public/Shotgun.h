// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Weapon_BaseClass.h"
#include "Shotgun.generated.h"

/**
 *
 */
UCLASS()
class MYPROJECT_API AShotgun : public AWeapon_BaseClass
{
	GENERATED_BODY()
		AShotgun();
	virtual void GenerateStats(float _f_WeaponStatsModifier) override;

protected:
	UPlayerStats* PlayerStats;
};
