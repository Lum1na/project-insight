// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TutorialGameMode.h"
#include "TargetDummyEnemy.generated.h"

UCLASS()
class MYPROJECT_API ATargetDummyEnemy : public AActor
{
	GENERATED_BODY()

#pragma region Defaults
public:
	ATargetDummyEnemy();
protected:
	virtual void BeginPlay() override;
#pragma endregion

public:
	UPROPERTY(EditAnywhere, Category = "Mesh Rotation")
		FRotator HitMeshRotation;
	UPROPERTY(EditAnywhere, Category = "Mesh Rotation")
		FVector HitMeshLocation;
	UPROPERTY(EditAnywhere, Category = "Mesh Rotation")
		FRotator DefaultMeshRotation;
	UPROPERTY(EditAnywhere, Category = "Mesh Rotation")
		FVector DefaultMeshLocation;
	UPROPERTY(EditAnywhere, Category = "Meshs")
		UStaticMeshComponent* DummyMesh;
	UPROPERTY(EditAnywhere, Category = "Meshs")
		UBoxComponent* CollisionComp;

	void HitTarget();
	void ResetTarget();

private:
	bool bHitTarget;
class	ATutorialGameMode* GameMode;
};
