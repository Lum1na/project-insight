// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "WeaponSpawner.generated.h"

class AWeapon_BaseClass;
class AArmor_Base;
class AAmmoDrop;
class AHealthDrop;
class AItem_PickUp;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MYPROJECT_API UWeaponSpawner : public UActorComponent
{
	GENERATED_BODY()

public:
#pragma region Defaults
	UWeaponSpawner();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
#pragma endregion

	void ChoosePoolToSpawn();

private:
	float f_WeaponStatsModifier;

	void DropWeapon();
	void SpawnWeapon(TSubclassOf<AWeapon_BaseClass> WeapToSpawn);

	void DropArmor();
	void SpawnArmor(TSubclassOf<AArmor_Base> ArmorToSpawn);

	void DropItem();
	void SpawnPickUP(TSubclassOf<AItem_PickUp> Item);

#pragma region SpawnPoolsWeights

 	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
		float f_ChanceOfWeaponOrArmorDrop;

	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
	float Pool_PickUp_Weight; 

	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
		float Pool_Rifle_Weight;
	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
		float Pool_Smg_Weight;
	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
		float Pool_Shotgun_Weight;
	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
		float Pool_RocketLauncher_Weight;

	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
		float Weapons_Pool_Weight;
	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
		float Armor_Weight;

	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
		float Armor_Chestpiece_Weight;
	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
		float Armor_Helmet_Weight;
	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
		float Armor_Legpiece_Weight;
	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
		float Armor_Feet_Weight;
	UPROPERTY(EditAnywhere, Category = "WeaponSpawner|ItemDrops")
		float Armor_EnergyShield_Weight;

#pragma endregion

	TSubclassOf<AWeapon_BaseClass> SmgSpawn;
	TSubclassOf<AWeapon_BaseClass> RifleSpawn;
	TSubclassOf<AWeapon_BaseClass> ShotgunSpawn;
	TSubclassOf<AWeapon_BaseClass> RocketLauncherSpawn;

	TSubclassOf<AWeapon_BaseClass> WeaponToSpawn;
	TSubclassOf<AArmor_Base> ArmorToSpawn;

	TSubclassOf<AArmor_Base> ChestArmor;
	TSubclassOf<AArmor_Base> FeetArmor;
	TSubclassOf<AArmor_Base> HeadArmor;
	TSubclassOf<AArmor_Base> LegArmor;
	TSubclassOf<AArmor_Base> EnergyShield;

	TSubclassOf<AItem_PickUp> AmmoDrop;
	TSubclassOf<AItem_PickUp> HealthDrop;
	TSubclassOf<AItem_PickUp> ItemToSpawn;
};
