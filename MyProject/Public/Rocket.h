// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Rocket.generated.h"


class AWeapon_BaseClass;
class AFPS_Character;
UCLASS()
class MYPROJECT_API ARocket : public AActor
{
	GENERATED_BODY()
	
public:	
	ARocket(const FObjectInitializer& ObjectInitializer);
	virtual void BeginPlay() override;
	virtual void Tick( float DeltaSeconds ) override;

	/** FX for the Bullet Trail */
	UPROPERTY(EditDefaultsOnly, Category = "Particle System")
		UParticleSystem* ImpactFX;
	UParticleSystemComponent* ImpactPSC;

	UPROPERTY(VisibleAnywhere, Category = Projectile)
		USphereComponent* CollisionComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
		class UProjectileMovementComponent* ProjectileMovement;

	UFUNCTION()
		void OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

private:
	AFPS_Character* OwningPlayer;
	float f_Damage;
};

