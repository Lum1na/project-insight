// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "Widget.h"
#include "MyProjectGameModeBase.generated.h"

class UUserWidget;
class AWaypoint_Manager;
class ABase_EnemyCharacter;
class AAI_BaseEnemyController;

/**
 *
 */
UCLASS()
class MYPROJECT_API AMyProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	AMyProjectGameModeBase();
	virtual void BeginPlay() override; //Override BeginPlay from the base class
	virtual void Tick(float DeltaSeconds) override;

	///////////Desired Player HUD to be spawned///////////
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HUD", Meta = (BlueprintsProtected = "true"))
		TSubclassOf<class UUserWidget> PlayerHUDClass;
	UPROPERTY(BlueprintReadOnly)
		UUserWidget* CurrentWidget;

	///////////Player Character///////////
	/** Sets the Player Character*/
	void SetPlayerCharacter(class AFPS_Character* _Player) { Player = _Player; }
	void SpawnWeaponFromSaveGame(TSubclassOf<AItem_PickUp> _ItemBase, FWeaponData _WeaponData, bool bIsEquipped = false);
	void SpawnArmorFromSaveGame(TSubclassOf<AItem_PickUp> _ItemBase, FArmorData _ArmorData, bool bIsEquipped = false);
private:
	class AFPS_Character* Player;


public:

	UFUNCTION(BlueprintCallable)
		void SaveGame();
	UFUNCTION(BlueprintCallable)
		void LoadGame();
	///////////Arena Variables & Functions /////////// 
	/** Checks if all enemies are dead*/
	void CheckRoundEnd();
	/** Ends the Round if the Player has died*/
	void EndRoundByPlayerDeath();
	/** Starts the Countdown timer for a new round to begin*/
	UFUNCTION(BlueprintCallable)
		void StartNewRoundCountDownTimer();
	/** Starts a Specific Round that the Player has Played before. If the Number is higher then the highest round Survived the round will be set to the highest survived + 1*/
	UFUNCTION(BlueprintCallable)
		void StartSelectedRound();

	/** Minimum Enemies that can spawn during a round*/
	UPROPERTY(EditDefaultsOnly, Category = "Arena")
		int i_MinEnemiesPerRound;
	/** Maximum Enemies that can spawn during a round*/
	UPROPERTY(EditDefaultsOnly, Category = "Arena")
		int i_MaxEnemiesPerRound;

	/** Time it takes to spawn Enemies after a new round has started*/
	UPROPERTY(EditDefaultsOnly, Category = "Arena")
		float f_NewRoundCountdownTime;

	/** Enemie Type that will be Spawned when the first round is Started*/
	UPROPERTY(EditDefaultsOnly, Category = "Arena")
		TSubclassOf<ABase_EnemyCharacter> EnemyToSpawn;
	/** Multiplier used when generation stats for Weapons and Enemies*/
	UPROPERTY(EditDefaultsOnly, Category = "Arena|Stats Generation")
		float f_ArenaRoundMultiplier;
	/** Returns the Multiplier*/
	float GetArenaRoundMultiplier();

	///////////Accesor Functions /////////// 	
	UFUNCTION(BlueprintCallable)
		int GetAliveEnemies();
	UFUNCTION(BlueprintCallable)
		int GetEnemiesThisRound();
	UFUNCTION(BlueprintCallable)
		int GetCurrentRound();
	UFUNCTION(BlueprintCallable)
		int GetMaxRoundUnlocked();
	UFUNCTION(BlueprintCallable)
		float GetNewRoundCountdownTimer();
	UFUNCTION(BlueprintCallable)
		bool GetIsNewRoundTimerActive();
	UFUNCTION(BlueprintCallable)
		bool GetIsRoundActive();
	UFUNCTION(BlueprintCallable)
		bool GetSurvivedLastRound();
	UFUNCTION(BlueprintCallable)
		bool GetIsFirstRound();
	UFUNCTION(BlueprintCallable)
		FString GetAfterRoundText();
	UFUNCTION(BlueprintCallable)
		ESlateVisibility GetShowNewRoundCountdownTimer();

	/** Function to set the bool that controlls if a desired round should be started or not*/
	UFUNCTION(BlueprintCallable)
		void Setb_StartSelectedRound(bool _b_bool);
	/** Function that sets the desired round to be played*/
	UFUNCTION(BlueprintCallable)
		void SetSelectedRound(int _i_NextRoundToStart);
	void SetMaxRoundUnlocked(int _MaxRoundsUnlocked);

	/** Toggles the Round Selection window and returns the Visibility Status*/
	void ToggleRoundSelection();
	UFUNCTION(BlueprintCallable, Category = "GameMode|RoundSlection")
		ESlateVisibility GetRoundSelectionShowing();
private:
	/** Spawns the Maximum amount of enemies that can be fought*/
	void SpawnEnemy();
	/** Starts a new round*/
	void StartNextRound();
	/** Resets the Enemies to either respawn or be set outside of the Arena if they number of Enemies that round has been reached*/
	void ResetEnemies();
	/** Decreses the next round timer by 0.01 every 0.01 seconds*/
	void DecreaseNextRoundTimer();



	/** An Array of enemies that have been spawned to more easily reset them*/
	TArray<ABase_EnemyCharacter*> SpawnedEnemies;
	/** Reference to the WaypointManager to get a Spawnpoint for the Enemies*/
	AWaypoint_Manager* WaypointManager;

	int i_EnemiesAlive;
	int i_CurrentRound;
	int i_MaxRoundUnlocked;
	int i_RoundEnemyAmount;
	int i_RoundToStart;

	float f_CountdownTimer;

	bool b_IsFirstRound;
	bool b_StartSelectedRound;
	bool b_RoundActive;
	bool b_FinishedRound;
	bool b_SpawnedEnemies;
	bool b_CountdownTimerStarted;
	bool b_SurvivedLastRound;

	bool b_ShowRoundSelection;

	FTimerHandle NewRoundTimerHandle;
};
