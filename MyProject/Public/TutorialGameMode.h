// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "TutorialGameMode.generated.h"

UCLASS()
class MYPROJECT_API ATutorialGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Tutorial|Timer")
		int32 TutorialTextDelay;

	UFUNCTION(BlueprintCallable, Category = "Tutorial")
		void SetbCompletedMovementTutorial(bool _bool = true);
	UFUNCTION(BlueprintCallable, Category = "Tutorial")
		void SetbCompletedWeaponTutorial(bool _bool);
	UFUNCTION(BlueprintCallable, Category = "Tutorial")
		void SetbCompletedItemTutorial(bool _bool);

	UFUNCTION(BlueprintCallable, Category = "Tutorial")
		bool GetbCompletedMovementTutorial();
	UFUNCTION(BlueprintCallable, Category = "Tutorial")
		bool GetbCompletedWeaponTutorial();
	UFUNCTION(BlueprintCallable, Category = "Tutorial")
		bool GetbCompletedItemTutorial();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HUD", Meta = (BlueprintsProtected = "true"))
		TSubclassOf<class UUserWidget> PlayerHUDClass;
	UPROPERTY(BlueprintReadOnly)
		UUserWidget* CurrentWidget;

	void AddTarget();
	void AddShotTargetCounter();
	void CheckMovementTutorialCompleted();

#pragma region MovementTutorial
	UFUNCTION(BlueprintCallable, Category = "Tutorial")
		void SetbWPressed();
	UFUNCTION(BlueprintCallable, Category = "Tutorial")
		void SetbSPressed();
	UFUNCTION(BlueprintCallable, Category = "Tutorial")
		void SetbAPressed();
	UFUNCTION(BlueprintCallable, Category = "Tutorial")
		void SetbDPressed();
	UFUNCTION(BlueprintCallable, Category = "Tutorial")
		void SetbSpacePressed();

	bool bWPressed;
	bool bSPressed;
	bool bAPressed;
	bool bDPressed;
	bool bSpacePressed;
#pragma endregion

#pragma region Weapon Tutorial
	UFUNCTION(BlueprintCallable)
		void StartWeaponTutorial();
#pragma endregion 

#pragma region ItemTUtorial

	UFUNCTION(BlueprintCallable)
		void StartItemTutorial();
	void StartSecondTutorialPart();
	void StartThirdTutorialPart();
	void StartLastTutorialPart();
#pragma endregion

private:
	class ATargetDummyEnemy* Dummy;
	bool bCompletedMovementTutorial;
	bool bCompletedWeaponTutorial;
	bool bCompletedItemTutorial;
	class AFPS_Character* Player;
	int32 DummyTargets;
	int32 ShotTargetCounter;
};
